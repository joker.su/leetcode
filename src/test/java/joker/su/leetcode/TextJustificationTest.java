package joker.su.leetcode;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class TextJustificationTest {

    @Test
    public void example1Test() {
        TextJustification justification = new TextJustification();
        List<String> result = justification.fullJustify(new String[]{"This", "is", "an", "example", "of", "text", "justification."}, 16);
        List<String> standard = Arrays.asList("This    is    an", "example  of text", "justification.  ");
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example2Test() {
        TextJustification justification = new TextJustification();
        List<String> result = justification.fullJustify(new String[]{"What", "must", "be", "acknowledgment", "shall", "be"}, 16);
        List<String> standard = Arrays.asList("What   must   be", "acknowledgment  ", "shall be        ");
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example3Test() {
        TextJustification justification = new TextJustification();
        List<String> result = justification.fullJustify(
            new String[]{"Science", "is", "what", "we", "understand", "well", "enough", "to", "explain", "to", "a", "computer.", "Art",
                         "is", "everything", "else", "we", "do"}, 20);
        List<String> standard = Arrays.asList("Science  is  what we", "understand      well", "enough to explain to",
            "a  computer.  Art is", "everything  else  we", "do                  ");
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }
}
