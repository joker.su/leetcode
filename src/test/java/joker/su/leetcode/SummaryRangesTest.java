package joker.su.leetcode;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class SummaryRangesTest {

    @Test
    public void example1Test() {
        SummaryRanges summaryRanges = new SummaryRanges();
        Assert.assertTrue(CollectionUtils.isEqualCollection(List.of("0->2", "4->5", "7"),
            summaryRanges.summaryRanges(new int[]{0, 1, 2, 4, 5, 7})));
    }

    @Test
    public void exampl21Test() {
        SummaryRanges summaryRanges = new SummaryRanges();
        Assert.assertTrue(CollectionUtils.isEqualCollection(List.of("0", "2->4", "6", "8->9"),
            summaryRanges.summaryRanges(new int[]{0, 2, 3, 4, 6, 8, 9})));
    }
}
