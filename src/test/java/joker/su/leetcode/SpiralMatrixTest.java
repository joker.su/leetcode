package joker.su.leetcode;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class SpiralMatrixTest {

    @Test
    public void example1Test() {
        SpiralMatrix tester = new SpiralMatrix();
        List<Integer> result = tester.spiralOrder(new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}});
        List<Integer> standard = List.of(1, 2, 3, 6, 9, 8, 7, 4, 5);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example2Test() {
        SpiralMatrix tester = new SpiralMatrix();
        List<Integer> result = tester.spiralOrder(new int[][]{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}});
        List<Integer> standard = List.of(1, 2, 3, 4, 8, 12, 11, 10, 9, 5, 6, 7);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example3Test() {
        SpiralMatrix tester = new SpiralMatrix();
        List<Integer> result = tester.spiralOrder(new int[][]{{1, 2}, {3, 4}});
        List<Integer> standard = List.of(1, 2, 4, 3);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }
}
