package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class MaxConsecutiveOnesIITest {


    @Test
    public void example1Test() {
        MaxConsecutiveOnesII tester = new MaxConsecutiveOnesII();
        Assert.assertEquals(4, tester.findMaxConsecutiveOnes(new int[]{1,0,1,1,0}));
    }

    @Test
    public void example2Test() {
        MaxConsecutiveOnesII tester = new MaxConsecutiveOnesII();
        Assert.assertEquals(4, tester.findMaxConsecutiveOnes(new int[]{1,0,1,1,0,1}));
    }

    @Test
    public void example3Test() {
        MaxConsecutiveOnesII tester = new MaxConsecutiveOnesII();
        Assert.assertEquals(4, tester.findMaxConsecutiveOnes(new int[]{1,1,0,1}));
    }

    @Test
    public void example4Test() {
        MaxConsecutiveOnesII tester = new MaxConsecutiveOnesII();
        Assert.assertEquals(1, tester.findMaxConsecutiveOnes(new int[]{1}));
    }

    @Test
    public void example5Test() {
        MaxConsecutiveOnesII tester = new MaxConsecutiveOnesII();
        Assert.assertEquals(1, tester.findMaxConsecutiveOnes(new int[]{0}));
    }

    @Test
    public void example6Test() {
        MaxConsecutiveOnesII tester = new MaxConsecutiveOnesII();
        Assert.assertEquals(1, tester.findMaxConsecutiveOnes(new int[]{0,0}));
    }

    @Test
    public void example7Test() {
        MaxConsecutiveOnesII tester = new MaxConsecutiveOnesII();
        Assert.assertEquals(2, tester.findMaxConsecutiveOnes(new int[]{1,1}));
    }

}
