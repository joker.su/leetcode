package joker.su.leetcode;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class DesignInMemoryFileSystemTest {

    @Test
    public void example1Test() {
        DesignInMemoryFileSystem fileSystem = new DesignInMemoryFileSystem();
        Assert.assertTrue(CollectionUtils.isEqualCollection(new ArrayList<>(), fileSystem.ls("/")));
        fileSystem.mkdir("/a/b/c");
        fileSystem.addContentToFile("/a/b/c/d", "hello");
        Assert.assertTrue(CollectionUtils.isEqualCollection(List.of("a"), fileSystem.ls("/")));
        Assert.assertTrue(CollectionUtils.isEqualCollection(List.of("b"), fileSystem.ls("/a")));
        Assert.assertTrue(CollectionUtils.isEqualCollection(List.of("c"), fileSystem.ls("/a/b")));
        Assert.assertEquals("hello", fileSystem.readContentFromFile("/a/b/c/d"));
    }
}
