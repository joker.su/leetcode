package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class DecodeStringTest {

    @Test
    public void example1Test() {
        DecodeString decodeString = new DecodeString();
        Assert.assertEquals("aaabcbc", decodeString.decodeString("3[a]2[bc]"));
    }

    @Test
    public void example2Test() {
        DecodeString decodeString = new DecodeString();
        Assert.assertEquals("accaccacc", decodeString.decodeString("3[a2[c]]"));
    }

    @Test
    public void example3Test() {
        DecodeString decodeString = new DecodeString();
        Assert.assertEquals("abcabccdcdcdef", decodeString.decodeString("2[abc]3[cd]ef"));
    }
}
