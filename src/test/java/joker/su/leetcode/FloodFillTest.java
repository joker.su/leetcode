package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class FloodFillTest {

    @Test
    public void example1Test() {
        FloodFill floodFill = new FloodFill();
        int[][] result = floodFill.floodFill(new int[][]{{1, 1, 1}, {1, 1, 0}, {1, 0, 1}}, 1, 1, 2);
        Assert.assertArrayEquals(new int[][]{{2, 2, 2}, {2, 2, 0}, {2, 0, 1}}, result);
    }

    @Test
    public void example2Test() {
        FloodFill floodFill = new FloodFill();
        int[][] result = floodFill.floodFill(new int[][]{{0, 0, 0}, {0, 0, 0}}, 0, 0, 2);
        Assert.assertArrayEquals(new int[][]{{2, 2, 2}, {2, 2, 2}}, result);
    }

    @Test
    public void example3Test() {
        FloodFill floodFill = new FloodFill();
        int[][] result = floodFill.floodFill(new int[][]{{0, 0, 0}, {0, 1, 1}}, 1, 1, 1);
        Assert.assertArrayEquals(new int[][]{{0, 0, 0}, {0, 1, 1}}, result);
    }
}
