package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class FirstMissingPositiveTest {

    @Test
    public void example1Test() {
        FirstMissingPositive tester = new FirstMissingPositive();
        Assert.assertEquals(3, tester.firstMissingPositive(new int[]{1,2,0}));
    }

    @Test
    public void example2Test() {
        FirstMissingPositive tester = new FirstMissingPositive();
        Assert.assertEquals(4, tester.firstMissingPositive(new int[]{3,2,1}));
    }

    @Test
    public void example3Test() {
        FirstMissingPositive tester = new FirstMissingPositive();
        Assert.assertEquals(2, tester.firstMissingPositive(new int[]{3,4,-1,1}));
    }

    @Test
    public void example4Test() {
        FirstMissingPositive tester = new FirstMissingPositive();
        Assert.assertEquals(1, tester.firstMissingPositive(new int[]{7,8,9,11,12}));
    }
}
