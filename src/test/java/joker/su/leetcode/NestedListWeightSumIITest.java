package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

import joker.su.leetcode.api.NestedInteger;

public class NestedListWeightSumIITest {

    @Test
    public void example1Test() {
        NestedListWeightSumII nestedListWeightSumII = new NestedListWeightSumII();
        NestedInteger testValue = NestedInteger.of("[[1,1],2,[1,1]]");
        int result = nestedListWeightSumII.depthSumInverse(testValue.getList());
        Assert.assertEquals( 8, result);
    }

    @Test
    public void example2Test() {
        NestedListWeightSumII nestedListWeightSumII = new NestedListWeightSumII();
        NestedInteger testValue = NestedInteger.of("[1,[4,[6]]]");
        int result = nestedListWeightSumII.depthSumInverse(testValue.getList());
        Assert.assertEquals( 17, result);
    }

    @Test
    public void example3Test() {
        NestedListWeightSumII nestedListWeightSumII = new NestedListWeightSumII();
        NestedInteger testValue = NestedInteger.of("[7,5,2,[6,[4,2,[5],[[4,[98]],10]]],[],[[]]]");
        int result = nestedListWeightSumII.depthSumInverse(testValue.getList());
        Assert.assertEquals( 289, result);
    }
    @Test
    public void example4Test() {
        NestedListWeightSumII nestedListWeightSumII = new NestedListWeightSumII();
        NestedInteger testValue = NestedInteger.of("[[1,1],2,[1,1],[[[[]]]]]");
        int result = nestedListWeightSumII.depthSumInverse(testValue.getList());
        Assert.assertEquals( 20, result);
    }
}
