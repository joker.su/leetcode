package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class LongestPalindromicSubstringTest {

    @Test
    public void example1Test() {
        LongestPalindromicSubstring longestPalindromicSubstring = new LongestPalindromicSubstring();
        String result = longestPalindromicSubstring.longestPalindrome("babad");
        Assert.assertTrue("bab".equals(result) || "aba".equals(result));
    }

    @Test
    public void example2Test() {
        LongestPalindromicSubstring longestPalindromicSubstring = new LongestPalindromicSubstring();
        String result = longestPalindromicSubstring.longestPalindrome("cbbd");
        Assert.assertEquals("bb", result);
    }

    @Test
    public void example3Test() {
        LongestPalindromicSubstring longestPalindromicSubstring = new LongestPalindromicSubstring();
        String result = longestPalindromicSubstring.longestPalindrome("abba");
        Assert.assertEquals("abba", result);
    }
}
