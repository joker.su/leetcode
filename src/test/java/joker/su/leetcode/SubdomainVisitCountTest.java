package joker.su.leetcode;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class SubdomainVisitCountTest {

    @Test
    public void example1Test() {
        SubdomainVisitCount subdomainVisitCount = new SubdomainVisitCount();
        List<String> result = subdomainVisitCount.subdomainVisits(new String[]{"9001 discuss.leetcode.com"});
        List<String> standard = Arrays.asList("9001 leetcode.com","9001 discuss.leetcode.com","9001 com");
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }


    @Test
    public void example2Test() {
        SubdomainVisitCount subdomainVisitCount = new SubdomainVisitCount();
        List<String> result = subdomainVisitCount.subdomainVisits(new String[]{"900 google.mail.com", "50 yahoo.com", "1 intel.mail.com", "5 wiki.org"});
        List<String> standard = Arrays.asList("901 mail.com","50 yahoo.com","900 google.mail.com","5 wiki.org","5 org","1 intel.mail.com","951 com");
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }
}
