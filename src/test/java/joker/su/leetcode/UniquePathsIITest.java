package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class UniquePathsIITest {

    @Test
    public void example1Test() {
        UniquePathsII uniquePaths = new UniquePathsII();
        Assert.assertEquals(2, uniquePaths.uniquePathsWithObstacles(new int[][]{{0,0,0}, {0,1,0}, {0,0,0}}));
    }

    @Test
    public void example2Test() {
        UniquePathsII uniquePaths = new UniquePathsII();
        Assert.assertEquals(1, uniquePaths.uniquePathsWithObstacles(new int[][]{{0,1}, {0,0}}));
    }

    @Test
    public void example3Test() {
        UniquePathsII uniquePaths = new UniquePathsII();
        Assert.assertEquals(1, uniquePaths.uniquePathsWithObstacles(new int[][]{{0,0}}));
    }

    @Test
    public void example4Test() {
        UniquePathsII uniquePaths = new UniquePathsII();
        Assert.assertEquals(0, uniquePaths.uniquePathsWithObstacles(new int[][]{{1,0}}));
    }

    @Test
    public void example5Test() {
        UniquePathsII uniquePaths = new UniquePathsII();
        Assert.assertEquals(0, uniquePaths.uniquePathsWithObstacles(new int[][]{{0,0}, {1,1}, {0,0}}));
    }

    @Test
    public void example6Test() {
        UniquePathsII uniquePaths = new UniquePathsII();
        Assert.assertEquals(0, uniquePaths.uniquePathsWithObstacles(new int[][]{{0,1,0,0,0},{1,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0}}));
    }
}
