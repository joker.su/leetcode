package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class ZigzagConversionTest {

    @Test
    public void example1Test() {
        ZigzagConversion tester = new ZigzagConversion();
        Assert.assertEquals("PAHNAPLSIIGYIR", tester.convert("PAYPALISHIRING", 3));
    }

    @Test
    public void example2Test() {
        ZigzagConversion tester = new ZigzagConversion();
        Assert.assertEquals("PINALSIGYAHRPI", tester.convert("PAYPALISHIRING", 4));
    }

    @Test
    public void example3Test() {
        ZigzagConversion tester = new ZigzagConversion();
        Assert.assertEquals("A", tester.convert("A", 1));
    }
}
