package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class JumpGameIITest {
    @Test
    public void example1Test() {
        JumpGameII jumpGame = new JumpGameII();
        Assert.assertEquals(2, jumpGame.jump(new int[]{2, 3, 1, 1, 4}));
    }

    @Test
    public void example2Test() {
        JumpGameII jumpGame = new JumpGameII();
        Assert.assertEquals(2, jumpGame.jump(new int[]{2, 3, 0, 1, 4}));
    }

    @Test
    public void example3Test() {
        JumpGameII jumpGame = new JumpGameII();
        Assert.assertEquals(1, jumpGame.jump(new int[]{1, 4}));
    }

    @Test
    public void example5Test() {
        JumpGameII jumpGame = new JumpGameII();
        Assert.assertEquals(1, jumpGame.jump(new int[]{20, 3, 1, 1, 4}));
    }

    @Test
    public void example6Test() {
        JumpGameII jumpGame = new JumpGameII();
        Assert.assertEquals(0, jumpGame.jump(new int[]{1}));
    }

    @Test
    public void example7Test() {
        JumpGameII jumpGame = new JumpGameII();
        Assert.assertEquals(3, jumpGame.jump(new int[]{3,4,3,2,5,4,3}));
    }
}
