package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class RunningSumOf1dArrayTest {

    @Test
    public void example1Test() {
        RunningSumOf1dArray runningSumOf1dArray = new RunningSumOf1dArray();
        int[] result = runningSumOf1dArray.runningSum(new int[]{1,2,3,4});
        Assert.assertArrayEquals(new int[]{1,3,6,10}, result);
    }

    @Test
    public void example2Test() {
        RunningSumOf1dArray runningSumOf1dArray = new RunningSumOf1dArray();
        int[] result = runningSumOf1dArray.runningSum(new int[]{1,1,1,1,1});
        Assert.assertArrayEquals(new int[]{1,2,3,4,5}, result);
    }

    @Test
    public void example3Test() {
        RunningSumOf1dArray runningSumOf1dArray = new RunningSumOf1dArray();
        int[] result = runningSumOf1dArray.runningSum(new int[]{3,1,2,10,1});
        Assert.assertArrayEquals(new int[]{3,4,6,16,17}, result);
    }
}
