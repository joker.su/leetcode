package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class RotateArrayTest {

    @Test
    public void oneTest() {
        RotateArray rotateArray = new RotateArray();
        int[] result = new int[]{1, 2, 4, 5};
        rotateArray.rotate(result, 1);
        Assert.assertArrayEquals(new int[]{5, 1, 2, 4}, result);
    }

    @Test
    public void twoTest() {
        RotateArray rotateArray = new RotateArray();
        int[] result = new int[]{1, 2, 3, 4, 5, 6, 7};
        rotateArray.rotate(result, 2);
        Assert.assertArrayEquals(new int[]{6, 7, 1, 2, 3, 4, 5}, result);
    }

    @Test
    public void threeTest() {
        RotateArray rotateArray = new RotateArray();
        int[] result = new int[]{1, 2, 3, 4, 5};
        rotateArray.rotate(result, 3);
        Assert.assertArrayEquals(new int[]{3, 4, 5, 1, 2}, result);
    }

    @Test
    public void zeroTest() {
        RotateArray rotateArray = new RotateArray();
        int[] result = new int[]{1, 2, 3, 4, 5};
        rotateArray.rotate(result, 0);
        Assert.assertArrayEquals(new int[]{1, 2, 3, 4, 5}, result);
    }


    @Test
    public void lengthTest() {
        RotateArray rotateArray = new RotateArray();
        int[] result = new int[]{1, 2, 4, 5};
        rotateArray.rotate(result, 4);
        Assert.assertArrayEquals(new int[]{1, 2, 4, 5}, result);
    }

    @Test
    public void moreThenLengthTest() {
        RotateArray rotateArray = new RotateArray();
        int[] result = new int[]{1, 2, 4, 5};
        rotateArray.rotate(result, 6);
        Assert.assertArrayEquals(new int[]{4, 5, 1, 2}, result);
    }

    @Test
    public void exampleTest() {
        RotateArray rotateArray = new RotateArray();
        int[] result = new int[]{1, 2, 3, 4, 5, 6, 7};
        rotateArray.rotate(result, 3);
        Assert.assertArrayEquals(new int[]{5, 6, 7, 1, 2, 3, 4}, result);
    }
}
