package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class RandomPickWithWeightTest {

    @Test
    public void example1Test() {
        RandomPickWithWeight randomPickWithWeight = new RandomPickWithWeight(new int[]{1, 3});
        int count = 0;
        int numberFirstCounter = 0;
        int numberSecondCounter = 0;
        while (count < 100000) {
            int index = randomPickWithWeight.pickIndex();
            if (index == 0) {
                numberFirstCounter++;
            } else if (index == 1) {
                numberSecondCounter++;
            } else {
                throw new IllegalArgumentException("Out of index");
            }
            count++;
        }
        Assert.assertEquals(0.25, numberFirstCounter / (double) count, 0.005d);
        Assert.assertEquals(0.75, numberSecondCounter / (double) count, 0.005d);
    }
}
