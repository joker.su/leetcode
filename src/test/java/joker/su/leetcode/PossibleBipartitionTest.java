package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class PossibleBipartitionTest {

    @Test
    public void example1Test() {
        PossibleBipartition tester = new PossibleBipartition();
        Assert.assertTrue(tester.possibleBipartition(4, new int[][]{{1, 2}, {1, 3}, {2, 4}}));
    }

    @Test
    public void example2Test() {
        PossibleBipartition tester = new PossibleBipartition();
        Assert.assertFalse(tester.possibleBipartition(3, new int[][]{{1, 2}, {1, 3}, {2, 3}}));
    }
}
