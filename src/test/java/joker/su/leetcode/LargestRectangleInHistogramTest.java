package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class LargestRectangleInHistogramTest {
    @Test
    public void example1Test() {
        LargestRectangleInHistogram largestRectangleInHistogram = new LargestRectangleInHistogram();
        Assert.assertEquals(10, largestRectangleInHistogram.largestRectangleArea(new int[]{2,1,5,6,2,3}));
    }

    @Test
    public void example2Test() {
        LargestRectangleInHistogram largestRectangleInHistogram = new LargestRectangleInHistogram();
        Assert.assertEquals(4, largestRectangleInHistogram.largestRectangleArea(new int[]{2,4}));
    }
}
