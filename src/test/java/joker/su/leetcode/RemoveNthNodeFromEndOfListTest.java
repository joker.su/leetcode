package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

import joker.su.leetcode.api.ListNode;

public class RemoveNthNodeFromEndOfListTest {

    @Test
    public void exampleTest() {
        RemoveNthNodeFromEndOfList removeNthNodeFromEndOfList = new RemoveNthNodeFromEndOfList();
        ListNode nodes = ListNode.toNodeList(5);
        ListNode result = removeNthNodeFromEndOfList.removeNthFromEnd(nodes, 2);
        Assert.assertArrayEquals(new int[]{1, 2, 3, 5}, ListNode.toArrayValue(result));
    }

    @Test
    public void exampleSecondTest() {
        RemoveNthNodeFromEndOfList removeNthNodeFromEndOfList = new RemoveNthNodeFromEndOfList();
        ListNode nodes = ListNode.toNodeList(1);
        ListNode result = removeNthNodeFromEndOfList.removeNthFromEnd(nodes, 1);
        Assert.assertArrayEquals(new int[]{}, ListNode.toArrayValue(result));
    }

    @Test
    public void exampleThirdTest() {
        RemoveNthNodeFromEndOfList removeNthNodeFromEndOfList = new RemoveNthNodeFromEndOfList();
        ListNode nodes = ListNode.toNodeList(2);
        ListNode result = removeNthNodeFromEndOfList.removeNthFromEnd(nodes, 1);
        Assert.assertArrayEquals(new int[]{1}, ListNode.toArrayValue(result));
    }

    @Test
    public void example4Test() {
        RemoveNthNodeFromEndOfList removeNthNodeFromEndOfList = new RemoveNthNodeFromEndOfList();
        ListNode nodes = ListNode.toNodeList(2);
        ListNode result = removeNthNodeFromEndOfList.removeNthFromEnd(nodes, 2);
        Assert.assertArrayEquals(new int[]{2}, ListNode.toArrayValue(result));
    }
}
