package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class NumberOf1BitsTest {

    @Test
    public void example1Test() {
        NumberOf1Bits numberOf1Bits = new NumberOf1Bits();
        int result = numberOf1Bits.hammingWeight(0);
        Assert.assertEquals(0, result);
    }


    @Test
    public void example2Test() {
        NumberOf1Bits numberOf1Bits = new NumberOf1Bits();
        int result = numberOf1Bits.hammingWeight(13);
        Assert.assertEquals(3, result);
    }


    @Test
    public void example3Test() {
        NumberOf1Bits numberOf1Bits = new NumberOf1Bits();
        int result = numberOf1Bits.hammingWeight(128);
        Assert.assertEquals(1, result);
    }


    @Test
    public void example4Test() {
        NumberOf1Bits numberOf1Bits = new NumberOf1Bits();
        int result = numberOf1Bits.hammingWeight(2147483645);
        Assert.assertEquals(30, result);
    }

    @Test
    public void example5Test() {
        NumberOf1Bits numberOf1Bits = new NumberOf1Bits();
        int result = numberOf1Bits.hammingWeight(-3);
        Assert.assertEquals(31, result);
    }
}
