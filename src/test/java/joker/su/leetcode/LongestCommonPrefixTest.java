package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class LongestCommonPrefixTest {

    @Test
    public void example1Test() {
        LongestCommonPrefix longestCommonPrefix = new LongestCommonPrefix();
        String result = longestCommonPrefix.longestCommonPrefix(new String[]{"flower", "flow", "flight"});
        Assert.assertEquals("fl", result);
    }

    @Test
    public void example2Test() {
        LongestCommonPrefix longestCommonPrefix = new LongestCommonPrefix();
        String result = longestCommonPrefix.longestCommonPrefix(new String[]{"dog", "racecar", "car"});
        Assert.assertEquals("", result);
    }


    @Test
    public void example3Test() {
        LongestCommonPrefix longestCommonPrefix = new LongestCommonPrefix();
        String result = longestCommonPrefix.longestCommonPrefix(new String[]{"dog"});
        Assert.assertEquals("dog", result);
    }


    @Test
    public void example4Test() {
        LongestCommonPrefix longestCommonPrefix = new LongestCommonPrefix();
        String result = longestCommonPrefix.longestCommonPrefix(new String[]{"dog", "dad"});
        Assert.assertEquals("d", result);
    }

    @Test
    public void example5Test() {
        LongestCommonPrefix longestCommonPrefix = new LongestCommonPrefix();
        String result = longestCommonPrefix.longestCommonPrefix(new String[]{"ab", "a"});
        Assert.assertEquals("a", result);
    }
}
