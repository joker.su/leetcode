package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

import joker.su.leetcode.api.ListNode;

public class MergeTwoSortedListsTest {

    @Test
    public void example1Test() {
        MergeTwoSortedLists mergeTwoSortedLists = new MergeTwoSortedLists();
        ListNode first = ListNode.toNodeList(new int[]{1, 2, 4});
        ListNode second = ListNode.toNodeList(new int[]{1, 3, 4});
        ListNode merged = mergeTwoSortedLists.mergeTwoLists(first, second);
        Assert.assertArrayEquals(new int[]{1, 1, 2, 3, 4, 4}, ListNode.toArrayValue(merged));
    }

    @Test
    public void example2Test() {
        MergeTwoSortedLists mergeTwoSortedLists = new MergeTwoSortedLists();
        ListNode first = ListNode.toNodeList(new int[]{});
        ListNode second = ListNode.toNodeList(new int[]{});
        ListNode merged = mergeTwoSortedLists.mergeTwoLists(first, second);
        Assert.assertArrayEquals(new int[]{}, ListNode.toArrayValue(merged));
    }

    @Test
    public void example3Test() {
        MergeTwoSortedLists mergeTwoSortedLists = new MergeTwoSortedLists();
        ListNode first = ListNode.toNodeList(new int[]{});
        ListNode second = ListNode.toNodeList(new int[]{0});
        ListNode merged = mergeTwoSortedLists.mergeTwoLists(first, second);
        Assert.assertArrayEquals(new int[]{0}, ListNode.toArrayValue(merged));
    }
}
