package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class ShortestPathToGetFoodTest {

    @Test
    public void example1Test() {
        ShortestPathToGetFood shortestPathToGetFood = new ShortestPathToGetFood();
        char[][] grid = new char[][]{{'X','X','X','X','X','X'},{'X','*','O','O','O','X'},{'X','O','O','#','O','X'},{'X','X','X','X','X','X'}};
        Assert.assertEquals(3, shortestPathToGetFood.getFood(grid));
    }


    @Test
    public void example2Test() {
        ShortestPathToGetFood shortestPathToGetFood = new ShortestPathToGetFood();
        char[][] grid = new char[][]{{'X','X','X','X','X'},{'X','*','X','O','X'},{'X','O','X','#','X'},{'X','X','X','X','X'}};
        Assert.assertEquals(-1, shortestPathToGetFood.getFood(grid));
    }

    @Test
    public void example3Test() {
        ShortestPathToGetFood shortestPathToGetFood = new ShortestPathToGetFood();
        char[][] grid = new char[][]{{'X','X','X','X','X','X','X','X'},{'X','*','O','X','O','#','O','X'},{'X','O','O','X','O','O','X','X'},{'X','O','O','O','O','#','O','X'},{'X','X','X','X','X','X','X','X'}};
        Assert.assertEquals(6, shortestPathToGetFood.getFood(grid));
    }


    @Test
    public void example4Test() {
        ShortestPathToGetFood shortestPathToGetFood = new ShortestPathToGetFood();
        char[][] grid = new char[][]{{'O','*'},{'#','O'}};
        Assert.assertEquals(2, shortestPathToGetFood.getFood(grid));
    }
}
