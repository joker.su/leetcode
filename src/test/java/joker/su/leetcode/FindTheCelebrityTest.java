package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class FindTheCelebrityTest {

    @Test
    public void example1Test() {
        FindTheCelebrity tester = new FindTheCelebrity(new int[][]{{1, 1, 0}, {0, 1, 0}, {1, 1, 1}});
        Assert.assertEquals(1, tester.findCelebrity(3));
    }

    @Test
    public void example2Test() {
        FindTheCelebrity tester = new FindTheCelebrity(new int[][]{{1, 0, 1}, {1, 1, 0}, {0, 1, 1}});
        Assert.assertEquals(-1, tester.findCelebrity(3));
    }
}
