package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;


public class BinarySearchTest {

    @Test
    public void middleTest() {
        BinarySearch binarySearch = new BinarySearch();
        int result = binarySearch.search(new int[]{-1, 0, 2, 4, 5}, 2);
        Assert.assertEquals(2, result);
    }

    @Test
    public void offTest() {
        BinarySearch binarySearch = new BinarySearch();
        int result = binarySearch.search(new int[]{-1, 0, 2, 4, 5}, 1);
        Assert.assertEquals(-1, result);
    }

    @Test
    public void startTest() {
        BinarySearch binarySearch = new BinarySearch();
        int result = binarySearch.search(new int[]{-1, 0, 2, 4, 5}, -1);
        Assert.assertEquals(0, result);
    }

    @Test
    public void endTest() {
        BinarySearch binarySearch = new BinarySearch();
        int result = binarySearch.search(new int[]{-1, 0, 2, 4, 5}, 5);
        Assert.assertEquals(4, result);

    }

    @Test
    public void singleTest() {
        BinarySearch binarySearch = new BinarySearch();
        int result = binarySearch.search(new int[]{5}, 5);
        Assert.assertEquals(0, result);
    }

    @Test
    public void evenTest() {
        BinarySearch binarySearch = new BinarySearch();
        int result = binarySearch.search(new int[]{-1, 0, 3, 5, 9, 12}, 12);
        Assert.assertEquals(5, result);
    }
}
