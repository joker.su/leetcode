package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class HouseRobberTest {

    @Test
    public void example1Test() {
        HouseRobber houseRobber = new HouseRobber();
        int result = houseRobber.rob(new int[]{1});
        Assert.assertEquals(1, result);
    }

    @Test
    public void example2Test() {
        HouseRobber houseRobber = new HouseRobber();
        int result = houseRobber.rob(new int[]{});
        Assert.assertEquals(0, result);
    }

    @Test
    public void example3Test() {
        HouseRobber houseRobber = new HouseRobber();
        int result = houseRobber.rob(new int[]{1, 2, 3, 1});
        Assert.assertEquals(4, result);
    }

    @Test
    public void example4Test() {
        HouseRobber houseRobber = new HouseRobber();
        int result = houseRobber.rob(new int[]{2, 7, 9, 3, 1});
        Assert.assertEquals(12, result);
    }

    @Test
    public void example5Test() {
        HouseRobber houseRobber = new HouseRobber();
        int result = houseRobber.rob(new int[]{2, 1});
        Assert.assertEquals(2, result);
    }
}
