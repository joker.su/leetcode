package joker.su.leetcode;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;


public class FindAllAnagramsInStringTest {

    @Test
    public void example1Test() {
        FindAllAnagramsInString tester = new FindAllAnagramsInString();
        List<Integer> result = tester.findAnagrams("cbaebabacd", "abc");
        List<Integer> standard = List.of(0, 6);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example2Test() {
        FindAllAnagramsInString tester = new FindAllAnagramsInString();
        List<Integer> result = tester.findAnagrams("abab", "ab");
        List<Integer> standard = List.of(0, 1, 2);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example3Test() {
        FindAllAnagramsInString tester = new FindAllAnagramsInString();
        List<Integer> result = tester.findAnagrams("aababa", "aba");
        List<Integer> standard = List.of(0, 1, 3);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

}
