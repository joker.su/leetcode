package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

import joker.su.leetcode.api.NestedInteger;

public class NestedListWeightSumTest {

    @Test
    public void example1Test() {
        NestedListWeightSum nestedListWeightSum = new NestedListWeightSum();
        NestedInteger testValue = NestedInteger.of("[[1,1],2,[1,1]]");
        int result = nestedListWeightSum.depthSum(testValue.getList());
        Assert.assertEquals( 10, result);
    }

    @Test
    public void example2Test() {
        NestedListWeightSum nestedListWeightSum = new NestedListWeightSum();
        NestedInteger testValue = NestedInteger.of("[1,[4,[6]]]");
        int result = nestedListWeightSum.depthSum(testValue.getList());
        Assert.assertEquals( 27, result);
    }

    @Test
    public void example3Test() {
        NestedListWeightSum nestedListWeightSum = new NestedListWeightSum();
        NestedInteger testValue = NestedInteger.of("[0]");
        int result = nestedListWeightSum.depthSum(testValue.getList());
        Assert.assertEquals( 0, result);
    }
}
