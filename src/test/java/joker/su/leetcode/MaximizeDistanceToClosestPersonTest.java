package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class MaximizeDistanceToClosestPersonTest {

    @Test
    public void example1Test() {
        MaximizeDistanceToClosestPerson maximizeDistanceToClosestPerson = new MaximizeDistanceToClosestPerson();
        Assert.assertEquals(2, maximizeDistanceToClosestPerson.maxDistToClosest(new int[]{1,0,0,0,1,0,1}));
    }

    @Test
    public void example2Test() {
        MaximizeDistanceToClosestPerson maximizeDistanceToClosestPerson = new MaximizeDistanceToClosestPerson();
        Assert.assertEquals(3, maximizeDistanceToClosestPerson.maxDistToClosest(new int[]{1,0,0,0}));
    }

    @Test
    public void example3Test() {
        MaximizeDistanceToClosestPerson maximizeDistanceToClosestPerson = new MaximizeDistanceToClosestPerson();
        Assert.assertEquals(3, maximizeDistanceToClosestPerson.maxDistToClosest(new int[]{0,0,0,1}));
    }

    @Test
    public void example4Test() {
        MaximizeDistanceToClosestPerson maximizeDistanceToClosestPerson = new MaximizeDistanceToClosestPerson();
        Assert.assertEquals(1, maximizeDistanceToClosestPerson.maxDistToClosest(new int[]{0,1}));
    }

    @Test
    public void example5Test() {
        MaximizeDistanceToClosestPerson maximizeDistanceToClosestPerson = new MaximizeDistanceToClosestPerson();
        Assert.assertEquals(2, maximizeDistanceToClosestPerson.maxDistToClosest(new int[]{1,0,0,0,1,0,1}));
    }
}
