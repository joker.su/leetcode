package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

import joker.su.leetcode.api.ListNode;

public class DeleteNodeInLinkedListTest {

    @Test
    public void example1Test() {
        DeleteNodeInLinkedList deleteNodeInLinkedList = new DeleteNodeInLinkedList();
        ListNode root = ListNode.toNodeList(new int[]{4, 5, 1, 9});
        deleteNodeInLinkedList.deleteNode(root.findNextWithValue(5));
        Assert.assertArrayEquals(new int[]{4, 1, 9}, ListNode.toArrayValue(root));
    }

    @Test
    public void example2Test() {
        DeleteNodeInLinkedList deleteNodeInLinkedList = new DeleteNodeInLinkedList();
        ListNode root = ListNode.toNodeList(new int[]{4, 5, 1, 9});
        deleteNodeInLinkedList.deleteNode(root.findNextWithValue(1));
        Assert.assertArrayEquals(new int[]{4, 5, 9}, ListNode.toArrayValue(root));
    }
}
