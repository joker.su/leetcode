package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class BestTeamWithNoConflictsTest {

    @Test
    public void example1Test() {
        BestTeamWithNoConflicts tester = new BestTeamWithNoConflicts();
        Assert.assertEquals(34, tester.bestTeamScore(new int[]{1,3,5,10,15}, new int[]{1,2,3,4,5}));
    }

    @Test
    public void example2Test() {
        BestTeamWithNoConflicts tester = new BestTeamWithNoConflicts();
        Assert.assertEquals(16, tester.bestTeamScore(new int[]{4,5,4,5}, new int[]{2,1,2,1}));
    }

    @Test
    public void example3Test() {
        BestTeamWithNoConflicts tester = new BestTeamWithNoConflicts();
        Assert.assertEquals(6, tester.bestTeamScore(new int[]{1,2,3,5}, new int[]{8,9,10,1}));
    }
}
