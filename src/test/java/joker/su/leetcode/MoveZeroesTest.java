package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class MoveZeroesTest {

    @Test
    public void exampleTest() {
        MoveZeroes moveZeroes = new MoveZeroes();
        int[] result = new int[]{0, 1, 0, 3, 12};
        moveZeroes.moveZeroes(result);
        Assert.assertArrayEquals(new int[]{1, 3, 12, 0, 0}, result);
    }

    @Test
    public void noZeroTest() {
        MoveZeroes moveZeroes = new MoveZeroes();
        int[] result = new int[]{1, 2, 3, 4, 5};
        moveZeroes.moveZeroes(result);
        Assert.assertArrayEquals(new int[]{1, 2, 3, 4, 5}, result);
    }

    @Test
    public void emptyTest() {
        MoveZeroes moveZeroes = new MoveZeroes();
        int[] result = new int[]{};
        moveZeroes.moveZeroes(result);
        Assert.assertArrayEquals(new int[]{}, result);
    }

    @Test
    public void singleOneTest() {
        MoveZeroes moveZeroes = new MoveZeroes();
        int[] result = new int[]{1};
        moveZeroes.moveZeroes(result);
        Assert.assertArrayEquals(new int[]{1}, result);
    }

    @Test
    public void singleZeroTest() {
        MoveZeroes moveZeroes = new MoveZeroes();
        int[] result = new int[]{0};
        moveZeroes.moveZeroes(result);
        Assert.assertArrayEquals(new int[]{0}, result);
    }
}
