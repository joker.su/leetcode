package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class NumberOfIslandsTest {

    @Test
    public void example1Test() {
        NumberOfIslands numberOfIslands = new NumberOfIslands();
        int result = numberOfIslands.numIslands(
            new char[][]{{'1', '1', '1', '1', '0'},
                         {'1', '1', '0', '1', '0'},
                         {'1', '1', '0', '0', '0'},
                         {'0', '0', '0', '0', '0'}});
        Assert.assertEquals(1, result);
    }


    @Test
    public void example2Test() {
        NumberOfIslands numberOfIslands = new NumberOfIslands();
        int result = numberOfIslands.numIslands(
            new char[][]{{'1', '1', '0', '0', '0'},
                         {'1', '1', '0', '0', '0'},
                         {'0', '0', '1', '0', '0'},
                         {'0', '0', '0', '1', '1'}});
        Assert.assertEquals(3, result);
    }
}
