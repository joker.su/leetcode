package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class ClimbingStairsTest {

    @Test
    public void example0Test() {
        ClimbingStairs climbingStairs = new ClimbingStairs();
        int result = climbingStairs.climbStairs(0);
        Assert.assertEquals(1, result);
    }

    @Test
    public void example1Test() {
        ClimbingStairs climbingStairs = new ClimbingStairs();
        int result = climbingStairs.climbStairs(1);
        Assert.assertEquals(1, result);
    }

    @Test
    public void example2Test() {
        ClimbingStairs climbingStairs = new ClimbingStairs();
        int result = climbingStairs.climbStairs(2);
        Assert.assertEquals(2, result);
    }

    @Test
    public void example3Test() {
        ClimbingStairs climbingStairs = new ClimbingStairs();
        int result = climbingStairs.climbStairs(3);
        Assert.assertEquals(3, result);
    }

    @Test
    public void example4Test() {
        ClimbingStairs climbingStairs = new ClimbingStairs();
        int result = climbingStairs.climbStairs(4);
        Assert.assertEquals(5, result);
    }
}
