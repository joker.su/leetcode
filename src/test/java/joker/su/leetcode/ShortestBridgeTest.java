package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class ShortestBridgeTest {

    @Test
    public void example1Test() {
        ShortestBridge shortestBridge = new ShortestBridge();
        int[][] grid = new int[][]{{0, 1},
                                   {1, 0}};
        Assert.assertEquals(1, shortestBridge.shortestBridge(grid));
    }

    @Test
    public void example2Test() {
        ShortestBridge shortestBridge = new ShortestBridge();
        int[][] grid = new int[][]{{0, 1, 0},
                                   {0, 0, 0},
                                   {0, 0, 1}};
        Assert.assertEquals(2, shortestBridge.shortestBridge(grid));
    }

    @Test
    public void example3Test() {
        ShortestBridge shortestBridge = new ShortestBridge();
        int[][] grid = new int[][]{{1, 1, 1, 1, 1},
                                   {1, 0, 0, 0, 1},
                                   {1, 0, 1, 0, 1},
                                   {1, 0, 0, 0, 1},
                                   {1, 1, 1, 1, 1}};
        Assert.assertEquals(1, shortestBridge.shortestBridge(grid));
    }


    @Test
    public void example4Test() {
        ShortestBridge shortestBridge = new ShortestBridge();
        int[][] grid = new int[][]{{0, 0, 1, 0, 1},
                                   {0, 1, 1, 0, 1},
                                   {0, 1, 0, 0, 1},
                                   {0, 0, 0, 0, 0},
                                   {0, 0, 0, 0, 0}};
        Assert.assertEquals(1, shortestBridge.shortestBridge(grid));
    }
}
