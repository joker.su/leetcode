package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class MeetingRoomsIITest {
    @Test
    public void example1Test() {
        MeetingRoomsII meetingRooms = new MeetingRoomsII();
        Assert.assertEquals(2, meetingRooms.minMeetingRooms(new int[][]{{0,30}, {5,10}, {15,20}}));
    }

    @Test
    public void example2Test() {
        MeetingRoomsII meetingRooms = new MeetingRoomsII();
        Assert.assertEquals(1, meetingRooms.minMeetingRooms(new int[][]{{7,10}, {2,4}}));
    }
}
