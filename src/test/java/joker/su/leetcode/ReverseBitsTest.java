package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class ReverseBitsTest {

    @Test
    public void example1Test() {
        ReverseBits reverseBits = new ReverseBits();
        int result = reverseBits.reverseBits(0);
        Assert.assertEquals(0, result);
    }

    @Test
    public void example2Test() {
        ReverseBits reverseBits = new ReverseBits();
        int result = reverseBits.reverseBits(43261596);
        Assert.assertEquals(964176192, result);
    }

    @Test
    public void example3Test() {
        ReverseBits reverseBits = new ReverseBits();
        int result = reverseBits.reverseBits(-3);
        Assert.assertEquals(-1073741825, result);
    }


}
