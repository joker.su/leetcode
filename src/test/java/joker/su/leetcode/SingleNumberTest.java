package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class SingleNumberTest {

    @Test
    public void example1Test() {
        SingleNumber singleNumber = new SingleNumber();
        int result = singleNumber.singleNumber(new int[]{2, 2, 1});
        Assert.assertEquals(1, result);
    }

    @Test
    public void example2Test() {
        SingleNumber singleNumber = new SingleNumber();
        int result = singleNumber.singleNumber(new int[]{4, 1, 2, 1, 2});
        Assert.assertEquals(4, result);
    }

    @Test
    public void example3Test() {
        SingleNumber singleNumber = new SingleNumber();
        int result = singleNumber.singleNumber(new int[]{1});
        Assert.assertEquals(1, result);
    }

}
