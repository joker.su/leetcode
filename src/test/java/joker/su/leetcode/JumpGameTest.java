package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class JumpGameTest {

    @Test
    public void example1Test() {
        JumpGame jumpGame = new JumpGame();
        Assert.assertTrue(jumpGame.canJump(new int[]{2,3,1,1,4}));
    }

    @Test
    public void example2Test() {
        JumpGame jumpGame = new JumpGame();
        Assert.assertFalse(jumpGame.canJump(new int[]{3,2,1,0,4}));
    }
}
