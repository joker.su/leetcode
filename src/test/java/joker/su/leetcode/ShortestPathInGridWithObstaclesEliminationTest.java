package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class ShortestPathInGridWithObstaclesEliminationTest {

    @Test
    public void example1Test() {
        ShortestPathInGridWithObstaclesElimination shortestPathInGrid = new ShortestPathInGridWithObstaclesElimination();
        int[][] grid = new int[][]{{0,0,0},{1,1,0},{0,0,0},{0,1,1},{0,0,0}};
        Assert.assertEquals(6, shortestPathInGrid.shortestPath(grid, 1));
    }

    @Test
    public void example2Test() {
        ShortestPathInGridWithObstaclesElimination shortestPathInGrid = new ShortestPathInGridWithObstaclesElimination();
        int[][] grid = new int[][]{{0,1,1},{1,1,1},{1,0,0}};
        Assert.assertEquals(-1, shortestPathInGrid.shortestPath(grid, 1));
    }
}
