package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;


public class SearchInsertPositionTest {

    @Test
    public void middleTest() {
        SearchInsertPosition searchInsertPosition = new SearchInsertPosition();
        int result = searchInsertPosition.searchInsert(new int[]{-1, 0, 2, 4, 5}, 2);
        Assert.assertEquals(2, result);
    }

    @Test
    public void offTest() {
        SearchInsertPosition searchInsertPosition = new SearchInsertPosition();
        int result = searchInsertPosition.searchInsert(new int[]{-1, 0, 2, 4, 5}, 1);
        Assert.assertEquals(2, result);
    }

    @Test
    public void beforeTest() {
        SearchInsertPosition searchInsertPosition = new SearchInsertPosition();
        int result = searchInsertPosition.searchInsert(new int[]{-1, 0, 2, 4, 5}, -2);
        Assert.assertEquals(0, result);
    }

    @Test
    public void afterTest() {
        SearchInsertPosition searchInsertPosition = new SearchInsertPosition();
        int result = searchInsertPosition.searchInsert(new int[]{-1, 0, 2, 4, 5}, 10);
        Assert.assertEquals(5, result);
    }

    @Test
    public void startTest() {
        SearchInsertPosition searchInsertPosition = new SearchInsertPosition();
        int result = searchInsertPosition.searchInsert(new int[]{-1, 0, 2, 4, 5}, -1);
        Assert.assertEquals(0, result);
    }

    @Test
    public void endTest() {
        SearchInsertPosition searchInsertPosition = new SearchInsertPosition();
        int result = searchInsertPosition.searchInsert(new int[]{-1, 0, 2, 4, 5}, 5);
        Assert.assertEquals(4, result);
    }

    @Test
    public void singleTest() {
        SearchInsertPosition searchInsertPosition = new SearchInsertPosition();
        int result = searchInsertPosition.searchInsert(new int[]{5}, 5);
        Assert.assertEquals(0, result);
    }

    @Test
    public void evenTest() {
        SearchInsertPosition searchInsertPosition = new SearchInsertPosition();
        int result = searchInsertPosition.searchInsert(new int[]{-1, 0, 3, 5, 9, 12}, 12);
        Assert.assertEquals(5, result);
    }
}
