package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class SumOfSubarrayMinimumsTest {

    @Test
    public void example1Test() {
        SumOfSubarrayMinimums sumOfSubarrayMinimums = new SumOfSubarrayMinimums();
        Assert.assertEquals(17, sumOfSubarrayMinimums.sumSubarrayMins(new int[]{3,1,2,4}));
    }

    @Test
    public void example2Test() {
        SumOfSubarrayMinimums sumOfSubarrayMinimums = new SumOfSubarrayMinimums();
        Assert.assertEquals(444, sumOfSubarrayMinimums.sumSubarrayMins(new int[]{11,81,94,43,3}));
    }
}
