package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class SubarraySumEqualsKTest {

    @Test
    public void example11Test() {
        SubarraySumEqualsK subarray = new SubarraySumEqualsK();
        int result = subarray.subarraySum(new int[]{1,1,1}, 2);
        Assert.assertEquals(2, result);
    }

    @Test
    public void example12Test() {
        SubarraySumEqualsK subarray = new SubarraySumEqualsK();
        int result = subarray.subarraySum(new int[]{1,2,3}, 3);
        Assert.assertEquals(2, result);
    }

    @Test
    public void example21Test() {
        SubarraySumEqualsK subarray = new SubarraySumEqualsK();
        int result = subarray.subarraySumWithoutSpace(new int[]{1,1,1}, 2);
        Assert.assertEquals(2, result);
    }

    @Test
    public void example32Test() {
        SubarraySumEqualsK subarray = new SubarraySumEqualsK();
        int result = subarray.subarraySumWithoutSpace(new int[]{1,2,3}, 3);
        Assert.assertEquals(2, result);
    }
}
