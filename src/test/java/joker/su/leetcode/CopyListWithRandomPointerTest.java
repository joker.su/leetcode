package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

import joker.su.leetcode.api.ListNode;

public class CopyListWithRandomPointerTest {

    @Test
    public void example1Test() {
        CopyListWithRandomPointer copyListWithRandomPointer = new CopyListWithRandomPointer();
        ListNode root = ListNode.toNodeList(new Integer[][]{{7, null}, {13, 0}, {11, 4}, {10, 2}, {1, 0}});
        ListNode result = copyListWithRandomPointer.copyRandomList(root);
        Assert.assertNotNull(result);
        Assert.assertArrayEquals(new Integer[][]{{7, null}, {13, 0}, {11, 4}, {10, 2}, {1, 0}}, ListNode.toArrayValueWithRandom(result));
    }

    @Test
    public void example2Test() {
        CopyListWithRandomPointer copyListWithRandomPointer = new CopyListWithRandomPointer();
        ListNode root = ListNode.toNodeList(new Integer[][]{{1, 1}, {2, 1}});
        ListNode result = copyListWithRandomPointer.copyRandomList(root);
        Assert.assertNotNull(result);
        Assert.assertArrayEquals(new Integer[][]{{1, 1}, {2, 1}}, ListNode.toArrayValueWithRandom(result));
    }

    @Test
    public void example3Test() {
        CopyListWithRandomPointer copyListWithRandomPointer = new CopyListWithRandomPointer();
        ListNode root = ListNode.toNodeList(new Integer[][]{{3, null}, {3, 0}, {3, null}});
        ListNode result = copyListWithRandomPointer.copyRandomList(root);
        Assert.assertNotNull(result);
        Assert.assertArrayEquals(new Integer[][]{{3, null}, {3, 0}, {3, null}}, ListNode.toArrayValueWithRandom(result));
    }
}
