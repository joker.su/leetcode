package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class Search2DMatrixTest {

    @Test
    public void example1Test() {
        Search2DMatrix search2DMatrix = new Search2DMatrix();
        Assert.assertTrue(search2DMatrix.searchMatrix(new int[][]{{1, 3, 5, 7}, {10, 11, 16, 20}, {23, 30, 34, 60}}, 3));
    }

    @Test
    public void example2Test() {
        Search2DMatrix search2DMatrix = new Search2DMatrix();
        Assert.assertTrue(search2DMatrix.searchMatrix(new int[][]{{1, 3, 5, 7}, {10, 11, 16, 20}, {23, 30, 34, 60}}, 16));
    }

    @Test
    public void example3Test() {
        Search2DMatrix search2DMatrix = new Search2DMatrix();
        Assert.assertFalse(search2DMatrix.searchMatrix(new int[][]{{1, 3, 5, 7}, {10, 11, 16, 20}, {23, 30, 34, 60}}, 13));
    }
}
