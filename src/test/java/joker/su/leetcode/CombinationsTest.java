package joker.su.leetcode;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CombinationsTest {

    @Test
    public void example1Test() {
        Combinations combinations = new Combinations();
        List<List<Integer>> result = combinations.combine(4, 2);
        Integer[][] standard = new Integer[][]{{2, 4}, {3, 4}, {2, 3}, {1, 2}, {1, 3}, {1, 4}};
        Assert.assertTrue(CollectionUtils.isEqualCollection(
            Arrays.stream(standard).map(Arrays::asList).collect(Collectors.toList()),
            result));
    }

    @Test
    public void example2Test() {
        Combinations combinations = new Combinations();
        List<List<Integer>> result = combinations.combine(1, 1);
        Integer[][] standard = new Integer[][]{{1}};
        Assert.assertTrue(CollectionUtils.isEqualCollection(
            Arrays.stream(standard).map(Arrays::asList).collect(Collectors.toList()),
            result));
    }
}
