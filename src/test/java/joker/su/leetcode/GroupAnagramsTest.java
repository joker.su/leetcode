package joker.su.leetcode;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class GroupAnagramsTest {

    @Test
    public void example1Test() {
        GroupAnagrams groupAnagrams = new GroupAnagrams();
        List<List<String>> result = groupAnagrams.groupAnagrams(new String[]{"eat","tea","tan","ate","nat","bat"});
        List<List<String>> standard = Arrays.asList(
            Arrays.asList("bat"),
            Arrays.asList("nat","tan"),
            Arrays.asList("ate","eat","tea")
        );
        Assert.assertTrue(standard.stream()
            .allMatch(standardColl -> result.stream().anyMatch(resultColl -> CollectionUtils.isEqualCollection(standardColl, resultColl))));
        Assert.assertTrue(result.stream()
            .allMatch(resultColl -> standard.stream().anyMatch(standardColl -> CollectionUtils.isEqualCollection(standardColl, resultColl))));
    }

    @Test
    public void example2Test() {
        GroupAnagrams groupAnagrams = new GroupAnagrams();
        List<List<String>> result = groupAnagrams.groupAnagrams(new String[]{""});
        List<List<String>> standard = Arrays.asList(
            Arrays.asList("")
        );
        Assert.assertTrue(standard.stream()
            .allMatch(standardColl -> result.stream().anyMatch(resultColl -> CollectionUtils.isEqualCollection(standardColl, resultColl))));
        Assert.assertTrue(result.stream()
            .allMatch(resultColl -> standard.stream().anyMatch(standardColl -> CollectionUtils.isEqualCollection(standardColl, resultColl))));
    }

    @Test
    public void example3Test() {
        GroupAnagrams groupAnagrams = new GroupAnagrams();
        List<List<String>> result = groupAnagrams.groupAnagrams(new String[]{"a"});
        List<List<String>> standard = Arrays.asList(
            Arrays.asList("a")
        );
        Assert.assertTrue(standard.stream()
            .allMatch(standardColl -> result.stream().anyMatch(resultColl -> CollectionUtils.isEqualCollection(standardColl, resultColl))));
        Assert.assertTrue(result.stream()
            .allMatch(resultColl -> standard.stream().anyMatch(standardColl -> CollectionUtils.isEqualCollection(standardColl, resultColl))));
    }
}
