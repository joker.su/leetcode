package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class ShortestWordDistanceIITest {

    @Test
    public void example1Test() {
        ShortestWordDistanceII wordDistance = new ShortestWordDistanceII(new String[]{"practice", "makes", "perfect", "coding", "makes"});
        Assert.assertEquals(3, wordDistance.shortest("coding", "practice"));
        Assert.assertEquals(1, wordDistance.shortest("makes", "coding"));
    }

    @Test
    public void example2Test() {
        ShortestWordDistanceII wordDistance = new ShortestWordDistanceII(new String[]{"a", "b", "a", "c", "d", "c", "e"});
        Assert.assertEquals(4, wordDistance.shortest("a", "e"));
        Assert.assertEquals(1, wordDistance.shortest("c", "e"));
    }
}
