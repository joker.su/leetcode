package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import joker.su.leetcode.api.NestedInteger;

public class FlattenNestedListIteratorTest {

    @Test
    public void example1Test() {
        NestedInteger testValue = NestedInteger.of("[[1,1],2,[1,1]]");
        List<Integer> result = Arrays.asList(1, 1, 2, 1, 1);
        FlattenNestedListIterator flattenNestedListIterator = new FlattenNestedListIterator(testValue.getList());
        for (Integer element : result) {
            Assert.assertTrue(flattenNestedListIterator.hasNext());
            Assert.assertEquals(element, flattenNestedListIterator.next());
        }
    }

    @Test
    public void example2Test() {
        NestedInteger testValue = NestedInteger.of("[1,[4,[6]]]");
        List<Integer> result = Arrays.asList(1, 4, 6);
        FlattenNestedListIterator flattenNestedListIterator = new FlattenNestedListIterator(testValue.getList());
        for (Integer element : result) {
            Assert.assertTrue(flattenNestedListIterator.hasNext());
            Assert.assertEquals(element, flattenNestedListIterator.next());
        }
    }
}
