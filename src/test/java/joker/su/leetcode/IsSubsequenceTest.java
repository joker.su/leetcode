package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class IsSubsequenceTest {

    @Test
    public void example1Test() {
        IsSubsequence tester = new IsSubsequence();
        Assert.assertTrue(tester.isSubsequence("abc", "ahbgdc"));
    }

    @Test
    public void example2Test() {
        IsSubsequence tester = new IsSubsequence();
        Assert.assertFalse(tester.isSubsequence("axc", "ahbgdc"));
    }
}
