package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class Sum3WithMultiplicityTest {

    @Test
    public void example1Test() {
        Sum3WithMultiplicity sum = new Sum3WithMultiplicity();
        Assert.assertEquals(20, sum.threeSumMulti(new int[]{1, 1, 2, 2, 3, 3, 4, 4, 5, 5}, 8));
    }

    @Test
    public void example2Test() {
        Sum3WithMultiplicity sum = new Sum3WithMultiplicity();
        Assert.assertEquals(12, sum.threeSumMulti(new int[]{1, 1, 2, 2, 2, 2}, 5));
    }

    @Test
    public void example3Test() {
        Sum3WithMultiplicity sum = new Sum3WithMultiplicity();
        Assert.assertEquals(1, sum.threeSumMulti(new int[]{2, 1, 3}, 6));
    }
}
