package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class MedianOfTwoSortedArraysTest {

    @Test
    public void example1Test() {
        MedianOfTwoSortedArrays median = new MedianOfTwoSortedArrays();
        double result = median.findMedianSortedArrays(new int[]{1, 3}, new int[]{2});
        Assert.assertEquals(2.0d, result, 0.1d);
    }

    @Test
    public void example2Test() {
        MedianOfTwoSortedArrays median = new MedianOfTwoSortedArrays();
        double result = median.findMedianSortedArrays(new int[]{1, 2}, new int[]{3, 4});
        Assert.assertEquals(2.5d, result, 0.1d);
    }

    @Test
    public void example3Test() {
        MedianOfTwoSortedArrays median = new MedianOfTwoSortedArrays();
        double result = median.findMedianSortedArrays(new int[]{1, 2}, new int[]{});
        Assert.assertEquals(1.5d, result, 0.1d);
    }
}
