package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class GasStationTest {

    @Test
    public void example1Test() {
        GasStation gasStation = new GasStation();
        Assert.assertEquals(3, gasStation.canCompleteCircuit(new int[]{1,2,3,4,5}, new int[]{3,4,5,1,2}));
    }

    @Test
    public void example2Test() {
        GasStation gasStation = new GasStation();
        Assert.assertEquals(-1, gasStation.canCompleteCircuit(new int[]{2,3,4}, new int[]{3,4,3}));
    }
}
