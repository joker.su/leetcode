package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class StampingTheSequenceTest {

    @Test
    public void example1Test() {
        StampingTheSequence stampingTheSequence = new StampingTheSequence();
        int[] result = stampingTheSequence.movesToStamp("abc", "ababc");
        Assert.assertArrayEquals(new int[]{0, 2}, result);
    }

    @Test
    public void example2Test() {
        StampingTheSequence stampingTheSequence = new StampingTheSequence();
        int[] result = stampingTheSequence.movesToStamp("abca", "aabcaca");
        Assert.assertArrayEquals(new int[]{0, 3, 1}, result);
    }

    @Test
    public void example3Test() {
        StampingTheSequence stampingTheSequence = new StampingTheSequence();
        int[] result = stampingTheSequence.movesToStamp("mda", "mdadddaaaa");
        Assert.assertArrayEquals(new int[]{}, result);
    }
}
