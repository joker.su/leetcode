package joker.su.leetcode;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class LetterCasePermutationTest {

    @Test
    public void example1Test() {
        LetterCasePermutation letterCasePermutation = new LetterCasePermutation();
        List<String> result = letterCasePermutation.letterCasePermutation("a1b2");
        List<String> standard = Arrays.asList("a1b2", "a1B2", "A1b2", "A1B2");
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example2Test() {
        LetterCasePermutation letterCasePermutation = new LetterCasePermutation();
        List<String> result = letterCasePermutation.letterCasePermutation("3z4");
        List<String> standard = Arrays.asList("3z4", "3Z4");
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example3Test() {
        LetterCasePermutation letterCasePermutation = new LetterCasePermutation();
        List<String> result = letterCasePermutation.letterCasePermutation("");
        List<String> standard = List.of("");
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example4Test() {
        LetterCasePermutation letterCasePermutation = new LetterCasePermutation();
        List<String> result = letterCasePermutation.letterCasePermutation("A");
        List<String> standard = List.of("A", "a");
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example5Test() {
        LetterCasePermutation letterCasePermutation = new LetterCasePermutation();
        List<String> result = letterCasePermutation.letterCasePermutation("so");
        List<String> standard = List.of("so", "So", "sO", "SO");
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }
}
