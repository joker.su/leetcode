package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class MergeSortedArrayTest {

    @Test
    public void example1Test() {
        MergeSortedArray mergeSortedArray = new MergeSortedArray();
        int[] nums1 = new int[]{1,2,3,0,0,0};
        int[] nums2 = new int[]{2,5,6};
        mergeSortedArray.merge(nums1, 3, nums2, 3);
        Assert.assertArrayEquals(new int[]{1,2,2,3,5,6}, nums1);
    }

    @Test
    public void example2Test() {
        MergeSortedArray mergeSortedArray = new MergeSortedArray();
        int[] nums1 = new int[]{1};
        int[] nums2 = new int[]{};
        mergeSortedArray.merge(nums1, 1, nums2, 0);
        Assert.assertArrayEquals(new int[]{1}, nums1);
    }

    @Test
    public void example3Test() {
        MergeSortedArray mergeSortedArray = new MergeSortedArray();
        int[] nums1 = new int[]{0};
        int[] nums2 = new int[]{1};
        mergeSortedArray.merge(nums1, 0, nums2, 1);
        Assert.assertArrayEquals(new int[]{1}, nums1);
    }
}
