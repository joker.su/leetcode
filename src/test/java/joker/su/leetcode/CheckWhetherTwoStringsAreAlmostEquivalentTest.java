package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class CheckWhetherTwoStringsAreAlmostEquivalentTest {

    @Test
    public void example1Test() {
        CheckWhetherTwoStringsAreAlmostEquivalent tester = new CheckWhetherTwoStringsAreAlmostEquivalent();
        Assert.assertFalse(tester.checkAlmostEquivalent("aaaa", "bccb"));
    }

    @Test
    public void example2Test() {
        CheckWhetherTwoStringsAreAlmostEquivalent tester = new CheckWhetherTwoStringsAreAlmostEquivalent();
        Assert.assertTrue(tester.checkAlmostEquivalent("abcdeef", "abaaacc"));
    }

    @Test
    public void example3Test() {
        CheckWhetherTwoStringsAreAlmostEquivalent tester = new CheckWhetherTwoStringsAreAlmostEquivalent();
        Assert.assertTrue(tester.checkAlmostEquivalent("cccddabba", "babababab"));
    }
}
