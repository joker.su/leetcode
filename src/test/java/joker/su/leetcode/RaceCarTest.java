package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class RaceCarTest {

    @Test
    public void example1Test() {
        RaceCar raceCar = new RaceCar();
        int result = raceCar.racecar(3);
        Assert.assertEquals(2, result);
    }


    @Test
    public void example2Test() {
        RaceCar raceCar = new RaceCar();
        int result = raceCar.racecar(6);
        Assert.assertEquals(5, result);
    }

    @Test
    public void example3Test() {
        RaceCar raceCar = new RaceCar();
        int result = raceCar.racecar(17);
        Assert.assertEquals(9, result);
    }

    @Test
    public void example4Test() {
        RaceCar raceCar = new RaceCar();
        int result = raceCar.racecar(10000);
        Assert.assertEquals(45, result);
    }

    @Test
    public void example5Test() {
        RaceCar raceCar = new RaceCar();
        int result = raceCar.racecar(1);
        Assert.assertEquals(1, result);
    }

}
