package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

import joker.su.leetcode.api.ListNode;

public class AddTwoNumbersTest {

    @Test
    public void example1Test() {
        AddTwoNumbers addTwoNumbers = new AddTwoNumbers();
        ListNode list1 = ListNode.toNodeList(new int[]{2, 4, 3});
        ListNode list2 = ListNode.toNodeList(new int[]{5, 6, 4});
        ListNode result = addTwoNumbers.addTwoNumbers(list1, list2);
        Assert.assertArrayEquals(new int[]{7,0,8}, ListNode.toArrayValue(result));
    }

    @Test
    public void example2Test() {
        AddTwoNumbers addTwoNumbers = new AddTwoNumbers();
        ListNode list1 = ListNode.toNodeList(new int[]{0});
        ListNode list2 = ListNode.toNodeList(new int[]{0});
        ListNode result = addTwoNumbers.addTwoNumbers(list1, list2);
        Assert.assertArrayEquals(new int[]{0}, ListNode.toArrayValue(result));
    }

    @Test
    public void example3Test() {
        AddTwoNumbers addTwoNumbers = new AddTwoNumbers();
        ListNode list1 = ListNode.toNodeList(new int[]{9, 9, 9, 9, 9, 9, 9});
        ListNode list2 = ListNode.toNodeList(new int[]{9, 9, 9, 9});
        ListNode result = addTwoNumbers.addTwoNumbers(list1, list2);
        Assert.assertArrayEquals(new int[]{8,9,9,9,0,0,0,1}, ListNode.toArrayValue(result));
    }
}
