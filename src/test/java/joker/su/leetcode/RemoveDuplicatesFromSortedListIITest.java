package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

import joker.su.leetcode.api.ListNode;

public class RemoveDuplicatesFromSortedListIITest {

    @Test
    public void example1Test() {
        RemoveDuplicatesFromSortedListII removeDuplicatesFromSortedList = new RemoveDuplicatesFromSortedListII();
        ListNode result = removeDuplicatesFromSortedList.deleteDuplicates(ListNode.toNodeList(new int[]{1,2,3,3,4,4,5}));
        Assert.assertArrayEquals(new int[]{1,2,5}, ListNode.toArrayValue(result));
    }

    @Test
    public void example2Test() {
        RemoveDuplicatesFromSortedListII removeDuplicatesFromSortedList = new RemoveDuplicatesFromSortedListII();
        ListNode result = removeDuplicatesFromSortedList.deleteDuplicates(ListNode.toNodeList(new int[]{1,1,1,2,3}));
        Assert.assertArrayEquals(new int[]{2,3}, ListNode.toArrayValue(result));
    }

    @Test
    public void example3Test() {
        RemoveDuplicatesFromSortedListII removeDuplicatesFromSortedList = new RemoveDuplicatesFromSortedListII();
        ListNode result = removeDuplicatesFromSortedList.deleteDuplicates(ListNode.toNodeList(new int[]{1,2,2}));
        Assert.assertArrayEquals(new int[]{1}, ListNode.toArrayValue(result));
    }

    @Test
    public void example4Test() {
        RemoveDuplicatesFromSortedListII removeDuplicatesFromSortedList = new RemoveDuplicatesFromSortedListII();
        ListNode result = removeDuplicatesFromSortedList.deleteDuplicates(ListNode.toNodeList(new int[]{}));
        Assert.assertArrayEquals(new int[]{}, ListNode.toArrayValue(result));
    }
}
