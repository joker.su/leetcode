package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class ReverseWordsInStringTest {

    @Test
    public void exampleTest() {
        ReverseWordsInString reverseWordsInString = new ReverseWordsInString();
        String result = reverseWordsInString.reverseWords("Let's take LeetCode contest");
        Assert.assertEquals("s'teL ekat edoCteeL tsetnoc", result);
    }

    @Test
    public void exampleSecondTest() {
        ReverseWordsInString reverseWordsInString = new ReverseWordsInString();
        String result = reverseWordsInString.reverseWords("God Ding");
        Assert.assertEquals("doG gniD", result);
    }

    @Test
    public void singleWordTest() {
        ReverseWordsInString reverseWordsInString = new ReverseWordsInString();
        String result = reverseWordsInString.reverseWords("LeetCode");
        Assert.assertEquals("edoCteeL", result);
    }
}
