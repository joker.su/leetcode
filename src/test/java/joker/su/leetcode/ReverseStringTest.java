package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class ReverseStringTest {

    @Test
    public void exampleTest() {
        ReverseString reverseString = new ReverseString();
        char[] result = new char[]{'h', 'e', 'l', 'l', 'o'};
        reverseString.reverseString(result);
        Assert.assertArrayEquals(new char[]{'o', 'l', 'l', 'e', 'h'}, result);
    }

    @Test
    public void exampleSecondTest() {
        ReverseString reverseString = new ReverseString();
        char[] result = new char[]{'H', 'a', 'n', 'n', 'a', 'h'};
        reverseString.reverseString(result);
        Assert.assertArrayEquals(new char[]{'h', 'a', 'n', 'n', 'a', 'H'}, result);
    }

}
