package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class SearchRotatedSortedArrayTest {

    @Test
    public void example1Test() {
        SearchRotatedSortedArray searchRotatedSortedArray = new SearchRotatedSortedArray();
        Assert.assertEquals(4, searchRotatedSortedArray.search(new int[]{4, 5, 6, 7, 0, 1, 2}, 0));
    }

    @Test
    public void example2Test() {
        SearchRotatedSortedArray searchRotatedSortedArray = new SearchRotatedSortedArray();
        Assert.assertEquals(-1, searchRotatedSortedArray.search(new int[]{4, 5, 6, 7, 0, 1, 2}, 3));
    }

    @Test
    public void example3Test() {
        SearchRotatedSortedArray searchRotatedSortedArray = new SearchRotatedSortedArray();
        Assert.assertEquals(-1, searchRotatedSortedArray.search(new int[]{1}, 0));
    }

    @Test
    public void example31Test() {
        SearchRotatedSortedArray searchRotatedSortedArray = new SearchRotatedSortedArray();
        Assert.assertEquals(-1, searchRotatedSortedArray.search(new int[]{1}, 2));
    }

    @Test
    public void example4Test() {
        SearchRotatedSortedArray searchRotatedSortedArray = new SearchRotatedSortedArray();
        Assert.assertEquals(-1, searchRotatedSortedArray.search(new int[]{}, 0));
    }

    @Test
    public void example5Test() {
        SearchRotatedSortedArray searchRotatedSortedArray = new SearchRotatedSortedArray();
        Assert.assertEquals(0, searchRotatedSortedArray.search(new int[]{0, 1, 2, 4, 5, 6, 7}, 0));
    }

    @Test
    public void example6Test() {
        SearchRotatedSortedArray searchRotatedSortedArray = new SearchRotatedSortedArray();
        Assert.assertEquals(6, searchRotatedSortedArray.search(new int[]{0, 1, 2, 4, 5, 6, 7}, 7));
    }

    @Test
    public void example7Test() {
        SearchRotatedSortedArray searchRotatedSortedArray = new SearchRotatedSortedArray();
        Assert.assertEquals(-1, searchRotatedSortedArray.search(new int[]{1, 3}, 2));
    }

    @Test
    public void example8Test() {
        SearchRotatedSortedArray searchRotatedSortedArray = new SearchRotatedSortedArray();
        Assert.assertEquals(1, searchRotatedSortedArray.search(new int[]{1, 3}, 3));
    }

    @Test
    public void example9Test() {
        SearchRotatedSortedArray searchRotatedSortedArray = new SearchRotatedSortedArray();
        Assert.assertEquals(1, searchRotatedSortedArray.search(new int[]{3, 1}, 1));
    }

    @Test
    public void example10Test() {
        SearchRotatedSortedArray searchRotatedSortedArray = new SearchRotatedSortedArray();
        Assert.assertEquals(0, searchRotatedSortedArray.search(new int[]{3, 1}, 3));
    }

    @Test
    public void example11Test() {
        SearchRotatedSortedArray searchRotatedSortedArray = new SearchRotatedSortedArray();
        Assert.assertEquals(2, searchRotatedSortedArray.search(new int[]{3, 5, 1}, 1));
    }
}
