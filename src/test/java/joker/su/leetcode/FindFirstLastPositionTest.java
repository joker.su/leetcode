package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class FindFirstLastPositionTest {

    @Test
    public void example1Test() {
        FindFirstLastPosition firstLastPosition = new FindFirstLastPosition();
        int[] result = firstLastPosition.searchRange(new int[]{1, 1, 1, 1}, 2);
        Assert.assertArrayEquals(new int[]{-1, -1}, result);
    }


    @Test
    public void example2Test() {
        FindFirstLastPosition firstLastPosition = new FindFirstLastPosition();
        int[] result = firstLastPosition.searchRange(new int[]{5, 7, 7, 8, 8, 10}, 8);
        Assert.assertArrayEquals(new int[]{3, 4}, result);
    }

    @Test
    public void example3Test() {
        FindFirstLastPosition firstLastPosition = new FindFirstLastPosition();
        int[] result = firstLastPosition.searchRange(new int[]{5, 8, 8, 8, 8, 8, 8, 8, 8, 10}, 8);
        Assert.assertArrayEquals(new int[]{1, 8}, result);
    }

    @Test
    public void example4Test() {
        FindFirstLastPosition firstLastPosition = new FindFirstLastPosition();
        int[] result = firstLastPosition.searchRange(new int[]{5, 7, 7, 8, 8, 10}, 6);
        Assert.assertArrayEquals(new int[]{-1, -1}, result);
    }

    @Test
    public void example5Test() {
        FindFirstLastPosition firstLastPosition = new FindFirstLastPosition();
        int[] result = firstLastPosition.searchRange(new int[]{}, 8);
        Assert.assertArrayEquals(new int[]{-1, -1}, result);
    }

    @Test
    public void example6Test() {
        FindFirstLastPosition firstLastPosition = new FindFirstLastPosition();
        int[] result = firstLastPosition.searchRange(new int[]{8, 8, 8, 8, 8, 8, 8, 8, 10}, 8);
        Assert.assertArrayEquals(new int[]{0, 7}, result);
    }

    @Test
    public void example7Test() {
        FindFirstLastPosition firstLastPosition = new FindFirstLastPosition();
        int[] result = firstLastPosition.searchRange(new int[]{8, 8, 8, 8, 8, 8, 8, 8}, 8);
        Assert.assertArrayEquals(new int[]{0, 7}, result);
    }
}
