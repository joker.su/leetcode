package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

import joker.su.leetcode.api.Node;

public class LowestCommonAncestorOfABinaryTreeIIITest {

    @Test
    public void example1Test() {
        LowestCommonAncestorOfABinaryTreeIII lowestCommonAncestorOfABinaryTreeIII = new LowestCommonAncestorOfABinaryTreeIII();
        Node root = Node.toTree(new Integer[]{3, 5, 1, 6, 2, 0, 8, null, null, 7, 4});
        Assert.assertNotNull(root);
        Node result = lowestCommonAncestorOfABinaryTreeIII.lowestCommonAncestor(root.findNodeByValue(5), root.findNodeByValue(1));
        Assert.assertNotNull(result);
        Assert.assertEquals(3, result.getVal());
    }


    @Test
    public void example2Test() {
        LowestCommonAncestorOfABinaryTreeIII lowestCommonAncestorOfABinaryTreeIII = new LowestCommonAncestorOfABinaryTreeIII();
        Node root = Node.toTree(new Integer[]{3, 5, 1, 6, 2, 0, 8, null, null, 7, 4});
        Assert.assertNotNull(root);
        Node result = lowestCommonAncestorOfABinaryTreeIII.lowestCommonAncestor(root.findNodeByValue(5), root.findNodeByValue(4));
        Assert.assertNotNull(result);
        Assert.assertEquals(5, result.getVal());
    }

    @Test
    public void example3Test() {
        LowestCommonAncestorOfABinaryTreeIII lowestCommonAncestorOfABinaryTreeIII = new LowestCommonAncestorOfABinaryTreeIII();
        Node root = Node.toTree(new Integer[]{1, 2});
        Assert.assertNotNull(root);
        Node result = lowestCommonAncestorOfABinaryTreeIII.lowestCommonAncestor(root.findNodeByValue(1), root.findNodeByValue(2));
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.getVal());
    }

}
