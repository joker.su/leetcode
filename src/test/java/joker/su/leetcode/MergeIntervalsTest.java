package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;

public class MergeIntervalsTest {

    @Test
    public void example1Test() {
        MergeIntervals mergeIntervals = new MergeIntervals();
        int[][] result = mergeIntervals.merge(new int[][]{{1, 3}, {2, 6}, {8, 10}, {15, 18}});
        Arrays.sort(result, Comparator.comparingInt(x -> x[0]));
        Assert.assertTrue(Arrays.deepEquals(new int[][]{{1, 6}, {8, 10}, {15, 18}}, result));
    }

    @Test
    public void example2Test() {
        MergeIntervals mergeIntervals = new MergeIntervals();
        int[][] result = mergeIntervals.merge(new int[][]{{1, 4}, {4, 5}});
        Arrays.sort(result, Comparator.comparingInt(x -> x[0]));
        Assert.assertTrue(Arrays.deepEquals(new int[][]{{1, 5}}, result));
    }

    @Test
    public void example3Test() {
        MergeIntervals mergeIntervals = new MergeIntervals();
        int[][] result = mergeIntervals.merge(new int[][]{{1, 4}, {2, 3}});
        Arrays.sort(result, Comparator.comparingInt(x -> x[0]));
        Assert.assertTrue(Arrays.deepEquals(new int[][]{{1, 4}}, result));
    }
}
