package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

import joker.su.leetcode.PermutationInString;

public class PermutationInStringTest {

    @Test
    public void example1Test() {
        PermutationInString permutationInString = new PermutationInString();
        Assert.assertTrue(permutationInString.checkInclusion("ab", "eidbaooo"));
    }

    @Test
    public void example2Test() {
        PermutationInString permutationInString = new PermutationInString();
        Assert.assertFalse(permutationInString.checkInclusion("ab", "eidboaoo"));
    }

    @Test
    public void example3Test() {
        PermutationInString permutationInString = new PermutationInString();
        Assert.assertTrue(permutationInString.checkInclusion("ab", "ba"));
    }

    @Test
    public void example4Test() {
        PermutationInString permutationInString = new PermutationInString();
        Assert.assertTrue(permutationInString.checkInclusion("abc", "bbbca"));
    }

    @Test
    public void example5Test() {
        PermutationInString permutationInString = new PermutationInString();
        Assert.assertFalse(permutationInString.checkInclusion("hello", "ooolleoooleh"));
    }

    @Test
    public void example6Test() {
        PermutationInString permutationInString = new PermutationInString();
        Assert.assertTrue(permutationInString.checkInclusion("abcdxabcde", "abcdeabcdx"));
    }
}
