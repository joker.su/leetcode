package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class LRUCacheTest {

    @Test
    public void example1Test() {
        LRUCache lRUCache = new LRUCache(2);
        lRUCache.put(1, 1);
        lRUCache.put(2, 2);
        Assert.assertEquals(lRUCache.get(1), 1);
        lRUCache.put(3, 3);
        Assert.assertEquals(lRUCache.get(2), - 1);
        lRUCache.put(4, 4);
        Assert.assertEquals(lRUCache.get(1), -1);
        Assert.assertEquals(lRUCache.get(3), 3);
        Assert.assertEquals(lRUCache.get(4), 4);
    }
}
