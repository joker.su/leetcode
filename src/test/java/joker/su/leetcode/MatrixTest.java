package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class MatrixTest {

    @Test
    public void example1Test() {
        Matrix matrix = new Matrix();
        int[][] result = matrix.updateMatrix(new int[][]{});
        Assert.assertArrayEquals(new int[][]{}, result);
    }


    @Test
    public void example2Test() {
        Matrix matrix = new Matrix();
        int[][] result = matrix.updateMatrix(new int[][]{{0, 0, 0}, {0, 1, 0}, {0, 0, 0}});
        Assert.assertArrayEquals(new int[][]{{0, 0, 0}, {0, 1, 0}, {0, 0, 0}}, result);
    }

    @Test
    public void example3Test() {
        Matrix matrix = new Matrix();
        int[][] result = matrix.updateMatrix(new int[][]{{0, 0, 0}, {0, 1, 0}, {1, 1, 1}});
        Assert.assertArrayEquals(new int[][]{{0, 0, 0}, {0, 1, 0}, {1, 2, 1}}, result);
    }

    @Test
    public void example4Test() {
        Matrix matrix = new Matrix();
        int[][] result = matrix.updateMatrix(
            new int[][]{{1, 0, 1, 1, 0, 0, 1, 0, 0, 1}, {0, 1, 1, 0, 1, 0, 1, 0, 1, 1}, {0, 0, 1, 0, 1, 0, 0, 1, 0, 0},
                        {1, 0, 1, 0, 1, 1, 1, 1, 1, 1}, {0, 1, 0, 1, 1, 0, 0, 0, 0, 1}, {0, 0, 1, 0, 1, 1, 1, 0, 1, 0},
                        {0, 1, 0, 1, 0, 1, 0, 0, 1, 1}, {1, 0, 0, 0, 1, 1, 1, 1, 0, 1}, {1, 1, 1, 1, 1, 1, 1, 0, 1, 0},
                        {1, 1, 1, 1, 0, 1, 0, 0, 1, 1}});
        Assert.assertArrayEquals(new int[][]{{1, 0, 1, 1, 0, 0, 1, 0, 0, 1}, {0, 1, 1, 0, 1, 0, 1, 0, 1, 1}, {0, 0, 1, 0, 1, 0, 0, 1, 0, 0},
                                             {1, 0, 1, 0, 1, 1, 1, 1, 1, 1}, {0, 1, 0, 1, 1, 0, 0, 0, 0, 1}, {0, 0, 1, 0, 1, 1, 1, 0, 1, 0},
                                             {0, 1, 0, 1, 0, 1, 0, 0, 1, 1}, {1, 0, 0, 0, 1, 2, 1, 1, 0, 1}, {2, 1, 1, 1, 1, 2, 1, 0, 1, 0},
                                             {3, 2, 2, 1, 0, 1, 0, 0, 1, 1}}, result);
    }

    @Test
    public void example5Test() {
        Matrix matrix = new Matrix();
        int[][] result = matrix.updateMatrix(new int[][]{{1, 0, 1, 0}, {0, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}});
        Assert.assertArrayEquals(new int[][]{{1, 0, 1, 0}, {0, 1, 2, 1}, {1, 2, 3, 2}, {2, 3, 4, 3}}, result);
    }

    @Test
    public void example6Test() {
        Matrix matrix = new Matrix();
        int[][] result = matrix.updateMatrix(
            new int[][]{{0, 0, 1, 0, 1, 1, 1, 0, 1, 1}, {1, 1, 1, 1, 0, 1, 1, 1, 1, 1}, {1, 1, 1, 1, 1, 0, 0, 0, 1, 1},
                        {1, 0, 1, 0, 1, 1, 1, 0, 1, 1}, {0, 0, 1, 1, 1, 0, 1, 1, 1, 1}, {1, 0, 1, 1, 1, 1, 1, 1, 1, 1},
                        {1, 1, 1, 1, 0, 1, 0, 1, 0, 1}, {0, 1, 0, 0, 0, 1, 0, 0, 1, 1}, {1, 1, 1, 0, 1, 1, 0, 1, 0, 1},
                        {1, 0, 1, 1, 1, 0, 1, 1, 1, 0}});
        Assert.assertArrayEquals(new int[][]{{0, 0, 1, 0, 1, 2, 1, 0, 1, 2}, {1, 1, 2, 1, 0, 1, 1, 1, 2, 3}, {2, 1, 2, 1, 1, 0, 0, 0, 1, 2},
                                             {1, 0, 1, 0, 1, 1, 1, 0, 1, 2}, {0, 0, 1, 1, 1, 0, 1, 1, 2, 3}, {1, 0, 1, 2, 1, 1, 1, 2, 1, 2},
                                             {1, 1, 1, 1, 0, 1, 0, 1, 0, 1}, {0, 1, 0, 0, 0, 1, 0, 0, 1, 2}, {1, 1, 1, 0, 1, 1, 0, 1, 0, 1},
                                             {1, 0, 1, 1, 1, 0, 1, 2, 1, 0}}, result);
    }

}
