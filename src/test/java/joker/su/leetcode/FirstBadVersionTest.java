package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class FirstBadVersionTest {


    @Test
    public void middleTest() {
        FirstBadVersion firstBadVersion = new FirstBadVersion();
        firstBadVersion.setCurrentVersion(5);
        firstBadVersion.setBadVersion(3);
        Assert.assertEquals(3, firstBadVersion.firstBadVersion(5));
    }

    @Test
    public void startTest() {
        FirstBadVersion firstBadVersion = new FirstBadVersion();
        firstBadVersion.setCurrentVersion(5);
        firstBadVersion.setBadVersion(1);
        Assert.assertEquals(1, firstBadVersion.firstBadVersion(5));
    }

    @Test
    public void endTest() {
        FirstBadVersion firstBadVersion = new FirstBadVersion();
        firstBadVersion.setCurrentVersion(5);
        firstBadVersion.setBadVersion(5);
        Assert.assertEquals(5, firstBadVersion.firstBadVersion(5));
    }

    @Test
    public void singleTest() {
        FirstBadVersion firstBadVersion = new FirstBadVersion();
        firstBadVersion.setCurrentVersion(1);
        firstBadVersion.setBadVersion(1);
        Assert.assertEquals(1, firstBadVersion.firstBadVersion(1));
    }

    @Test
    public void evenTest() {
        FirstBadVersion firstBadVersion = new FirstBadVersion();
        firstBadVersion.setCurrentVersion(6);
        firstBadVersion.setBadVersion(6);
        Assert.assertEquals(6, firstBadVersion.firstBadVersion(6));
    }
}
