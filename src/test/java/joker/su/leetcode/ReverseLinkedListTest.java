package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

import joker.su.leetcode.api.ListNode;

public class ReverseLinkedListTest {

    @Test
    public void example1Test() {
        ReverseLinkedList reverseLinkedList = new ReverseLinkedList();
        ListNode list = ListNode.toNodeList(new int[]{1, 2, 3, 4, 5});
        ListNode result = reverseLinkedList.reverseList(list);
        Assert.assertArrayEquals(new int[]{5, 4, 3, 2, 1}, ListNode.toArrayValue(result));
    }


    @Test
    public void example2Test() {
        ReverseLinkedList reverseLinkedList = new ReverseLinkedList();
        ListNode list = ListNode.toNodeList(new int[]{1, 2});
        ListNode result = reverseLinkedList.reverseList(list);
        Assert.assertArrayEquals(new int[]{2, 1}, ListNode.toArrayValue(result));
    }

    @Test
    public void example3Test() {
        ReverseLinkedList reverseLinkedList = new ReverseLinkedList();
        ListNode list = ListNode.toNodeList(new int[]{});
        ListNode result = reverseLinkedList.reverseList(list);
        Assert.assertArrayEquals(new int[]{}, ListNode.toArrayValue(result));
    }
}
