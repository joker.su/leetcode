package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class MinimumWindowSubstringTest {

    @Test
    public void example1Test() {
        MinimumWindowSubstring tester = new MinimumWindowSubstring();
        Assert.assertEquals("BANC", tester.minWindow("ADOBECODEBANC", "ABC"));
    }

    @Test
    public void example2Test() {
        MinimumWindowSubstring tester = new MinimumWindowSubstring();
        Assert.assertEquals("a", tester.minWindow("a", "a"));
    }

    @Test
    public void example3Test() {
        MinimumWindowSubstring tester = new MinimumWindowSubstring();
        Assert.assertEquals("", tester.minWindow("a", "aa"));
    }
}
