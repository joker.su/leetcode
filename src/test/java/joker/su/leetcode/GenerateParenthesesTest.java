package joker.su.leetcode;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class GenerateParenthesesTest {

    @Test
    public void example1Test() {
        GenerateParentheses generateParentheses = new GenerateParentheses();
        List<String> result = generateParentheses.generateParenthesis(3);
        List<String> standard = Arrays.asList("((()))","(()())","(())()","()(())","()()()");
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example2Test() {
        GenerateParentheses generateParentheses = new GenerateParentheses();
        List<String> result = generateParentheses.generateParenthesis(1);
        List<String> standard = Arrays.asList("()");
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }
}
