package joker.su.leetcode;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import joker.su.leetcode.api.TreeNode;

public class BinaryTreeVerticalOrderTraversalTest {

    @Test
    public void example1Test() {
        BinaryTreeVerticalOrderTraversal verticalOrderTraversal = new BinaryTreeVerticalOrderTraversal();
        List<List<Integer>> result = verticalOrderTraversal.verticalOrder(TreeNode.toTree(new Integer[]{3,9,20,null,null,15,7}));
        List<List<Integer>> standard = List.of(
            List.of(9),
            List.of(3,15),
            List.of(20),
            List.of(7));
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example2Test() {
        BinaryTreeVerticalOrderTraversal verticalOrderTraversal = new BinaryTreeVerticalOrderTraversal();
        List<List<Integer>> result = verticalOrderTraversal.verticalOrder(TreeNode.toTree(new Integer[]{3,9,8,4,0,1,7}));
        List<List<Integer>> standard = List.of(
            List.of(4),
            List.of(9),
            List.of(3,0,1),
            List.of(8),
            List.of(7));
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example3Test() {
        BinaryTreeVerticalOrderTraversal verticalOrderTraversal = new BinaryTreeVerticalOrderTraversal();
        List<List<Integer>> result = verticalOrderTraversal.verticalOrder(TreeNode.toTree(new Integer[]{3,9,8,4,0,1,7,null,null,null,2,5}));
        List<List<Integer>> standard = List.of(
            List.of(4),
            List.of(9,5),
            List.of(3,0,1),
            List.of(8,2),
            List.of(7));
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }
}
