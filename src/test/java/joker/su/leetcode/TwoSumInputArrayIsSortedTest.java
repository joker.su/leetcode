package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class TwoSumInputArrayIsSortedTest {

    @Test
    public void exampleTest() {
        TwoSumInputArrayIsSorted twoSumInput = new TwoSumInputArrayIsSorted();
        int[] result = twoSumInput.twoSum(new int[]{2, 7, 11, 15}, 9);
        Assert.assertArrayEquals(new int[]{1, 2}, result);
    }

    @Test
    public void example2Test() {
        TwoSumInputArrayIsSorted twoSumInput = new TwoSumInputArrayIsSorted();
        int[] result = twoSumInput.twoSum(new int[]{2, 3, 4}, 6);
        Assert.assertArrayEquals(new int[]{1, 3}, result);
    }

    @Test
    public void example3Test() {
        TwoSumInputArrayIsSorted twoSumInput = new TwoSumInputArrayIsSorted();
        int[] result = twoSumInput.twoSum(new int[]{-1, 0}, -1);
        Assert.assertArrayEquals(new int[]{1, 2}, result);
    }

    @Test
    public void example4Test() {
        TwoSumInputArrayIsSorted twoSumInput = new TwoSumInputArrayIsSorted();
        int[] result = twoSumInput.twoSum(new int[]{0, 0, 3, 4}, 0);
        Assert.assertArrayEquals(new int[]{1, 2}, result);
    }

}
