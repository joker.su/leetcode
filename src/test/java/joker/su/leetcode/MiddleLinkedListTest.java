package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

import joker.su.leetcode.api.ListNode;

public class MiddleLinkedListTest {

    @Test
    public void exampleTest() {
        MiddleLinkedList middleLinkedList = new MiddleLinkedList();
        ListNode nodes = ListNode.toNodeList(5);
        ListNode middle = middleLinkedList.middleNode(nodes);
        Assert.assertEquals(3, middle.val);
    }

    @Test
    public void exampleSecondTest() {
        MiddleLinkedList middleLinkedList = new MiddleLinkedList();
        ListNode nodes = ListNode.toNodeList(6);
        ListNode middle = middleLinkedList.middleNode(nodes);
        Assert.assertEquals(4, middle.val);
    }
}
