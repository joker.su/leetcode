package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

import joker.su.leetcode.api.TreeNode;

public class MergeTwoBinaryTreesTest {

    @Test
    public void example1Test() {
        MergeTwoBinaryTrees mergeTwoBinaryTrees = new MergeTwoBinaryTrees();
        TreeNode root1 = TreeNode.toTree(new Integer[]{1, 3, 2, 5});
        TreeNode root2 = TreeNode.toTree(new Integer[]{2, 1, 3, null, 4, null, 7});
        TreeNode result = mergeTwoBinaryTrees.mergeTrees(root1, root2);
        Assert.assertEquals(TreeNode.toTree(new Integer[]{3, 4, 5, 5, 4, null, 7}), result);
    }

    @Test
    public void example2Test() {
        MergeTwoBinaryTrees mergeTwoBinaryTrees = new MergeTwoBinaryTrees();
        TreeNode root1 = TreeNode.toTree(new Integer[]{1});
        TreeNode root2 = TreeNode.toTree(new Integer[]{1, 2});
        TreeNode result = mergeTwoBinaryTrees.mergeTrees(root1, root2);
        Assert.assertEquals(TreeNode.toTree(new Integer[]{2, 2}), result);
    }

    @Test
    public void example3Test() {
        MergeTwoBinaryTrees mergeTwoBinaryTrees = new MergeTwoBinaryTrees();
        TreeNode root1 = TreeNode.toTree(new Integer[]{});
        TreeNode root2 = TreeNode.toTree(new Integer[]{});
        TreeNode result = mergeTwoBinaryTrees.mergeTrees(root1, root2);
        Assert.assertEquals(TreeNode.toTree(new Integer[]{}), result);
    }
}
