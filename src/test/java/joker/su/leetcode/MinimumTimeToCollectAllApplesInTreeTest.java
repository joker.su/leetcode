package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class MinimumTimeToCollectAllApplesInTreeTest {

    @Test
    public void example1Test() {
        MinimumTimeToCollectAllApplesInTree tester = new MinimumTimeToCollectAllApplesInTree();
        Assert.assertEquals(8, tester.minTime(7, new int[][]{{0, 1}, {0, 2}, {1, 4}, {1, 5}, {2, 3}, {2, 6}},
            Arrays.asList(false, false, true, false, true, true, false)));
    }

    @Test
    public void example2Test() {
        MinimumTimeToCollectAllApplesInTree tester = new MinimumTimeToCollectAllApplesInTree();
        Assert.assertEquals(6, tester.minTime(7, new int[][]{{0, 1}, {0, 2}, {1, 4}, {1, 5}, {2, 3}, {2, 6}},
            Arrays.asList(false, false, true, false, false, true, false)));
    }

    @Test
    public void example3Test() {
        MinimumTimeToCollectAllApplesInTree tester = new MinimumTimeToCollectAllApplesInTree();
        Assert.assertEquals(0, tester.minTime(7, new int[][]{{0, 1}, {0, 2}, {1, 4}, {1, 5}, {2, 3}, {2, 6}},
            Arrays.asList(false, false, false, false, false, false, false)));
    }


}
