package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class RomanToIntegerTest {

    @Test
    public void example1Test() {
        RomanToInteger convertor = new RomanToInteger();
        Assert.assertEquals(3, convertor.romanToInt("III"));
    }

    @Test
    public void example2Test() {
        RomanToInteger convertor = new RomanToInteger();
        Assert.assertEquals(58, convertor.romanToInt("LVIII"));
    }

    @Test
    public void example3Test() {
        RomanToInteger convertor = new RomanToInteger();
        Assert.assertEquals(8, convertor.romanToInt("IIX"));
    }

    @Test
    public void example4Test() {
        RomanToInteger convertor = new RomanToInteger();
        Assert.assertEquals(1994, convertor.romanToInt("MCMXCIV"));
    }
}
