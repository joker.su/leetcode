package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class MaximumSubarrayTest {

    @Test
    public void example1Test() {
        MaximumSubarray maximumSubarray = new MaximumSubarray();
        int result = maximumSubarray.maxSubArray(new int[]{-2, 1, -3, 4, -1, 2, 1, -5, 4});
        Assert.assertEquals(6, result);
    }

    @Test
    public void example2Test() {
        MaximumSubarray maximumSubarray = new MaximumSubarray();
        int result = maximumSubarray.maxSubArray(new int[]{1});
        Assert.assertEquals(1, result);
    }

    @Test
    public void example3Test() {
        MaximumSubarray maximumSubarray = new MaximumSubarray();
        int result = maximumSubarray.maxSubArray(new int[]{5, 4, -1, 7, 8});
        Assert.assertEquals(23, result);
    }
}
