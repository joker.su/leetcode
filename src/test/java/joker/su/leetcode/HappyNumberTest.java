package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class HappyNumberTest {

    @Test
    public void example1Test() {
        HappyNumber happyNumber = new HappyNumber();
        Assert.assertTrue(happyNumber.isHappy(19));
    }

    @Test
    public void example2Test() {
        HappyNumber happyNumber = new HappyNumber();
        Assert.assertFalse(happyNumber.isHappy(2));
    }
}
