package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class TrappingRainWaterTest {

    @Test
    public void example1Test() {
        TrappingRainWater trappingRainWater = new TrappingRainWater();
        int result = trappingRainWater.trap(new int[]{0,1,0,2,1,0,1,3,2,1,2,1});
        Assert.assertEquals(6, result);
    }

    @Test
    public void example2Test() {
        TrappingRainWater trappingRainWater = new TrappingRainWater();
        int result = trappingRainWater.trap(new int[]{4,2,0,3,2,5});
        Assert.assertEquals(9, result);
    }
}
