package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class TriangleTest {

    @Test
    public void example1Test() {
        Triangle triangle = new Triangle();
        int result = triangle.minimumTotal(List.of(
            List.of(2),
            List.of(3, 4),
            List.of(6, 5, 7),
            List.of(4, 1, 8, 3)));
        Assert.assertEquals(11, result);
    }

    @Test
    public void example2Test() {
        Triangle triangle = new Triangle();
        int result = triangle.minimumTotal(List.of(List.of(-10)));
        Assert.assertEquals(-10, result);
    }

    @Test
    public void example3Test() {
        Triangle triangle = new Triangle();
        int result = triangle.minimumTotal(List.of(List.of()));
        Assert.assertEquals(0, result);
    }
}
