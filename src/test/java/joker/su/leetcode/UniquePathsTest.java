package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class UniquePathsTest {

    @Test
    public void example1Test() {
        UniquePaths uniquePaths = new UniquePaths();
        Assert.assertEquals(28, uniquePaths.uniquePaths(3, 7));
    }

    @Test
    public void example2Test() {
        UniquePaths uniquePaths = new UniquePaths();
        Assert.assertEquals(3, uniquePaths.uniquePaths(3, 2));
    }
}
