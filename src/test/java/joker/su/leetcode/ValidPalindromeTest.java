package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class ValidPalindromeTest {

    @Test
    public void example1Test() {
        ValidPalindrome tester = new ValidPalindrome();
        Assert.assertTrue(tester.isPalindrome("A man, a plan, a canal: Panama"));
    }

    @Test
    public void example2Test() {
        ValidPalindrome tester = new ValidPalindrome();
        Assert.assertFalse(tester.isPalindrome("race a car"));
    }

    @Test
    public void example3Test() {
        ValidPalindrome tester = new ValidPalindrome();
        Assert.assertTrue(tester.isPalindrome(" "));
    }
}
