package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class AddTwoIntegersTest {

    @Test
    public void example1Test() {
        AddTwoIntegers addTwoIntegers = new AddTwoIntegers();
        Assert.assertEquals(17, addTwoIntegers.sum(12, 5));
    }

    @Test
    public void example2Test() {
        AddTwoIntegers addTwoIntegers = new AddTwoIntegers();
        Assert.assertEquals(-6, addTwoIntegers.sum(-10, 4));
    }
}
