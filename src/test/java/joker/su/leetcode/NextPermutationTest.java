package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class NextPermutationTest {

    @Test
    public void example1Test() {
        NextPermutation nextPermutation = new NextPermutation();
        int[] result = new int[]{1, 2, 3};
        nextPermutation.nextPermutation(result);
        Assert.assertArrayEquals(new int[]{1, 3, 2}, result);
    }

    @Test
    public void example2Test() {
        NextPermutation nextPermutation = new NextPermutation();
        int[] result = new int[]{3, 2, 1};
        nextPermutation.nextPermutation(result);
        Assert.assertArrayEquals(new int[]{1, 2, 3}, result);
    }

    @Test
    public void example3Test() {
        NextPermutation nextPermutation = new NextPermutation();
        int[] result = new int[]{1, 1, 5};
        nextPermutation.nextPermutation(result);
        Assert.assertArrayEquals(new int[]{1, 5, 1}, result);
    }

    @Test
    public void example4Test() {
        NextPermutation nextPermutation = new NextPermutation();
        int[] result = new int[]{2,3,1};
        nextPermutation.nextPermutation(result);
        Assert.assertArrayEquals(new int[]{3,1,2}, result);
    }

    @Test
    public void example5Test() {
        NextPermutation nextPermutation = new NextPermutation();
        int[] result = new int[]{1, 5, 1};
        nextPermutation.nextPermutation(result);
        Assert.assertArrayEquals(new int[]{5, 1, 1}, result);
    }
}
