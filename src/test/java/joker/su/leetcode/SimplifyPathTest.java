package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class SimplifyPathTest {

    @Test
    public void example1Test() {
        SimplifyPath simplifyPath = new SimplifyPath();
        String result = simplifyPath.simplifyPath("/home/");
        Assert.assertEquals("/home", result);
    }

    @Test
    public void example2Test() {
        SimplifyPath simplifyPath = new SimplifyPath();
        String result = simplifyPath.simplifyPath("/../");
        Assert.assertEquals("/", result);
    }

    @Test
    public void example3Test() {
        SimplifyPath simplifyPath = new SimplifyPath();
        String result = simplifyPath.simplifyPath("/home//foo/");
        Assert.assertEquals("/home/foo", result);
    }

    @Test
    public void example4Test() {
        SimplifyPath simplifyPath = new SimplifyPath();
        String result = simplifyPath.simplifyPath("/a/b/..//c/./d");
        Assert.assertEquals("/a/c/d", result);
    }
}
