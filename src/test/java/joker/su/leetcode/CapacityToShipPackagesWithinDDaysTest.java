package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class CapacityToShipPackagesWithinDDaysTest {

    @Test
    public void example1Test() {
        CapacityToShipPackagesWithinDDays tester = new CapacityToShipPackagesWithinDDays();
        int result = tester.shipWithinDays(new int[]{1,2,3,4,5,6,7,8,9,10}, 5);
        Assert.assertEquals(15, result);
    }

    @Test
    public void example2Test() {
        CapacityToShipPackagesWithinDDays tester = new CapacityToShipPackagesWithinDDays();
        int result = tester.shipWithinDays(new int[]{3,2,2,4,1,4}, 3);
        Assert.assertEquals(6, result);
    }

    @Test
    public void example3Test() {
        CapacityToShipPackagesWithinDDays tester = new CapacityToShipPackagesWithinDDays();
        int result = tester.shipWithinDays(new int[]{1,2,3,1,1}, 4);
        Assert.assertEquals(3, result);
    }
}
