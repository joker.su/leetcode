package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class SqrtTest {

    @Test
    public void example1Test() {
        Sqrt sqrt = new Sqrt();
        Assert.assertEquals(2, sqrt.mySqrt(4));
    }

    @Test
    public void example2Test() {
        Sqrt sqrt = new Sqrt();
        Assert.assertEquals(2, sqrt.mySqrt(8));
    }
}
