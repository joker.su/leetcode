package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class FindMinimumRotatedSortedArrayTest {

    @Test
    public void example1Test() {
        FindMinimumRotatedSortedArray findMinimumRotatedSortedArray = new FindMinimumRotatedSortedArray();
        Assert.assertEquals(1, findMinimumRotatedSortedArray.findMin(new int[]{3, 4, 5, 1, 2}));
    }

    @Test
    public void example2Test() {
        FindMinimumRotatedSortedArray findMinimumRotatedSortedArray = new FindMinimumRotatedSortedArray();
        Assert.assertEquals(0, findMinimumRotatedSortedArray.findMin(new int[]{4, 5, 6, 7, 0, 1, 2}));
    }

    @Test
    public void example3Test() {
        FindMinimumRotatedSortedArray findMinimumRotatedSortedArray = new FindMinimumRotatedSortedArray();
        Assert.assertEquals(11, findMinimumRotatedSortedArray.findMin(new int[]{11, 13, 15, 17}));
    }

    @Test
    public void example4Test() {
        FindMinimumRotatedSortedArray findMinimumRotatedSortedArray = new FindMinimumRotatedSortedArray();
        Assert.assertEquals(0, findMinimumRotatedSortedArray.findMin(new int[]{0, 1, 2, 4, 5, 6, 7}));
    }
}
