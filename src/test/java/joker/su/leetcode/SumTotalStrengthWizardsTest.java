package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class SumTotalStrengthWizardsTest {

    @Test
    public void example1Test() {
        SumTotalStrengthWizards sumTotalStrengthWizards = new SumTotalStrengthWizards();
        Assert.assertEquals(44, sumTotalStrengthWizards.totalStrength(new int[]{1,3,1,2}));
    }

    @Test
    public void example2Test() {
        SumTotalStrengthWizards sumTotalStrengthWizards = new SumTotalStrengthWizards();
        Assert.assertEquals(213, sumTotalStrengthWizards.totalStrength(new int[]{5,4,6}));
    }
}
