package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class InsertDeleteGetRandomTest {

    @Test
    public void example1Test() {
        InsertDeleteGetRandom randomizedSet = new InsertDeleteGetRandom();

        Assert.assertTrue(randomizedSet.insert(1));
        Assert.assertFalse(randomizedSet.remove(2));
        Assert.assertTrue(randomizedSet.insert(2));

        int count = 0;
        int numberFirstCounter = 0;
        int numberSecondCounter = 0;
        while (count < 100000) {
            int value = randomizedSet.getRandom();
            if (value == 1) {
                numberFirstCounter++;
            } else if (value == 2) {
                numberSecondCounter++;
            } else {
                throw new IllegalArgumentException("Out of index");
            }
            count++;
        }
        Assert.assertEquals(0.5, numberFirstCounter / (double) count, 0.005d);
        Assert.assertEquals(0.5, numberSecondCounter / (double) count, 0.005d);

        Assert.assertTrue(randomizedSet.remove(1));
        Assert.assertFalse(randomizedSet.insert(2));

        count = 0;
        numberFirstCounter = 0;
        numberSecondCounter = 0;
        while (count < 100000) {
            int value = randomizedSet.getRandom();
            if (value == 1) {
                numberFirstCounter++;
            } else if (value == 2) {
                numberSecondCounter++;
            } else {
                throw new IllegalArgumentException("Out of index");
            }
            count++;
        }
        Assert.assertEquals(0.0, numberFirstCounter / (double) count, 0.005d);
        Assert.assertEquals(1.0, numberSecondCounter / (double) count, 0.005d);
    }
}
