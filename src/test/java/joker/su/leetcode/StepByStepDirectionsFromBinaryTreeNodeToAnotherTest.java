package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

import joker.su.leetcode.api.TreeNode;

public class StepByStepDirectionsFromBinaryTreeNodeToAnotherTest {

    @Test
    public void example1Test() {
        StepByStepDirectionsFromBinaryTreeNodeToAnother directions = new StepByStepDirectionsFromBinaryTreeNodeToAnother();
        TreeNode root = TreeNode.toTree(new Integer[]{5,1,2,3,null,6,4});
        Assert.assertEquals("UURL", directions.getDirections(root, 3, 6));
    }

    @Test
    public void example2Test() {
        StepByStepDirectionsFromBinaryTreeNodeToAnother directions = new StepByStepDirectionsFromBinaryTreeNodeToAnother();
        TreeNode root = TreeNode.toTree(new Integer[]{2, 1});
        Assert.assertEquals("L", directions.getDirections(root, 2, 1));
    }

    @Test
    public void example3Test() {
        StepByStepDirectionsFromBinaryTreeNodeToAnother directions = new StepByStepDirectionsFromBinaryTreeNodeToAnother();
        TreeNode root = TreeNode.toTree(new Integer[]{5,1,2,3,7,6,4});
        Assert.assertEquals("UR", directions.getDirections(root, 3, 7));
    }

    @Test
    public void example4Test() {
        StepByStepDirectionsFromBinaryTreeNodeToAnother directions = new StepByStepDirectionsFromBinaryTreeNodeToAnother();
        TreeNode root = TreeNode.toTree(new Integer[]{5,8,3,1,null,4,7,6,null,null,null,null,null,null,2});
        Assert.assertEquals("U", directions.getDirections(root, 4, 3));
    }
}
