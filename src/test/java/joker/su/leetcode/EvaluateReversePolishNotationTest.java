package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class EvaluateReversePolishNotationTest {

    @Test
    public void example1Test() {
        EvaluateReversePolishNotation tester = new EvaluateReversePolishNotation();
        Assert.assertEquals(9, tester.evalRPN(new String[]{"2", "1", "+", "3", "*"}));
    }

    @Test
    public void example2Test() {
        EvaluateReversePolishNotation tester = new EvaluateReversePolishNotation();
        Assert.assertEquals(6, tester.evalRPN(new String[]{"4", "13", "5", "/", "+"}));
    }

    @Test
    public void example3Test() {
        EvaluateReversePolishNotation tester = new EvaluateReversePolishNotation();
        Assert.assertEquals(22, tester.evalRPN(new String[]{"10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"}));
    }
}
