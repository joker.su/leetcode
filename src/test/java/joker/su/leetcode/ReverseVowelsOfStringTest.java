package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class ReverseVowelsOfStringTest {

    @Test
    public void example1Test() {
        ReverseVowelsOfString tester = new ReverseVowelsOfString();
        Assert.assertEquals("holle", tester.reverseVowels("hello"));
    }

    @Test
    public void example2Test() {
        ReverseVowelsOfString tester = new ReverseVowelsOfString();
        Assert.assertEquals("leotcede", tester.reverseVowels("leetcode"));
    }
}
