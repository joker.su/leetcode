package joker.su.leetcode;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class Sum3Test {

    @Test
    public void example1Test() {
        Sum3 sum = new Sum3();
        List<List<Integer>> result = sum.threeSum(new int[]{-1,0,1,2,-1,-4});
        List<List<Integer>> standard = List.of(
            List.of(-1,-1,2),
            List.of(-1,0,1));
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example2Test() {
        Sum3 sum = new Sum3();
        List<List<Integer>> result = sum.threeSum(new int[]{0,1,1});
        List<List<Integer>> standard = List.of();
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example3Test() {
        Sum3 sum = new Sum3();
        List<List<Integer>> result = sum.threeSum(new int[]{0,0,0});
        List<List<Integer>> standard = List.of(
            List.of(0,0,0));
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    public void example4Test() {
        Sum3 sum = new Sum3();
        List<List<Integer>> result = sum.threeSum(new int[]{1,0,-1});
        List<List<Integer>> standard = List.of(
            List.of(-1,0,1));
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

}
