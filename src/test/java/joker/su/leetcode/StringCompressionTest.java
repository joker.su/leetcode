package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class StringCompressionTest {

    @Test
    public void example1Test() {
        StringCompression tester = new StringCompression();
        Assert.assertEquals(6, tester.compress(new char[]{'a','a','b','b','c','c','c'}));
    }

    @Test
    public void example2Test() {
        StringCompression tester = new StringCompression();
        Assert.assertEquals(1, tester.compress(new char[]{'a'}));
    }


    @Test
    public void example3Test() {
        StringCompression tester = new StringCompression();
        Assert.assertEquals(4, tester.compress(new char[]{'a','b','b','b','b','b','b','b','b','b','b','b','b'}));
    }
}
