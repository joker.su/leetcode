package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class PowerOfTwoTest {

    @Test
    public void example1Test() {
        PowerOfTwo powerOfTwo = new PowerOfTwo();
        Assert.assertTrue(powerOfTwo.isPowerOfTwo(1));
    }

    @Test
    public void example2Test() {
        PowerOfTwo powerOfTwo = new PowerOfTwo();
        Assert.assertTrue(powerOfTwo.isPowerOfTwo(16));
    }

    @Test
    public void example3Test() {
        PowerOfTwo powerOfTwo = new PowerOfTwo();
        Assert.assertFalse(powerOfTwo.isPowerOfTwo(3));
    }

    @Test
    public void example4Test() {
        PowerOfTwo powerOfTwo = new PowerOfTwo();
        Assert.assertFalse(powerOfTwo.isPowerOfTwo(0));
    }

    @Test
    public void example5Test() {
        PowerOfTwo powerOfTwo = new PowerOfTwo();
        Assert.assertFalse(powerOfTwo.isPowerOfTwo(5));
    }

    @Test
    public void example6Test() {
        PowerOfTwo powerOfTwo = new PowerOfTwo();
        Assert.assertFalse(powerOfTwo.isPowerOfTwo(6));
    }

    @Test
    public void example7Test() {
        PowerOfTwo powerOfTwo = new PowerOfTwo();
        Assert.assertTrue(powerOfTwo.isPowerOfTwo(2));
    }
}
