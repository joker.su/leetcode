package joker.su.leetcode;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class LetterCombinationsOfPhoneNumberTest {

    @Test
    public void example1Test() {
        LetterCombinationsOfPhoneNumber letterCombinationsOfPhoneNumber = new LetterCombinationsOfPhoneNumber();
        List<String> result = letterCombinationsOfPhoneNumber.letterCombinations("23");
        List<String> standard = Arrays.asList("ad","ae","af","bd","be","bf","cd","ce","cf");
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example2Test() {
        LetterCombinationsOfPhoneNumber letterCombinationsOfPhoneNumber = new LetterCombinationsOfPhoneNumber();
        List<String> result = letterCombinationsOfPhoneNumber.letterCombinations("");
        List<String> standard = Arrays.asList();
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example3Test() {
        LetterCombinationsOfPhoneNumber letterCombinationsOfPhoneNumber = new LetterCombinationsOfPhoneNumber();
        List<String> result = letterCombinationsOfPhoneNumber.letterCombinations("2");
        List<String> standard = Arrays.asList("a","b","c");
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }
}
