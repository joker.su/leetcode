package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class IntegerToEnglishWordsTest {

    @Test
    public void example1Test() {
        IntegerToEnglishWords integerToEnglishWords = new IntegerToEnglishWords();
        Assert.assertEquals("One Hundred Twenty Three", integerToEnglishWords.numberToWords(123));
    }

    @Test
    public void example2Test() {
        IntegerToEnglishWords integerToEnglishWords = new IntegerToEnglishWords();
        Assert.assertEquals("Twelve Thousand Three Hundred Forty Five", integerToEnglishWords.numberToWords(12345));
    }

    @Test
    public void example3Test() {
        IntegerToEnglishWords integerToEnglishWords = new IntegerToEnglishWords();
        Assert.assertEquals("One Million Two Hundred Thirty Four Thousand Five Hundred Sixty Seven",
            integerToEnglishWords.numberToWords(1234567));
    }

    @Test
    public void example4Test() {
        IntegerToEnglishWords integerToEnglishWords = new IntegerToEnglishWords();
        Assert.assertEquals("Two Billion One Hundred Forty Seven Million Four Hundred Eighty Three Thousand Six Hundred Forty Seven",
            integerToEnglishWords.numberToWords(Integer.MAX_VALUE));
    }

    @Test
    public void example5Test() {
        IntegerToEnglishWords integerToEnglishWords = new IntegerToEnglishWords();
        Assert.assertEquals("One Hundred Million", integerToEnglishWords.numberToWords(100_000_000));
    }

    @Test
    public void example6Test() {
        IntegerToEnglishWords integerToEnglishWords = new IntegerToEnglishWords();
        Assert.assertEquals("Four Thousand", integerToEnglishWords.numberToWords(4_000));
    }

    @Test
    public void example7Test() {
        IntegerToEnglishWords integerToEnglishWords = new IntegerToEnglishWords();
        Assert.assertEquals("Zero", integerToEnglishWords.numberToWords(0));
    }
}
