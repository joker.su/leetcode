package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class BuildingsWithAnOceanViewTest {

    @Test
    public void example1Test() {
        BuildingsWithAnOceanView buildingsWithAnOceanView = new BuildingsWithAnOceanView();
        int[] result = buildingsWithAnOceanView.findBuildings(new int[]{4,2,3,1});
        Assert.assertArrayEquals(new int[]{0,2,3}, result);
    }

    @Test
    public void example2Test() {
        BuildingsWithAnOceanView buildingsWithAnOceanView = new BuildingsWithAnOceanView();
        int[] result = buildingsWithAnOceanView.findBuildings(new int[]{4,3,2,1});
        Assert.assertArrayEquals(new int[]{0,1,2,3}, result);
    }

    @Test
    public void example3Test() {
        BuildingsWithAnOceanView buildingsWithAnOceanView = new BuildingsWithAnOceanView();
        int[] result = buildingsWithAnOceanView.findBuildings(new int[]{1,3,2,4});
        Assert.assertArrayEquals(new int[]{3}, result);
    }
}
