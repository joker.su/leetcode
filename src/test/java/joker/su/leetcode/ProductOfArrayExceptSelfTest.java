package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class ProductOfArrayExceptSelfTest {

    @Test
    public void example1Test() {
        ProductOfArrayExceptSelf tester = new ProductOfArrayExceptSelf();
        Assert.assertArrayEquals(new int[]{24,12,8,6}, tester.productExceptSelf(new int[]{1,2,3,4}));
    }


    @Test
    public void example2Test() {
        ProductOfArrayExceptSelf tester = new ProductOfArrayExceptSelf();
        Assert.assertArrayEquals(new int[]{0,0,9,0,0}, tester.productExceptSelf(new int[]{-1,1,0,-3,3}));
    }
}
