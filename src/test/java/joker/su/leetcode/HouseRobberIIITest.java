package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

import joker.su.leetcode.api.TreeNode;

public class HouseRobberIIITest {

    @Test
    public void example1Test() {
        HouseRobberIII houseRobber = new HouseRobberIII();
        TreeNode root = TreeNode.toTree(new Integer[]{3, 2, 3, null, 3, null, 1});
        int result = houseRobber.rob(root);
        Assert.assertEquals(7, result);
    }


    @Test
    public void example2Test() {
        HouseRobberIII houseRobber = new HouseRobberIII();
        TreeNode root = TreeNode.toTree(new Integer[]{3, 4, 5, 1, 3, null, 1});
        int result = houseRobber.rob(root);
        Assert.assertEquals(9, result);
    }
}
