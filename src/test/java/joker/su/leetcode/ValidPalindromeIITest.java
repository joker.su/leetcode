package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class ValidPalindromeIITest {

    @Test
    public void example1Test() {
        ValidPalindromeII validPalindromeII = new ValidPalindromeII();
        Assert.assertTrue(validPalindromeII.validPalindrome("aba"));
    }

    @Test
    public void example2Test() {
        ValidPalindromeII validPalindromeII = new ValidPalindromeII();
        Assert.assertTrue(validPalindromeII.validPalindrome("abca"));
    }

    @Test
    public void example3Test() {
        ValidPalindromeII validPalindromeII = new ValidPalindromeII();
        Assert.assertFalse(validPalindromeII.validPalindrome("abc"));
    }

    @Test
    public void example4Test() {
        ValidPalindromeII validPalindromeII = new ValidPalindromeII();
        Assert.assertTrue(validPalindromeII.validPalindrome("a"));
    }

    @Test
    public void example5Test() {
        ValidPalindromeII validPalindromeII = new ValidPalindromeII();
        Assert.assertTrue(validPalindromeII.validPalindrome("cbbcc"));
    }

    @Test
    public void example6Test() {
        ValidPalindromeII validPalindromeII = new ValidPalindromeII();
        Assert.assertFalse(validPalindromeII.validPalindrome("axbcbaba"));
    }

    @Test
    public void example7Test() {
        ValidPalindromeII validPalindromeII = new ValidPalindromeII();
        Assert.assertFalse(validPalindromeII.validPalindrome("eeccccbebaeeabebccceea"));
    }

    @Test
    public void example8Test() {
        ValidPalindromeII validPalindromeII = new ValidPalindromeII();
        Assert.assertFalse(validPalindromeII.validPalindrome("eddboebddcaacddkbebdde"));
    }


    @Test
    public void example9Test() {
        ValidPalindromeII validPalindromeII = new ValidPalindromeII();
        Assert.assertTrue(validPalindromeII.validPalindrome("aguokepatgbnvfqmgmlcupuufxoohdfpgjdmysgvhmvffcnqxjjxqncffvmhvgsymdjgpfdhooxfuupuculmgmqfvnbgtapekouga"));
    }
}
