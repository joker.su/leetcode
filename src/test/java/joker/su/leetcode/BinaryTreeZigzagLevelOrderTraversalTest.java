package joker.su.leetcode;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import joker.su.leetcode.api.TreeNode;

public class BinaryTreeZigzagLevelOrderTraversalTest {

    @Test
    public void example1Test() {
        BinaryTreeZigzagLevelOrderTraversal tester = new BinaryTreeZigzagLevelOrderTraversal();
        List<List<Integer>> result = tester.zigzagLevelOrder(TreeNode.toTree(new Integer[]{3, 9, 20, null, null, 15, 7}));
        List<List<Integer>> standard = List.of(
            List.of(3),
            List.of(20,9),
            List.of(15, 7));
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example2Test() {
        BinaryTreeZigzagLevelOrderTraversal tester = new BinaryTreeZigzagLevelOrderTraversal();
        List<List<Integer>> result = tester.zigzagLevelOrder(TreeNode.toTree(new Integer[]{1}));
        List<List<Integer>> standard = List.of(
            List.of(1));
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example3Test() {
        BinaryTreeZigzagLevelOrderTraversal tester = new BinaryTreeZigzagLevelOrderTraversal();
        List<List<Integer>> result = tester.zigzagLevelOrder(TreeNode.toTree(new Integer[]{}, true));
        List<List<Integer>> standard = List.of();
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }


    @Test
    public void example4Test() {
        BinaryTreeZigzagLevelOrderTraversal tester = new BinaryTreeZigzagLevelOrderTraversal();
        List<List<Integer>> result = tester.zigzagLevelOrder(TreeNode.toTree(new Integer[]{1,2,3,4,null,null,5}));
        List<List<Integer>> standard = List.of(
            List.of(1),
            List.of(3,2),
            List.of(4, 5));
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }
}
