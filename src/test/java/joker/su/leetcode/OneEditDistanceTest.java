package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class OneEditDistanceTest {

    @Test
    public void example1Test() {
        OneEditDistance tester = new OneEditDistance();
        Assert.assertTrue(tester.isOneEditDistance("ab", "acb"));
    }

    @Test
    public void example2Test() {
        OneEditDistance tester = new OneEditDistance();
        Assert.assertTrue(tester.isOneEditDistance("axb", "acb"));
    }

    @Test
    public void example3Test() {
        OneEditDistance tester = new OneEditDistance();
        Assert.assertTrue(tester.isOneEditDistance("acx", "acb"));
    }

    @Test
    public void example4Test() {
        OneEditDistance tester = new OneEditDistance();
        Assert.assertFalse(tester.isOneEditDistance("", ""));
    }

    @Test
    public void example5Test() {
        OneEditDistance tester = new OneEditDistance();
        Assert.assertFalse(tester.isOneEditDistance("asdf", "as"));
    }
}
