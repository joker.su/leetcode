package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class CountOddNumbersInIntervalRangeTest {

    @Test
    public void example1Test() {
        CountOddNumbersInIntervalRange countOddNumbersInIntervalRange = new CountOddNumbersInIntervalRange();
        Assert.assertEquals(3, countOddNumbersInIntervalRange.countOdds(3, 7));
    }

    @Test
    public void example2Test() {
        CountOddNumbersInIntervalRange countOddNumbersInIntervalRange = new CountOddNumbersInIntervalRange();
        Assert.assertEquals(1, countOddNumbersInIntervalRange.countOdds(8, 10));
    }
}
