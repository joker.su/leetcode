package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class BestTimeToBuyAndSellStockTest {

    @Test
    public void example1Test() {
        BestTimeToBuyAndSellStock bestTimeToBuyAndSellStock = new BestTimeToBuyAndSellStock();
        int result = bestTimeToBuyAndSellStock.maxProfit(new int[]{7,1,5,3,6,4});
        Assert.assertEquals(5, result);
    }

    @Test
    public void example2Test() {
        BestTimeToBuyAndSellStock bestTimeToBuyAndSellStock = new BestTimeToBuyAndSellStock();
        int result = bestTimeToBuyAndSellStock.maxProfit(new int[]{7,6,4,3,1});
        Assert.assertEquals(0, result);
    }
}
