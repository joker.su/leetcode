package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class BackspaceStringCompareTest {

    @Test
    public void example1Test() {
        BackspaceStringCompare backspaceStringCompare = new BackspaceStringCompare();
        Assert.assertTrue(backspaceStringCompare.backspaceCompare("ab#c", "ad#c"));
    }

    @Test
    public void example2Test() {
        BackspaceStringCompare backspaceStringCompare = new BackspaceStringCompare();
        Assert.assertTrue(backspaceStringCompare.backspaceCompare("ab##", "c#d#"));
    }

    @Test
    public void example3Test() {
        BackspaceStringCompare backspaceStringCompare = new BackspaceStringCompare();
        Assert.assertFalse(backspaceStringCompare.backspaceCompare("a#c", "b"));
    }

    @Test
    public void example4Test() {
        BackspaceStringCompare backspaceStringCompare = new BackspaceStringCompare();
        Assert.assertFalse(backspaceStringCompare.backspaceCompare("a#", "b"));
    }

    @Test
    public void example5Test() {
        BackspaceStringCompare backspaceStringCompare = new BackspaceStringCompare();
        Assert.assertTrue(backspaceStringCompare.backspaceCompare("a#", ""));
    }

    @Test
    public void example6Test() {
        BackspaceStringCompare backspaceStringCompare = new BackspaceStringCompare();
        Assert.assertTrue(backspaceStringCompare.backspaceCompare("a#####c", "c"));
    }


    @Test
    public void example7Test() {
        BackspaceStringCompare backspaceStringCompare = new BackspaceStringCompare();
        Assert.assertTrue(backspaceStringCompare.backspaceCompare("a##c","#a#c"));
    }

    @Test
    public void example8Test() {
        BackspaceStringCompare backspaceStringCompare = new BackspaceStringCompare();
        Assert.assertTrue(backspaceStringCompare.backspaceCompare("bxj##tw", "bxo#j##tw"));
    }
}
