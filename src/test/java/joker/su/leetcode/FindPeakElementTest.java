package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class FindPeakElementTest {

    @Test
    public void example1Test() {
        FindPeakElement findPeakElement = new FindPeakElement();
        int result = findPeakElement.findPeakElement(new int[]{1, 2, 3, 1});
        Assert.assertEquals(2, result);
    }

    @Test
    public void example2Test() {
        FindPeakElement findPeakElement = new FindPeakElement();
        int result = findPeakElement.findPeakElement(new int[]{1, 2, 1, 3, 5, 6, 4});
        Assert.assertTrue(result == 1 || result == 5);
    }

    @Test
    public void example3Test() {
        FindPeakElement findPeakElement = new FindPeakElement();
        int result = findPeakElement.findPeakElement(new int[]{4});
        Assert.assertEquals(0, result);
    }
}
