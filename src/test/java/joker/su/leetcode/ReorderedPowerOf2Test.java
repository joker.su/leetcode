package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class ReorderedPowerOf2Test {

    @Test
    public void example1Test() {
        ReorderedPowerOf2 reorderedPowerOf2 = new ReorderedPowerOf2();
        Assert.assertTrue(reorderedPowerOf2.reorderedPowerOf2(1));
    }

    @Test
    public void example2Test() {
        ReorderedPowerOf2 reorderedPowerOf2 = new ReorderedPowerOf2();
        Assert.assertFalse(reorderedPowerOf2.reorderedPowerOf2(10));
    }


    @Test
    public void example3Test() {
        ReorderedPowerOf2 reorderedPowerOf2 = new ReorderedPowerOf2();
        Assert.assertTrue(reorderedPowerOf2.reorderedPowerOf2(32));
    }

    @Test
    public void example4Test() {
        ReorderedPowerOf2 reorderedPowerOf2 = new ReorderedPowerOf2();
        Assert.assertTrue(reorderedPowerOf2.reorderedPowerOf2(23));
    }
}
