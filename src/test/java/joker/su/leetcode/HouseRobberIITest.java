package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class HouseRobberIITest {

    @Test
    public void example1Test() {
        HouseRobberII houseRobber = new HouseRobberII();
        int result = houseRobber.rob(new int[]{1});
        Assert.assertEquals(1, result);
    }

    @Test
    public void example2Test() {
        HouseRobberII houseRobber = new HouseRobberII();
        int result = houseRobber.rob(new int[]{});
        Assert.assertEquals(0, result);
    }

    @Test
    public void example3Test() {
        HouseRobberII houseRobber = new HouseRobberII();
        int result = houseRobber.rob(new int[]{2, 3, 2});
        Assert.assertEquals(3, result);
    }

    @Test
    public void example4Test() {
        HouseRobberII houseRobber = new HouseRobberII();
        int result = houseRobber.rob(new int[]{1, 2, 3, 1});
        Assert.assertEquals(4, result);
    }

    @Test
    public void example5Test() {
        HouseRobberII houseRobber = new HouseRobberII();
        int result = houseRobber.rob(new int[]{1, 2, 3});
        Assert.assertEquals(3, result);
    }
}
