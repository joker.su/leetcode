package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class KthLargestElementInArrayTest {

    @Test
    public void example1Test() {
        KthLargestElementInArray kthLargestElementInArray = new KthLargestElementInArray();
        Assert.assertEquals(5, kthLargestElementInArray.findKthLargest(new int[]{3,2,1,5,6,4}, 2));
    }

    @Test
    public void example2Test() {
        KthLargestElementInArray kthLargestElementInArray = new KthLargestElementInArray();
        Assert.assertEquals(4, kthLargestElementInArray.findKthLargest(new int[]{3,2,3,1,2,4,5,5,6}, 4));
    }


}
