package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class SquaresSortedArrayTest {


    @Test
    public void positiveOnly() {
        SquaresSortedArray squaresSortedArray = new SquaresSortedArray();
        int[] result = squaresSortedArray.sortedSquares(new int[]{1, 2, 4, 5});
        Assert.assertArrayEquals(new int[]{1, 4, 16, 25}, result);
    }


    @Test
    public void negativeOnly() {
        SquaresSortedArray squaresSortedArray = new SquaresSortedArray();
        int[] result = squaresSortedArray.sortedSquares(new int[]{-5, -4, -2, -1});
        Assert.assertArrayEquals(new int[]{1, 4, 16, 25}, result);
    }

    @Test
    public void unevenTest() {
        SquaresSortedArray squaresSortedArray = new SquaresSortedArray();
        int[] result = squaresSortedArray.sortedSquares(new int[]{-4, -1, 0, 3, 10});
        Assert.assertArrayEquals(new int[]{0, 1, 9, 16, 100}, result);
    }


    @Test
    public void evenTest() {
        SquaresSortedArray squaresSortedArray = new SquaresSortedArray();
        int[] result = squaresSortedArray.sortedSquares(new int[]{-7, -3, -2, 3, 5, 11});
        Assert.assertArrayEquals(new int[]{4, 9, 9, 25, 49, 121}, result);
    }
}
