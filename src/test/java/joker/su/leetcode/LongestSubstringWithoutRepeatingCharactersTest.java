package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

import joker.su.leetcode.LongestSubstringWithoutRepeatingCharacters;

public class LongestSubstringWithoutRepeatingCharactersTest {

    @Test
    public void exampleTest() {
        LongestSubstringWithoutRepeatingCharacters longestSubstring = new LongestSubstringWithoutRepeatingCharacters();
        int result = longestSubstring.lengthOfLongestSubstring("abcabcbb");
        Assert.assertEquals(3, result);
    }

    @Test
    public void exampleSecondTest() {
        LongestSubstringWithoutRepeatingCharacters longestSubstring = new LongestSubstringWithoutRepeatingCharacters();
        int result = longestSubstring.lengthOfLongestSubstring("bbbbb");
        Assert.assertEquals(1, result);
    }

    @Test
    public void exampleThirdTest() {
        LongestSubstringWithoutRepeatingCharacters longestSubstring = new LongestSubstringWithoutRepeatingCharacters();
        int result = longestSubstring.lengthOfLongestSubstring("pwwkew");
        Assert.assertEquals(3, result);
    }

    @Test
    public void emptyTest() {
        LongestSubstringWithoutRepeatingCharacters longestSubstring = new LongestSubstringWithoutRepeatingCharacters();
        int result = longestSubstring.lengthOfLongestSubstring("");
        Assert.assertEquals(0, result);
    }

    @Test
    public void example4Test() {
        LongestSubstringWithoutRepeatingCharacters longestSubstring = new LongestSubstringWithoutRepeatingCharacters();
        int result = longestSubstring.lengthOfLongestSubstring("dvdf");
        Assert.assertEquals(3, result);
    }
}
