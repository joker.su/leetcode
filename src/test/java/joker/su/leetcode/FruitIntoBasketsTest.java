package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class FruitIntoBasketsTest {

    @Test
    public void example1Test() {
        FruitIntoBaskets tester = new FruitIntoBaskets();
        Assert.assertEquals(3, tester.totalFruit(new int[]{1, 2, 1}));
    }

    @Test
    public void example2Test() {
        FruitIntoBaskets tester = new FruitIntoBaskets();
        Assert.assertEquals(3, tester.totalFruit(new int[]{0,1,2,2}));
    }

    @Test
    public void example3Test() {
        FruitIntoBaskets tester = new FruitIntoBaskets();
        Assert.assertEquals(4, tester.totalFruit(new int[]{1,2,3,2,2}));
    }
}
