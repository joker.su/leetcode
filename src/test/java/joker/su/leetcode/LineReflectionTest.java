package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class LineReflectionTest {

    @Test
    public void example1Test() {
        LineReflection lineReflection = new LineReflection();
        Assert.assertTrue(lineReflection.isReflected(new int[][]{{1, 1}, {-1, 1}}));
    }

    @Test
    public void example2Test() {
        LineReflection lineReflection = new LineReflection();
        Assert.assertFalse(lineReflection.isReflected(new int[][]{{1, 1}, {-1, -1}}));
    }
}
