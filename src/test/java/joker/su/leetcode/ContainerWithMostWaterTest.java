package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class ContainerWithMostWaterTest {

    @Test
    public void example1Test() {
        ContainerWithMostWater containerWithMostWater = new ContainerWithMostWater();
        int result = containerWithMostWater.maxArea(new int[]{1,8,6,2,5,4,8,3,7});
        Assert.assertEquals(49, result);
    }

    @Test
    public void example2Test() {
        ContainerWithMostWater containerWithMostWater = new ContainerWithMostWater();
        int result = containerWithMostWater.maxArea(new int[]{1,1});
        Assert.assertEquals(1, result);
    }
}
