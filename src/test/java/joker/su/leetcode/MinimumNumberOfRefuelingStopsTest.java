package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class MinimumNumberOfRefuelingStopsTest {

    @Test
    public void example1Test() {
        MinimumNumberOfRefuelingStops numberOfRefuelingStops = new MinimumNumberOfRefuelingStops();
        Assert.assertEquals(0, numberOfRefuelingStops.minRefuelStops(1, 1, new int[][]{}));
    }

    @Test
    public void example2Test() {
        MinimumNumberOfRefuelingStops numberOfRefuelingStops = new MinimumNumberOfRefuelingStops();
        Assert.assertEquals(-1, numberOfRefuelingStops.minRefuelStops(100, 1, new int[][]{{10, 100}}));
    }

    @Test
    public void example3Test() {
        MinimumNumberOfRefuelingStops numberOfRefuelingStops = new MinimumNumberOfRefuelingStops();
        Assert.assertEquals(2, numberOfRefuelingStops.minRefuelStops(100, 10, new int[][]{{10, 60}, {20, 30}, {30, 30}, {60, 40}}));
    }
}
