package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class IntervalListIntersectionsTest {

    @Test
    public void example1Test() {
        IntervalListIntersections tester = new IntervalListIntersections();
        int[][] firstList = new int[][]{{0, 2}, {5, 10}, {13, 23}, {24, 25}};
        int[][] secondList = new int[][]{{1, 5}, {8, 12}, {15, 24}, {25, 26}};
        int[][] result = tester.intervalIntersection(firstList, secondList);
        Assert.assertArrayEquals(new int[][]{{1, 2}, {5, 5}, {8, 10}, {15, 23}, {24, 24}, {25, 25}}, result);
    }


    @Test
    public void example2Test() {
        IntervalListIntersections tester = new IntervalListIntersections();
        int[][] firstList = new int[][]{{1, 3}, {5, 9}};
        int[][] secondList = new int[][]{};
        int[][] result = tester.intervalIntersection(firstList, secondList);
        Assert.assertArrayEquals(new int[][]{}, result);
    }


}
