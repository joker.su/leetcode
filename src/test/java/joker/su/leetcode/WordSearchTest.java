package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class WordSearchTest {

    @Test
    public void example1Test() {
        WordSearch wordSearch = new WordSearch();
        Assert.assertTrue(wordSearch.exist(new char[][]{{'A','B','C','E'},{'S','F','C','S'},{'A','D','E','E'}}, "ABCCED"));
    }

    @Test
    public void example2Test() {
        WordSearch wordSearch = new WordSearch();
        Assert.assertTrue(wordSearch.exist(new char[][]{{'A','B','C','E'},{'S','F','C','S'},{'A','D','E','E'}}, "SEE"));
    }

    @Test
    public void example3Test() {
        WordSearch wordSearch = new WordSearch();
        Assert.assertFalse(wordSearch.exist(new char[][]{{'A','B','C','E'},{'S','F','C','S'},{'A','D','E','E'}}, "ABCB"));
    }
}
