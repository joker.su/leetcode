package joker.su.leetcode;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PermutationsTest {

    @Test
    public void example1Test() {
        Permutations permutations = new Permutations();
        List<List<Integer>> result = permutations.permute(new int[]{1, 2, 3});
        Integer[][] standard = new Integer[][]{{1, 2, 3}, {1, 3, 2}, {2, 1, 3}, {2, 3, 1}, {3, 1, 2}, {3, 2, 1}};
        Assert.assertTrue(CollectionUtils.isEqualCollection(
            Arrays.stream(standard).map(Arrays::asList).collect(Collectors.toList()),
            result));
    }

    @Test
    public void example2Test() {
        Permutations permutations = new Permutations();
        List<List<Integer>> result = permutations.permute(new int[]{0, 1});
        Integer[][] standard = new Integer[][]{{0, 1}, {1, 0}};
        Assert.assertTrue(CollectionUtils.isEqualCollection(
            Arrays.stream(standard).map(Arrays::asList).collect(Collectors.toList()),
            result));
    }

    @Test
    public void example3Test() {
        Permutations permutations = new Permutations();
        List<List<Integer>> result = permutations.permute(new int[]{1});
        Integer[][] standard = new Integer[][]{{1}};
        Assert.assertTrue(CollectionUtils.isEqualCollection(
            Arrays.stream(standard).map(Arrays::asList).collect(Collectors.toList()),
            result));
    }
}
