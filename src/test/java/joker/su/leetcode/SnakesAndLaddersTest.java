package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class SnakesAndLaddersTest {

    @Test
    public void example1Test() {
        SnakesAndLadders snakesAndLadders = new SnakesAndLadders();
        int[][] board = new int[][]{{-1, -1, -1, -1, -1, -1},
                                    {-1, -1, -1, -1, -1, -1},
                                    {-1, -1, -1, -1, -1, -1},
                                    {-1, 35, -1, -1, 13, -1},
                                    {-1, -1, -1, -1, -1, -1},
                                    {-1, 15, -1, -1, -1, -1}};
        Assert.assertEquals(4, snakesAndLadders.snakesAndLadders(board));
    }

    @Test
    public void example2Test() {
        SnakesAndLadders snakesAndLadders = new SnakesAndLadders();
        int[][] board = new int[][]{{-1, -1},
                                    {-1, 3}};
        Assert.assertEquals(1, snakesAndLadders.snakesAndLadders(board));
    }
}
