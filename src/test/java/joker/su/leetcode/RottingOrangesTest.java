package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class RottingOrangesTest {

    @Test
    public void example1Test() {
        RottingOranges rottingOranges = new RottingOranges();
        int result = rottingOranges.orangesRotting(new int[][]{{2, 1, 1}, {1, 1, 0}, {0, 1, 1}});
        Assert.assertEquals(4, result);
    }

    @Test
    public void example2Test() {
        RottingOranges rottingOranges = new RottingOranges();
        int result = rottingOranges.orangesRotting(new int[][]{{2, 1, 1}, {0, 1, 1}, {1, 0, 1}});
        Assert.assertEquals(-1, result);
    }


    @Test
    public void example3Test() {
        RottingOranges rottingOranges = new RottingOranges();
        int result = rottingOranges.orangesRotting(new int[][]{{0, 2}});
        Assert.assertEquals(0, result);
    }

}
