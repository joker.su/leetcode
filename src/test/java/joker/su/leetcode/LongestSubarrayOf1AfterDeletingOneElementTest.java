package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

public class LongestSubarrayOf1AfterDeletingOneElementTest {

    @Test
    public void example1Test() {
        LongestSubarrayOf1AfterDeletingOneElement longestSubarrayOf1AfterDeletingOneElement = new LongestSubarrayOf1AfterDeletingOneElement();
        Assert.assertEquals(3, longestSubarrayOf1AfterDeletingOneElement.longestSubarray(new int[]{1,1,0,1}));
    }

    @Test
    public void example2Test() {
        LongestSubarrayOf1AfterDeletingOneElement longestSubarrayOf1AfterDeletingOneElement = new LongestSubarrayOf1AfterDeletingOneElement();
        Assert.assertEquals(5, longestSubarrayOf1AfterDeletingOneElement.longestSubarray(new int[]{0,1,1,1,0,1,1,0,1}));
    }

    @Test
    public void example3Test() {
        LongestSubarrayOf1AfterDeletingOneElement longestSubarrayOf1AfterDeletingOneElement = new LongestSubarrayOf1AfterDeletingOneElement();
        Assert.assertEquals(2, longestSubarrayOf1AfterDeletingOneElement.longestSubarray(new int[]{1,1,1}));
    }
}
