package joker.su.leetcode;

import org.junit.Assert;
import org.junit.Test;

import joker.su.leetcode.api.Node;

public class PopulatingNextRightPointersEachNodeTest {

    @Test
    public void example1Test() {
        PopulatingNextRightPointersEachNode populatingNextRightPointersEachNode = new PopulatingNextRightPointersEachNode();
        Node root = Node.toTree(new Integer[]{1, 2, 3, 4, 5, 6, 7});
        Node result = populatingNextRightPointersEachNode.connect(root);
        Assert.assertNotNull(result);
        Assert.assertArrayEquals(new Integer[]{1, null, 2, 3, null, 4, 5, 6, 7, null}, result.toArray());
    }

    @Test
    public void example2Test() {
        PopulatingNextRightPointersEachNode populatingNextRightPointersEachNode = new PopulatingNextRightPointersEachNode();
        Node root = Node.toTree(new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15});
        Node result = populatingNextRightPointersEachNode.connect(root);
        Assert.assertNotNull(result);
        Assert.assertArrayEquals(new Integer[]{1, null, 2, 3, null, 4, 5, 6, 7, null, 8, 9, 10, 11, 12, 13, 14, 15, null},
            result.toArray());
    }

    @Test
    public void example3Test() {
        PopulatingNextRightPointersEachNode populatingNextRightPointersEachNode = new PopulatingNextRightPointersEachNode();
        Node root = Node.toTree(new Integer[]{});
        Node result = populatingNextRightPointersEachNode.connect(root);
        Assert.assertNull(result);
    }
}
