package joker.su.interview.g42;

import org.junit.Assert;
import org.junit.Test;

public class MinimalNumberOfSubstringsTest {

    @Test
    public void example1Test() {
        MinimalNumberOfSubstrings tester = new MinimalNumberOfSubstrings();
        Assert.assertEquals(1, tester.getNumberOfSubstrings("world"));
    }

    @Test
    public void example2Test() {
        MinimalNumberOfSubstrings tester = new MinimalNumberOfSubstrings();
        Assert.assertEquals(4, tester.getNumberOfSubstrings("dddd"));
    }

    @Test
    public void example3Test() {
        MinimalNumberOfSubstrings tester = new MinimalNumberOfSubstrings();
        Assert.assertEquals(2, tester.getNumberOfSubstrings("cycle"));
    }

    @Test
    public void example4Test() {
        MinimalNumberOfSubstrings tester = new MinimalNumberOfSubstrings();
        Assert.assertEquals(2, tester.getNumberOfSubstrings("abba"));
    }
}
