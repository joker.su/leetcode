package joker.su.interview.g42;

import org.junit.Assert;
import org.junit.Test;

public class NumberOfPatchesTest {

    @Test
    public void example1Test() {
        MinimumNumberOfPatches tester = new MinimumNumberOfPatches();
        Assert.assertEquals(2, tester.minNumberOfPatches(".X..X"));
    }

    @Test
    public void example2Test() {
        MinimumNumberOfPatches tester = new MinimumNumberOfPatches();
        Assert.assertEquals(3, tester.minNumberOfPatches("X.XXXXX.X."));
    }

    @Test
    public void example3Test() {
        MinimumNumberOfPatches tester = new MinimumNumberOfPatches();
        Assert.assertEquals(2, tester.minNumberOfPatches("XX.XXX.."));
    }

    @Test
    public void example4Test() {
        MinimumNumberOfPatches tester = new MinimumNumberOfPatches();
        Assert.assertEquals(2, tester.minNumberOfPatches("XXXX"));
    }

    @Test
    public void example5Test() {
        MinimumNumberOfPatches tester = new MinimumNumberOfPatches();
        Assert.assertEquals(0, tester.minNumberOfPatches("..."));
    }
}
