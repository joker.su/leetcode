package joker.su.interview.g42;

import org.junit.Assert;
import org.junit.Test;

public class MaximumMultiplesFourTest {

    @Test
    public void example1Test() {
        MaximumMultiplesFour tester = new MaximumMultiplesFour();
        Assert.assertEquals(84, tester.findMaximumMultiplesFour(new int[]{-6, -91, 1011, -100, 84, -22, 0, 1, 473}));
    }


    @Test
    public void example2Test() {
        MaximumMultiplesFour tester = new MaximumMultiplesFour();
        Assert.assertEquals(-84, tester.findMaximumMultiplesFour(new int[]{-6, -91, 1011, -100, -84, -22, 1, 473}));
    }

}
