package joker.su.interview.katim;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class KthSmallestSortedPairTest {

    @Test
    public void example11Test() {
        KthSmallestSortedPair tester = new KthSmallestSortedPair();
        List<Integer> result = tester.getkthSmallestTerm(List.of(3, 1, 2), 1);
        List<Integer> standard = List.of(1, 1);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example12Test() {
        KthSmallestSortedPair tester = new KthSmallestSortedPair();
        List<Integer> result = tester.getkthSmallestTerm(List.of(3, 1, 2), 2);
        List<Integer> standard = List.of(1, 2);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example13Test() {
        KthSmallestSortedPair tester = new KthSmallestSortedPair();
        List<Integer> result = tester.getkthSmallestTerm(List.of(3, 1, 2), 3);
        List<Integer> standard = List.of(1, 3);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example14Test() {
        KthSmallestSortedPair tester = new KthSmallestSortedPair();
        List<Integer> result = tester.getkthSmallestTerm(List.of(3, 1, 2), 4);
        List<Integer> standard = List.of(2, 1);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example15Test() {
        KthSmallestSortedPair tester = new KthSmallestSortedPair();
        List<Integer> result = tester.getkthSmallestTerm(List.of(3, 1, 2), 5);
        List<Integer> standard = List.of(2, 2);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example16Test() {
        KthSmallestSortedPair tester = new KthSmallestSortedPair();
        List<Integer> result = tester.getkthSmallestTerm(List.of(3, 1, 2), 6);
        List<Integer> standard = List.of(2, 3);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example17Test() {
        KthSmallestSortedPair tester = new KthSmallestSortedPair();
        List<Integer> result = tester.getkthSmallestTerm(List.of(3, 1, 2), 7);
        List<Integer> standard = List.of(3, 1);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example18Test() {
        KthSmallestSortedPair tester = new KthSmallestSortedPair();
        List<Integer> result = tester.getkthSmallestTerm(List.of(3, 1, 2), 8);
        List<Integer> standard = List.of(3, 2);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example19Test() {
        KthSmallestSortedPair tester = new KthSmallestSortedPair();
        List<Integer> result = tester.getkthSmallestTerm(List.of(3, 1, 2), 9);
        List<Integer> standard = List.of(3, 3);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example21Test() {
        KthSmallestSortedPair tester = new KthSmallestSortedPair();
        List<Integer> result = tester.getkthSmallestTerm(List.of(2, 2, 1), 1);
        List<Integer> standard = List.of(1, 1);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example22Test() {
        KthSmallestSortedPair tester = new KthSmallestSortedPair();
        List<Integer> result = tester.getkthSmallestTerm(List.of(2, 2, 1), 2);
        List<Integer> standard = List.of(1, 2);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example23Test() {
        KthSmallestSortedPair tester = new KthSmallestSortedPair();
        List<Integer> result = tester.getkthSmallestTerm(List.of(2, 2, 1), 3);
        List<Integer> standard = List.of(1, 2);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example24Test() {
        KthSmallestSortedPair tester = new KthSmallestSortedPair();
        List<Integer> result = tester.getkthSmallestTerm(List.of(2, 2, 1), 4);
        List<Integer> standard = List.of(2, 1);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example25Test() {
        KthSmallestSortedPair tester = new KthSmallestSortedPair();
        List<Integer> result = tester.getkthSmallestTerm(List.of(2, 2, 1), 5);
        List<Integer> standard = List.of(2, 1);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example26Test() {
        KthSmallestSortedPair tester = new KthSmallestSortedPair();
        List<Integer> result = tester.getkthSmallestTerm(List.of(2, 2, 1), 6);
        List<Integer> standard = List.of(2, 2);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example27Test() {
        KthSmallestSortedPair tester = new KthSmallestSortedPair();
        List<Integer> result = tester.getkthSmallestTerm(List.of(2, 2, 1), 7);
        List<Integer> standard = List.of(2, 2);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example28Test() {
        KthSmallestSortedPair tester = new KthSmallestSortedPair();
        List<Integer> result = tester.getkthSmallestTerm(List.of(2, 2, 1), 8);
        List<Integer> standard = List.of(2, 2);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }

    @Test
    public void example29Test() {
        KthSmallestSortedPair tester = new KthSmallestSortedPair();
        List<Integer> result = tester.getkthSmallestTerm(List.of(2, 2, 1), 9);
        List<Integer> standard = List.of(2, 2);
        Assert.assertTrue(CollectionUtils.isEqualCollection(standard, result));
    }
}
