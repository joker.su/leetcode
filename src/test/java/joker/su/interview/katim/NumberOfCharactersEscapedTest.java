package joker.su.interview.katim;

import org.junit.Assert;
import org.junit.Test;

public class NumberOfCharactersEscapedTest {

    @Test
    public void example1Test() {
        NumberOfCharactersEscaped tester = new NumberOfCharactersEscaped();
        Assert.assertEquals(1, tester.numberOfCharactersEscaped("#ab!c#de!f"));
    }

    @Test
    public void example2Test() {
        NumberOfCharactersEscaped tester = new NumberOfCharactersEscaped();
        Assert.assertEquals(0, tester.numberOfCharactersEscaped("##!r#po#"));
    }
}
