package joker.su.interview.vacuumlabs.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Synonyms {

    @JsonProperty("T")
    private int numberTestCases;
    private List<SynonymTestCase> testCases;
}
