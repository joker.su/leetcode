package joker.su.interview.vacuumlabs;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import joker.su.interview.vacuumlabs.model.SynonymTestCase;
import joker.su.interview.vacuumlabs.model.Synonyms;
import joker.su.util.Utils;

public class SynonymsSearcherTest {

    @Test
    public void example1Test() throws URISyntaxException, IOException {
        String input = Files.readString(Paths.get(getClass().getResource("/synonyms-example.input").toURI()));

        ObjectMapper mapper = Utils.createMapper();
        Synonyms synonyms = mapper.readValue(input, Synonyms.class);
        List<String> result = new LinkedList<>();
        for (SynonymTestCase testCase : synonyms.getTestCases()) {
            SynonymsSearcher synonymsSearch = new SynonymsSearcher(testCase.getDictionary());
            result.addAll(synonymsSearch.validatePair(testCase.getQueries()));
        }

        String outputStr = Files.readString(Paths.get(getClass().getResource("/synonyms-example.output").toURI()));
        String[] output = outputStr.replaceAll("\r", "").split("\n");
        Assert.assertArrayEquals(output, result.toArray(new String[0]));
    }

    @Test
    public void example2Test() throws URISyntaxException, IOException {
        String input = Files.readString(Paths.get(getClass().getResource("/synonyms-big-example.input").toURI()));

        ObjectMapper mapper = Utils.createMapper();
        Synonyms synonyms = mapper.readValue(input, Synonyms.class);
        List<String> result = new LinkedList<>();
        for (SynonymTestCase testCase : synonyms.getTestCases()) {
            SynonymsSearcher synonymsSearch = new SynonymsSearcher(testCase.getDictionary());
            result.addAll(synonymsSearch.validatePair(testCase.getQueries()));
        }

        String outputStr = Files.readString(Paths.get(getClass().getResource("/synonyms-big-example.output").toURI()));
        String[] output = outputStr.replaceAll("\r", "").split("\n");
        Assert.assertArrayEquals(output, result.toArray(new String[0]));
    }

    @Test
    public void exampleTestTest() throws URISyntaxException, IOException {
        String input = Files.readString(Paths.get(getClass().getResource("/synonyms-test.input").toURI()));

        ObjectMapper mapper = Utils.createMapper();
        Synonyms synonyms = mapper.readValue(input, Synonyms.class);
        List<String> result = new LinkedList<>();
        for (SynonymTestCase testCase : synonyms.getTestCases()) {
            SynonymsSearcher synonymsSearch = new SynonymsSearcher(testCase.getDictionary());
            result.addAll(synonymsSearch.validatePair(testCase.getQueries()));
        }

        String outputStr = Files.readString(Paths.get(getClass().getResource("/synonyms-test.output").toURI()));
        String[] output = outputStr.replaceAll("\r", "").split("\n");
        Assert.assertArrayEquals(output, result.toArray(new String[0]));
    }

}
