package joker.su.leetcode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;

import joker.su.leetcode.api.Pair;
import joker.su.leetcode.api.TreeNode;

/*
Given the root of a binary tree, return the vertical order traversal of its nodes' values.
(i.e., from top to bottom, column by column).
If two nodes are in the same row and column, the order should be from left to right.

Example 1:
Input: root = [3,9,20,null,null,15,7]
Output: [[9],[3,15],[20],[7]]

Example 2:
Input: root = [3,9,8,4,0,1,7]
Output: [[4],[9],[3,0,1],[8],[7]]

Example 3:
Input: root = [3,9,8,4,0,1,7,null,null,null,2,5]
Output: [[4],[9,5],[3,0,1],[8,2],[7]]

Constraints:
The number of nodes in the tree is in the range [0, 100].
-100 <= Node.val <= 100
 */
public class BinaryTreeVerticalOrderTraversal {

    public List<List<Integer>> verticalOrder(TreeNode root) {
        Map<Integer, List<Integer>> cache = new TreeMap<>();
        Queue<Pair<Integer, TreeNode>> queue = new ArrayDeque<>();
        queue.add(new Pair<>(0, root));
        while (!queue.isEmpty()) {
            Pair<Integer, TreeNode> pair = queue.poll();
            TreeNode node = pair.getValue();
            int level = pair.getKey();
            if (node == null) {
                continue;
            }
            cache.computeIfAbsent(level, key -> new ArrayList<>()).add(node.val);
            queue.add(new Pair<>(level - 1, node.left));
            queue.add(new Pair<>(level + 1, node.right));
        }
        return new ArrayList<>(cache.values());
    }
}
