package joker.su.leetcode;

/*
Given an array of integers nums which is sorted in ascending order, and an integer target, write a function to search target in nums.
If target exists, then return its index. Otherwise, return -1.

You must write an algorithm with O(log n) runtime complexity.

Example 1:

Input: nums = [-1,0,3,5,9,12], target = 9
Output: 4
Explanation: 9 exists in nums and its index is 4

Example 2:

Input: nums = [-1,0,3,5,9,12], target = 2
Output: -1
Explanation: 2 does not exist in nums so return -1


Constraints:
1 <= nums.length <= 104
-104 < nums[i], target < 104
All the integers in nums are unique.
nums is sorted in ascending order.
 */

public class BinarySearch {

    public int search(int[] nums, int target) {
        int length = nums.length;
        int index = length / 2;
        int half = index;
        int iterationNumber = index;
        while (index >= 0 && index < length && iterationNumber >= 0) {
            int current = nums[index];
            if (current == target) {
                return index;
            }
            half = half > 2 ? half / 2 : 1;
            if (current > target) {
                index = index - half;
            } else {
                index = index + half;
            }
            iterationNumber--;
        }
        return -1;
    }
}
