package joker.su.leetcode;

import java.util.ArrayList;
import java.util.List;

public class IntervalListIntersections {
    public int[][] intervalIntersection(int[][] firstList, int[][] secondList) {
        int firstPointer = 0;
        int secondPointer = 0;
        List<int[]> result = new ArrayList<>();
        while (firstPointer < firstList.length && secondPointer < secondList.length) {
            int[] first = firstList[firstPointer];
            int[] second = secondList[secondPointer];
            int left = Math.max(first[0], second[0]);
            int right = Math.min(first[1], second[1]);
            if(left <= right) {
                result.add(new int[]{left, right});
            }

            if(first[1] <= second[1]){
                firstPointer++;
            } else {
                secondPointer++;
            }
        }
        return result.toArray(new int[result.size()][]);
    }
}
