package joker.su.leetcode;

/*
Given an array of integers nums sorted in non-decreasing order, find the starting and ending position of a given target value.
If target is not found in the array, return [-1, -1].
You must write an algorithm with O(log n) runtime complexity.

Example 1:
Input: nums = [5,7,7,8,8,10], target = 8
Output: [3,4]

Example 2:
Input: nums = [5,7,7,8,8,10], target = 6
Output: [-1,-1]

Example 3:
Input: nums = [], target = 0
Output: [-1,-1]

Constraints:
0 <= nums.length <= 105
-109 <= nums[i] <= 109
nums is a non-decreasing array.
-109 <= target <= 109
 */
public class FindFirstLastPosition {

    public int[] searchRange(int[] nums, int target) {
        if (nums.length == 0) {
            return new int[]{-1, -1};
        }

        int firstId = nums.length / 2;
        int half = nums.length / 2;
        int counter = nums.length / 2;

        while (firstId >= 0 && firstId < nums.length && counter >= 0) {
            if (nums[firstId] == target) {
                if (firstId == 0 || nums[firstId - 1] < target) {
                    break;
                }
            }
            half = half > 2 ? half / 2 : 1;
            if (nums[firstId] >= target) {
                firstId = firstId - half;
            } else {
                firstId = firstId + half;
            }
            counter--;
        }
        if (counter < 0 || firstId < 0 || firstId == nums.length) {
            return new int[]{-1, -1};
        }

        half = (nums.length - firstId) / 2;
        int lastId = firstId + half;
        counter = half + 1;
        while (lastId >= firstId && lastId < nums.length && counter >= 0) {
            if (nums[lastId] == target) {
                if (lastId == nums.length - 1 || nums[lastId + 1] > target) {
                    break;
                }
            }
            half = half > 2 ? half / 2 : 1;
            if (nums[lastId] <= target) {
                lastId = lastId + half;
            } else {
                lastId = lastId - half;
            }
            counter--;
        }

        return counter <= 0 ? new int[]{firstId, -1} : new int[]{firstId, lastId};
    }
}
