package joker.su.leetcode;

/*
Given two strings s and t, return true if they are equal when both are typed into empty text editors. '#' means a backspace character.
Note that after backspacing an empty text, the text will continue empty.

Example 1:
Input: s = "ab#c", t = "ad#c"
Output: true
Explanation: Both s and t become "ac".

Example 2:
Input: s = "ab##", t = "c#d#"
Output: true
Explanation: Both s and t become "".

Example 3:
Input: s = "a#c", t = "b"
Output: false
Explanation: s becomes "c" while t becomes "b".

Constraints:
1 <= s.length, t.length <= 200
s and t only contain lowercase letters and '#' characters.

Follow up: Can you solve it in O(n) time and O(1) space?
 */
public class BackspaceStringCompare {

    public boolean backspaceCompare(String s, String t) {
        int first = moveNext(s, s.length() - 1);
        int second = moveNext(t, t.length() - 1);
        while (first >= 0 && second >= 0 && s.charAt(first) == t.charAt(second)) {
            first = moveNext(s, first - 1);
            second = moveNext(t, second - 1);
        }
        return first < 0 && second < 0;
    }

    private int moveNext(String value, int start) {
        int backCount = 0;
        while (start >= 0) {
            if(value.charAt(start) == '#') {
                backCount++;
            } else if(backCount != 0) {
                backCount--;
            } else {
                break;
            }
            start--;
        }
        return start;
    }
}
