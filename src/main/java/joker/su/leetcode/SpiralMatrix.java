package joker.su.leetcode;

import java.util.LinkedList;
import java.util.List;

/*
Given an m x n matrix, return all elements of the matrix in spiral order.

Example 1:
Input: matrix = [[1,2,3],[4,5,6],[7,8,9]]
Output: [1,2,3,6,9,8,7,4,5]

Example 2:
Input: matrix = [[1,2,3,4],[5,6,7,8],[9,10,11,12]]
Output: [1,2,3,4,8,12,11,10,9,5,6,7]


Constraints:
m == matrix.length
n == matrix[i].length
1 <= m, n <= 10
-100 <= matrix[i][j] <= 100
 */
public class SpiralMatrix {

    public List<Integer> spiralOrder(int[][] matrix) {
        int rowSize = matrix.length;
        int columnSize = matrix[0].length;
        List<Integer> answer = new LinkedList<>();
        int up = 0;
        int left = 0;
        int right = columnSize - 1;
        int down = rowSize - 1;
        while (answer.size() != rowSize * columnSize) {
            for (int i = left; i <= right; i++) {
                answer.add(matrix[up][i]);
            }

            for (int i = up + 1; i <= down; i++) {
                answer.add(matrix[i][right]);
            }

            for (int i = right - 1; i >= left && up != down; i--) {
                answer.add(matrix[down][i]);
            }

            for (int i = down - 1; i > up && left != right; i--) {
                answer.add(matrix[i][left]);
            }
            up++;
            down--;
            left++;
            right--;
        }
        return answer;
    }
}
