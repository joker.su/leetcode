package joker.su.leetcode;

/*
You are given an m x n binary matrix grid. An island is a group of 1's (representing land) connected 4-directionally (horizontal or vertical.) You may assume all four edges of the grid are surrounded by water.
The area of an island is the number of cells with a value 1 in the island.
Return the maximum area of an island in grid. If there is no island, return 0.

Example 1:
Input: grid = [[0,0,1,0,0,0,0,1,0,0,0,0,0],[0,0,0,0,0,0,0,1,1,1,0,0,0],[0,1,1,0,1,0,0,0,0,0,0,0,0],[0,1,0,0,1,1,0,0,1,0,1,0,0],[0,1,0,0,1,1,0,0,1,1,1,0,0],[0,0,0,0,0,0,0,0,0,0,1,0,0],[0,0,0,0,0,0,0,1,1,1,0,0,0],[0,0,0,0,0,0,0,1,1,0,0,0,0]]
Output: 6
Explanation: The answer is not 11, because the island must be connected 4-directionally.

Example 2:
Input: grid = [[0,0,0,0,0,0,0,0]]
Output: 0
 */
public class MaxAreaOfIsland {
    public int maxAreaOfIsland(int[][] grid) {
        int lastMax = 0;
        for (int row = 0; row < grid.length; row++){
            for (int column = 0; column < grid[row].length; column++){
                if(grid[row][column] == 1){
                    int current = calculateSize(grid, row, column);
                    if(lastMax < current) {
                        lastMax = current;
                    }
                }
            }
        }
        return lastMax;
    }

    private int calculateSize(int[][] grid, int row, int column) {
        if(grid[row][column] != 1){
            return 0;
        }
        int size = 1;
        grid[row][column] = 2;
        if (row - 1 >= 0) {
            size += calculateSize(grid, row - 1, column);
        }
        if (row + 1 < grid.length) {
            size += calculateSize(grid, row + 1, column);
        }
        if (column - 1 >= 0) {
            size += calculateSize(grid, row, column - 1);
        }
        if (column + 1 < grid[row].length) {
            size += calculateSize(grid, row, column + 1);
        }
        return size;
    }

}
