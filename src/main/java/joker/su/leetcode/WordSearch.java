package joker.su.leetcode;

/*
Given an m x n grid of characters board and a string word, return true if word exists in the grid.
The word can be constructed from letters of sequentially adjacent cells, where adjacent cells are horizontally or vertically neighboring.
The same letter cell may not be used more than once.

Example 1:
Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCCED"
Output: true

Example 2:
Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "SEE"
Output: true

Example 3:
Input: board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCB"
Output: false

Constraints:
m == board.length
n = board[i].length
1 <= m, n <= 6
1 <= word.length <= 15
board and word consists of only lowercase and uppercase English letters.
Follow up: Could you use search pruning to make your solution faster with a larger board?
 */
public class WordSearch {
    public boolean exist(char[][] board, String word) {
        for(int row = 0; row < board.length; row++) {
            for(int column = 0; column < board[0].length; column++) {
                if(search(row, column, board, word, 0)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean search(int row, int column, char[][] board, String word, int index) {
        if(index >= word.length()) {
            return true;
        }
        if(row < 0 || row == board.length || column < 0 || column == board[0].length || board[row][column] != word.charAt(index)) {
            return false;
        }
        board[row][column] = '#';
        int[] rowOffset = new int[] {0, 1, 0, -1};
        int[] columnOffset = new int[] {1, 0, -1, 0};
        boolean result = false;
        for(int i = 0; i < 4; i++) {
            result = search(row + rowOffset[i], column + columnOffset[i], board, word, index + 1);
            if(result) {
                break;
            }
        }
        board[row][column] = word.charAt(index);
        return result;
    }
}
