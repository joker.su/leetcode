package joker.su.leetcode;

import java.util.HashMap;
import java.util.Map;

/*
Given a string s, find the length of the longest substring without repeating characters.


Example 1:
Input: s = "abcabcbb"
Output: 3
Explanation: The answer is "abc", with the length of 3.

Example 2:
Input: s = "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.

Example 3:
Input: s = "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3.
Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.


Constraints:
0 <= s.length <= 5 * 104
s consists of English letters, digits, symbols and spaces.
 */
public class LongestSubstringWithoutRepeatingCharacters {

    public int lengthOfLongestSubstring(String s) {
        int count = 0;
        int lastMax = 0;
        Map<Character, Integer> cache = new HashMap<>();
        char[] symbols = s.toCharArray();
        while (count < symbols.length) {
            cache.clear();
            for (int i = count; i < symbols.length; i++) {
                char symbol = symbols[count];
                if (cache.containsKey(symbol)) {
                    if (cache.size() > lastMax) {
                        lastMax = cache.size();
                    }
                    count = cache.get(symbol) + 1;
                    break;
                }
                cache.put(symbol, i);
                count++;
            }

        }
        return Math.max(lastMax, cache.size());
    }
}
