package joker.su.leetcode;

import java.util.Optional;

import joker.su.leetcode.api.ListNode;

/*
You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order,
and each of their nodes contains a single digit. Add the two numbers and return the sum as a linked list.
You may assume the two numbers do not contain any leading zero, except the number 0 itself.

Example 1:
Input: l1 = [2,4,3], l2 = [5,6,4]
Output: [7,0,8]
Explanation: 342 + 465 = 807.

Example 2:
Input: l1 = [0], l2 = [0]
Output: [0]

Example 3:
Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
Output: [8,9,9,9,0,0,0,1]

Constraints:
The number of nodes in each linked list is in the range [1, 100].
0 <= Node.val <= 9
It is guaranteed that the list represents a number that does not have leading zeros.
 */
public class AddTwoNumbers {

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode result = new ListNode();
        ListNode current = result;
        int transfer = 0;
        while (l1 != null || l2 != null) {
            int value = Optional.ofNullable(l1).map(x -> x.val).orElse(0)
                + Optional.ofNullable(l2).map(x -> x.val).orElse(0)
                + transfer;
            current.next = new ListNode(value % 10);
            current = current.next;
            transfer = value / 10;

            l1 = Optional.ofNullable(l1).map(listNode -> listNode.next).orElse(null);
            l2 = Optional.ofNullable(l2).map(listNode -> listNode.next).orElse(null);
        }
        if (transfer != 0) {
            current.next = new ListNode(transfer);
        }
        return result.next;
    }
}

