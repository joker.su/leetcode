package joker.su.leetcode;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import joker.su.leetcode.api.NestedInteger;
import joker.su.leetcode.api.Pair;

public class NestedListWeightSum {
    public int depthSum(List<NestedInteger> nestedList) {
        List<Pair<Integer, Integer>> valueWithDepth = new LinkedList<>();
        Deque<Iterator<NestedInteger>> stack = new ArrayDeque<>();
        stack.push(nestedList.iterator());
        while (!stack.isEmpty()) {
            Iterator<NestedInteger> iterator = stack.pop();
            while (iterator.hasNext()) {
                NestedInteger value = iterator.next();
                if(value.isInteger()) {
                    valueWithDepth.add(new Pair<>(value.getInteger(), stack.size() + 1));
                } else {
                    stack.push(iterator);
                    stack.push(value.getList().iterator());
                    break;
                }
            }
        }
        return valueWithDepth.stream().mapToInt(x -> x.getKey() * x.getValue()).sum();
    }
}
