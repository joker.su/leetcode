package joker.su.leetcode;

import java.util.Arrays;

/*
Given an integer array arr, and an integer target, return the number of tuples i, j, k such that i < j < k and arr[i] + arr[j] + arr[k] == target.
As the answer can be very large, return it modulo 109 + 7.


Example 1:
Input: arr = [1,1,2,2,3,3,4,4,5,5], target = 8
Output: 20
Explanation:
Enumerating by the values (arr[i], arr[j], arr[k]):
(1, 2, 5) occurs 8 times;
(1, 3, 4) occurs 8 times;
(2, 2, 4) occurs 2 times;
(2, 3, 3) occurs 2 times.

Example 2:
Input: arr = [1,1,2,2,2,2], target = 5
Output: 12
Explanation:
arr[i] = 1, arr[j] = arr[k] = 2 occurs 12 times:
We choose one 1 from [1,1] in 2 ways,
and two 2s from [2,2,2,2] in 6 ways.

Example 3:
Input: arr = [2,1,3], target = 6
Output: 1
Explanation: (1, 2, 3) occured one time in the array so we return 1.

Constraints:
3 <= arr.length <= 3000
0 <= arr[i] <= 100
0 <= target <= 300
 */
public class Sum3WithMultiplicity {

    public int threeSumMulti(int[] arr, int target) {
        final int modulo = 1_000_000_007;
        long answer = 0;
        Arrays.sort(arr);
        for(int i = 0; i < arr.length; i++) {
            int currentTarget = target - arr[i];
            int left = i + 1;
            int right = arr.length - 1;
            while (left < right) {
                if(arr[left] + arr[right] < currentTarget) {
                    left++;
                } else if(arr[left] + arr[right] > currentTarget) {
                    right--;
                } else {
                    if(arr[left] == arr[right]) {
                        // we have n the same numbers and will calculate combination
                        answer += (right - left + 1) * (right - left) / 2;
                        answer = answer % modulo;
                        break;
                    } else {
                        long rightCount = 1;
                        long leftCount = 1;
                        while (arr[left] == arr[left + 1] && (left + 1) < right) {
                            leftCount++;
                            left++;
                        }
                        while (arr[right] == arr[right - 1] && left < (right - 1)) {
                            rightCount++;
                            right--;
                        }
                        answer += rightCount * leftCount;
                        answer = answer % modulo;
                        left++;
                        right--;
                    }
                }
            }
        }
        return (int)answer;
    }
}
