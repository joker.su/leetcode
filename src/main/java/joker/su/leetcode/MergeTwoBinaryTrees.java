package joker.su.leetcode;

import joker.su.leetcode.api.TreeNode;

/*
You are given two binary trees root1 and root2.
Imagine that when you put one of them to cover the other, some nodes of the two trees are overlapped while the others are not. You need to merge the two trees into a new binary tree. The merge rule is that if two nodes overlap, then sum node values up as the new value of the merged node. Otherwise, the NOT null node will be used as the node of the new tree.
Return the merged tree.
Note: The merging process must start from the root nodes of both trees.

Example 1:
Input: root1 = [1,3,2,5], root2 = [2,1,3,null,4,null,7]
Output: [3,4,5,5,4,null,7]

Example 2:
Input: root1 = [1], root2 = [1,2]
Output: [2,2]

Constraints:
The number of nodes in both trees is in the range [0, 2000].
-104 <= Node.val <= 104
 */
public class MergeTwoBinaryTrees {

    public TreeNode mergeTrees(TreeNode root1, TreeNode root2) {
        TreeNode merged = new TreeNode();
        if (root1 != null || root2 != null) {
            mergeTrees(merged, root1, root2);
            return merged;
        }
        return null;
    }

    private void mergeTrees(TreeNode merged, TreeNode root1, TreeNode root2) {
        merged.val = (root1 != null ? root1.val : 0) + (root2 != null ? root2.val : 0);

        if ((root1 != null && root1.left != null) || (root2 != null && root2.left != null)) {
            TreeNode left = new TreeNode();
            merged.left = left;
            mergeTrees(left, root1 != null ? root1.left : null, root2 != null ? root2.left : null);
        }

        if ((root1 != null && root1.right != null) || (root2 != null && root2.right != null)) {
            TreeNode right = new TreeNode();
            merged.right = right;
            mergeTrees(right, root1 != null ? root1.right : null, root2 != null ? root2.right : null);
        }
    }
}
