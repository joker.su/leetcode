package joker.su.leetcode;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Objects;
import java.util.Queue;

/*
You are given an m x n integer matrix grid where each cell is either 0 (empty) or 1 (obstacle). You can move up, down, left, or right from and to an empty cell in one step.
Return the minimum number of steps to walk from the upper left corner (0, 0) to the lower right corner (m - 1, n - 1) given that you can eliminate at most k obstacles. If it is not possible to find such walk return -1.

Example 1:
Input: grid = [[0,0,0],[1,1,0],[0,0,0],[0,1,1],[0,0,0]], k = 1
Output: 6
Explanation:
The shortest path without eliminating any obstacle is 10.
The shortest path with one obstacle elimination at position (3,2) is 6. Such path is (0,0) -> (0,1) -> (0,2) -> (1,2) -> (2,2) -> (3,2) -> (4,2).

Example 2:
Input: grid = [[0,1,1],[1,1,1],[1,0,0]], k = 1
Output: -1
Explanation: We need to eliminate at least two obstacles to find such a walk.

Constraints:
m == grid.length
n == grid[i].length
1 <= m, n <= 40
1 <= k <= m * n
grid[i][j] is either 0 or 1.
grid[0][0] == grid[m - 1][n - 1] == 0
 */
public class ShortestPathInGridWithObstaclesElimination {

    private static final class StepState {
        private final int path,  row, column, remainingElimination;

        private StepState(int path, int row, int column, int remainingElimination) {
            this.path = path;
            this.row = row;
            this.column = column;
            this.remainingElimination = remainingElimination;
        }

        public int getPath() {
            return path;
        }

        public int getColumn() {
            return column;
        }

        public int getRow() {
            return row;
        }

        public int getRemainingElimination() {
            return remainingElimination;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            StepState stepState = (StepState) o;
            return column == stepState.column && row == stepState.row && remainingElimination == stepState.remainingElimination;
        }

        @Override
        public int hashCode() {
            return Objects.hash(column, row, remainingElimination);
        }
    }

    public int shortestPath(int[][] grid, int k) {
        int rows = grid.length;
        int columns = grid[0].length;

        if(k >= rows + columns - 2) {
            return rows + columns - 2;
        }
        HashSet<StepState> visitedSteps = new HashSet<>();
        Queue<StepState> queue = new ArrayDeque<>();
        StepState firstStep = new StepState(0, 0, 0, k);
        queue.add(firstStep);
        visitedSteps.add(firstStep);

        while (!queue.isEmpty()) {
            StepState step = queue.poll();
            if(step.getColumn() == columns - 1 && step.getRow() == rows - 1) {
                return step.path;
            }

            int[] stepDiff = new int[]{0, 1,
                                       1, 0,
                                       0, -1,
                                       -1, 0};
            for (int i = 0; i < stepDiff.length; i +=2) {
                int nextRow = step.getRow() + stepDiff[i];
                int nextColumn = step.getColumn() +  stepDiff[i + 1];

                if(nextRow < 0 || nextRow == rows || nextColumn < 0 || nextColumn == columns) {
                    continue;
                }
                int nextElimination = step.getRemainingElimination() - grid[nextRow][nextColumn];
                StepState nextStep = new StepState(step.getPath() + 1, nextRow, nextColumn, nextElimination);
                if(nextElimination >= 0 && !visitedSteps.contains(nextStep)) {
                    queue.add(nextStep);
                    visitedSteps.add(nextStep);
                }
            }
        }
        return -1;
    }


}
