package joker.su.leetcode;

import java.util.ArrayDeque;
import java.util.Queue;

import joker.su.leetcode.api.Pair;

/*
You are given an m x n grid where each cell can have one of three values:
0 representing an empty cell,
1 representing a fresh orange, or
2 representing a rotten orange.
Every minute, any fresh orange that is 4-directionally adjacent to a rotten orange becomes rotten.
Return the minimum number of minutes that must elapse until no cell has a fresh orange. If this is impossible, return -1.

Example 1:
Input: grid = [[2,1,1],[1,1,0],[0,1,1]]
Output: 4

Example 2:
Input: grid = [[2,1,1],[0,1,1],[1,0,1]]
Output: -1
Explanation: The orange in the bottom left corner (row 2, column 0) is never rotten, because rotting only happens 4-directionally.

Example 3:
Input: grid = [[0,2]]
Output: 0
Explanation: Since there are already no fresh oranges at minute 0, the answer is just 0.

Constraints:
m == grid.length
n == grid[i].length
1 <= m, n <= 10
grid[i][j] is 0, 1, or 2.
 */
public class RottingOranges {

    public int orangesRotting(int[][] grid) {
        Queue<Pair<Integer, Integer>> rottenQueue = new ArrayDeque<>();
        int fresh = 0;
        int result = -1;
        int rowSize = grid.length;
        int columnSize = grid[0].length;

        for (int row = 0; row < rowSize; row++) {
            for (int column = 0; column < columnSize; column++) {
                if (grid[row][column] == 2) {
                    rottenQueue.offer(new Pair<>(row, column));
                } else if (grid[row][column] == 1) {
                    fresh++;
                }
            }
        }

        rottenQueue.offer(new Pair<>(-1, -1));
        int[][] steps = {{0, -1}, {0, 1}, {-1, 0}, {1, 0}};

        while (!rottenQueue.isEmpty()) {
            Pair<Integer, Integer> rottenOrange = rottenQueue.poll();
            int rowOfOrange = rottenOrange.getKey();
            int columnOfOrange = rottenOrange.getValue();

            if (rowOfOrange == -1) {
                result++;
                if (!rottenQueue.isEmpty()) {
                    rottenQueue.offer(new Pair<>(-1, -1));
                }
                continue;
            }

            for (int[] step : steps) {
                int neighborRow = rowOfOrange + step[0];
                int neighborColumn = columnOfOrange + step[1];
                if (neighborRow >= 0 && neighborRow < rowSize &&
                    neighborColumn >= 0 && neighborColumn < columnSize) {
                    if (grid[neighborRow][neighborColumn] == 1) {
                        grid[neighborRow][neighborColumn] = 2;
                        rottenQueue.offer(new Pair<>(neighborRow, neighborColumn));
                        fresh--;
                    }
                }
            }
        }
        return fresh == 0 ? result : -1;
    }
}
