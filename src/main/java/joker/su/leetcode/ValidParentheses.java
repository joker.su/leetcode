package joker.su.leetcode;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

/*
Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:
Open brackets must be closed by the same type of brackets.
Open brackets must be closed in the correct order.
Every close bracket has a corresponding open bracket of the same type.


Example 1:
Input: s = "()"
Output: true

Example 2:
Input: s = "()[]{}"
Output: true

Example 3:
Input: s = "(]"
Output: false

Constraints:
1 <= s.length <= 104
s consists of parentheses only '()[]{}'.
 */
public class ValidParentheses {

    private final Map<Character, Integer> characters = new HashMap<>() {{
        put('(', 1);
        put(')', -1);
        put('{', 10);
        put('}', -10);
        put('[', 100);
        put(']', -100);
    }};

    public boolean isValid(String s) {
        Deque<Integer> stack = new ArrayDeque<>();
        for(int i = 0; i < s.length(); i++) {
            int charWeight = characters.get(s.charAt(i));
            if(charWeight > 0) {
                stack.push(charWeight);
            } else {
                if(stack.isEmpty() || charWeight + stack.pop() != 0) {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }
}
