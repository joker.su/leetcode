package joker.su.leetcode;

/*
Given an m x n binary matrix mat, return the distance of the nearest 0 for each cell.
The distance between two adjacent cells is 1.

Example 1:
Input: mat = [[0,0,0],[0,1,0],[0,0,0]]
Output: [[0,0,0],[0,1,0],[0,0,0]]

Example 2:
Input: mat = [[0,0,0],[0,1,0],[1,1,1]]
Output: [[0,0,0],[0,1,0],[1,2,1]]

Constraints:
m == mat.length
n == mat[i].length
1 <= m, n <= 104
1 <= m * n <= 104
mat[i][j] is either 0 or 1.
There is at least one 0 in mat.
 */
public class Matrix {

    public int[][] updateMatrix(int[][] mat) {
        if (mat.length == 0) {
            return mat;
        }
        int totalRows = mat.length;
        int totalColumn = mat[0].length;
        int[][] result = new int[totalRows][];
        for (int row = 0; row < totalRows; row++) {
            result[row] = new int[totalColumn];
            for (int column = 0; column < totalColumn; column++) {
                result[row][column] = Integer.MAX_VALUE / 2;
            }
        }

        for (int row = 0; row < totalRows; row++) {
            for (int column = 0; column < totalColumn; column++) {
                if (mat[row][column] == 0) {
                    result[row][column] = 0;
                    continue;
                }
                if (column > 0) {
                    result[row][column] = Math.min(result[row][column], result[row][column - 1] + 1);
                }
                if (row > 0) {
                    result[row][column] = Math.min(result[row][column], result[row - 1][column] + 1);
                }
            }
        }

        for (int row = totalRows - 1; row >= 0; row--) {
            for (int column = totalColumn - 1; column >= 0; column--) {
                if (column + 1 < totalColumn) {
                    result[row][column] = Math.min(result[row][column], result[row][column + 1] + 1);
                }
                if (row + 1 < totalRows) {
                    result[row][column] = Math.min(result[row][column], result[row + 1][column] + 1);
                }
            }
        }
        return result;
    }
}
