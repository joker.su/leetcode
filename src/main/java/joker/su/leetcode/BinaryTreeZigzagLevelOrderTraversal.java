package joker.su.leetcode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

import joker.su.leetcode.api.TreeNode;

public class BinaryTreeZigzagLevelOrderTraversal {

    private final Random random = new Random();
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        if(random.nextBoolean()) {
            dfs(root, result, 0);
        } else {
            bfs(root, result);
        }
        return result;
    }

    private void dfs(TreeNode node, List<List<Integer>> result, int level) {
        if(node == null){
            return;
        }
        if(level >= result.size()) {
            List<Integer> levelList = new LinkedList<>();
            result.add(levelList);
            levelList.add(node.val);
        } else if(level % 2 == 0) {
            result.get(level).add(node.val);
        } else {
            result.get(level).add(0, node.val);
        }

        dfs(node.left, result, level + 1);
        dfs(node.right, result, level + 1);
    }

    private void bfs(TreeNode root, List<List<Integer>> result) {
        if(root == null) {
            return;
        }
        Queue<TreeNode> queue = new ArrayDeque<>();
        queue.add(root);
        boolean even = true;
        while (!queue.isEmpty()) {
            int levelSize = queue.size();
            List<Integer> levelList = new LinkedList<>();
            for(int i = 0; i < levelSize; i++){
                TreeNode node = queue.poll();
                if(node.left != null) {
                    queue.add(node.left);
                }
                if(node.right != null) {
                    queue.add(node.right);
                }
                if(even) {
                    levelList.add(node.val);
                } else {
                    levelList.add(0, node.val);
                }
            }
            result.add(levelList);
            even = !even;
        }
    }
}
