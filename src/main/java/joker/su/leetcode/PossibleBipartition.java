package joker.su.leetcode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;

/*
We want to split a group of n people (labeled from 1 to n) into two groups of any size.
Each person may dislike some other people, and they should not go into the same group.
Given the integer n and the array dislikes where dislikes[i] = [ai, bi] indicates that the person labeled ai does not like the person labeled bi
return true if it is possible to split everyone into two groups in this way.

Example 1:
Input: n = 4, dislikes = [[1,2],[1,3],[2,4]]
Output: true
Explanation: The first group has [1,4], and the second group has [2,3].

Example 2:
Input: n = 3, dislikes = [[1,2],[1,3],[2,3]]
Output: false
Explanation: We need at least 3 groups to divide them. We cannot put them in two groups.

Constraints:
1 <= n <= 2000
0 <= dislikes.length <= 104
dislikes[i].length == 2
1 <= ai < bi <= n
All the pairs of dislikes are unique.
 */
public class PossibleBipartition {

    public boolean possibleBipartition(int n, int[][] dislikes) {
        Map<Integer, List<Integer>> graph = new HashMap<>();
        for(int[] edge : dislikes){
            graph.computeIfAbsent(edge[0], x -> new ArrayList<>()).add(edge[1]);
            graph.computeIfAbsent(edge[1], x -> new ArrayList<>()).add(edge[0]);
        }
        
        int[] color = new int[n + 1];
        Arrays.fill(color, -1);
        for(int i = 1; i <= n; i++) {
            if(color[i] == -1) {
                if(!bfs(i, graph, color)) {
                    return false;
                }
            }
        }
        
        return true;
    }

    private boolean bfs(int i, Map<Integer, List<Integer>> graph, int[] color) {
        Queue<Integer> queue = new ArrayDeque<>();
        queue.add(i);
        color[i] = 0;

        while (!queue.isEmpty()) {
            int node = queue.poll();
            if(!graph.containsKey(node)) {
                continue;
            }
            for(int neighbor: graph.get(node)) {
                if(color[neighbor] == color[node]) {
                    return false;
                }
                if(color[neighbor] == -1) {
                    color[neighbor] = 1 - color[node];
                    queue.add(neighbor);
                }
            }
        }
        return true;
    }
}
