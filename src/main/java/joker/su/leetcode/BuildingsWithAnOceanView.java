package joker.su.leetcode;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Stack;

public class BuildingsWithAnOceanView {
    public int[] findBuildings(int[] heights) {
        Deque<Integer> stack = new ArrayDeque<>();
        int maxHeight = 0;

        for(int i = heights.length - 1; i >=0; i--) {
            if(maxHeight < heights[i]) {
                maxHeight = heights[i];
                stack.push(i);
            }
        }

        int[] result = new int[stack.size()];
        for(int i = 0; i < result.length; i++) {
            result[i] = stack.pop();
        }
        return result;
    }
}
