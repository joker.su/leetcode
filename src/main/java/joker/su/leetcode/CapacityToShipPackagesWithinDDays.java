package joker.su.leetcode;

public class CapacityToShipPackagesWithinDDays {

    public int shipWithinDays(int[] weights, int days) {
        int maxWeight = 0;
        int totalWeight = 0;

        for(int weight : weights) {
            totalWeight += weight;
            maxWeight = Math.max(maxWeight, weight);
        }

        int left = maxWeight;
        int right = totalWeight;

        while(left < right) {
            int middle = (right + left) / 2;
            if(feasible(weights, days, middle)) {
                right = middle;
            } else {
                left = middle + 1;
            }
        }
        return left;


    }

    private boolean feasible(int[] weights, int days, int capacity) {
        int neededDays = 0;
        int currentShip = 0;
        for(int weight : weights) {
            if(currentShip + weight > capacity) {
                neededDays++;
                currentShip = 0;
            }
            currentShip += weight;
        }
        if(currentShip > 0) {
            neededDays++;
        }
        return days >= neededDays;
    }

}
