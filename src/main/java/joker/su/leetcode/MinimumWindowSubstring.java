package joker.su.leetcode;

import java.util.HashMap;
import java.util.Map;

/*
Given two strings s and t of lengths m and n respectively, return the minimum window substring of s such
that every character in t (including duplicates) is included in the window. If there is no such substring, return the empty string "".
The testcases will be generated such that the answer is unique.

Example 1:
Input: s = "ADOBECODEBANC", t = "ABC"
Output: "BANC"
Explanation: The minimum window substring "BANC" includes 'A', 'B', and 'C' from string t.

Example 2:
Input: s = "a", t = "a"
Output: "a"
Explanation: The entire string s is the minimum window.

Example 3:
Input: s = "a", t = "aa"
Output: ""
Explanation: Both 'a's from t must be included in the window.
Since the largest window of s only has one 'a', return empty string.


Constraints:
m == s.length
n == t.length
1 <= m, n <= 105
s and t consist of uppercase and lowercase English letters.
 */
public class MinimumWindowSubstring {

    public String minWindow(String s, String t) {
        if (t.length() > s.length()) {
            return "";
        }
        int minSize = Integer.MAX_VALUE;
        int startPosition = -1;
        int endPosition = -1;

        int left = 0;
        int right = 0;
        Map<Character, Integer> symbols = new HashMap<>();
        Map<Character, Integer> window = new HashMap<>();
        for (char symbol : t.toCharArray()) {
            symbols.put(symbol, symbols.getOrDefault(symbol, 0) + 1);
        }

        int requiredSymbols = symbols.size();
        int foundSymbols = 0;
        while (right < s.length()) {
            char symbol = s.charAt(right);
            window.put(symbol, window.getOrDefault(symbol, 0) + 1);
            if (symbols.containsKey(symbol) && symbols.get(symbol).intValue() == window.get(symbol).intValue()) {
                foundSymbols++;
            }
            while (requiredSymbols == foundSymbols && left <= right) {
                symbol = s.charAt(left);
                int size = right - left + 1;
                if (minSize > size) {
                    minSize = size;
                    startPosition = left;
                    endPosition = right;
                }
                window.put(symbol, window.get(symbol) - 1);
                if (symbols.containsKey(symbol) && symbols.get(symbol) > window.get(symbol)) {
                    foundSymbols--;
                }
                left++;
            }
            right++;
        }
        return minSize == Integer.MAX_VALUE ? "" : s.substring(startPosition, endPosition + 1);
    }
}
