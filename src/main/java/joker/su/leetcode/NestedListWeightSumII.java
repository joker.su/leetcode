package joker.su.leetcode;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import joker.su.leetcode.api.NestedInteger;
import joker.su.leetcode.api.Pair;

/*
You are given a nested list of integers nestedList. Each element is either an integer or a list whose elements may also be integers or other lists.
The depth of an integer is the number of lists that it is inside of.
For example, the nested list [1,[2,2],[[3],2],1] has each integer's value set to its depth. Let maxDepth be the maximum depth of any integer.
The weight of an integer is maxDepth - (the depth of the integer) + 1.
Return the sum of each integer in nestedList multiplied by its weight.


Example 1:
Input: nestedList = [[1,1],2,[1,1]]
Output: 8
Explanation: Four 1's with a weight of 1, one 2 with a weight of 2.
1*1 + 1*1 + 2*2 + 1*1 + 1*1 = 8

Example 2:
Input: nestedList = [1,[4,[6]]]
Output: 17
Explanation: One 1 at depth 3, one 4 at depth 2, and one 6 at depth 1.
1*3 + 4*2 + 6*1 = 17

Constraints:
1 <= nestedList.length <= 50
The values of the integers in the nested list is in the range [-100, 100].
The maximum depth of any integer is less than or equal to 50.
 */
public class NestedListWeightSumII {
    public int depthSumInverse(List<NestedInteger> nestedList) {
        List<Pair<Integer, Integer>> valueWithDepth = new LinkedList<>();
        int maxDepth = 1;
        Deque<Iterator<NestedInteger>> stack = new ArrayDeque<>();
        stack.push(nestedList.iterator());
        while (!stack.isEmpty()) {
            Iterator<NestedInteger> iterator = stack.pop();
            while (iterator.hasNext()) {
                NestedInteger value = iterator.next();
                maxDepth = Math.max(maxDepth, stack.size() + 1);
                if(value.isInteger()){
                    valueWithDepth.add(Pair.of(value.getInteger(), stack.size() + 1));
                } else {
                    stack.push(iterator);
                    stack.push(value.getList().iterator());
                    break;
                }
            }
        }
        int finalMaxDepth = maxDepth;
        return valueWithDepth.stream().mapToInt(x -> x.getKey() * (finalMaxDepth - x.getValue() + 1)).sum();
    }
}
