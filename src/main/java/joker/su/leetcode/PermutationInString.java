package joker.su.leetcode;

import java.util.HashMap;
import java.util.Map;

/*
Given two strings s1 and s2, return true if s2 contains a permutation of s1, or false otherwise.
In other words, return true if one of s1's permutations is the substring of s2.


Example 1:
Input: s1 = "ab", s2 = "eidbaooo"
Output: true
Explanation: s2 contains one permutation of s1 ("ba").

Example 2:
Input: s1 = "ab", s2 = "eidboaoo"
Output: false


Constraints:
1 <= s1.length, s2.length <= 104
s1 and s2 consist of lowercase English letters.
 */
public class PermutationInString {

    public boolean checkInclusion(String s1, String s2) {
        if (s2.equals(s1)) {
            return true;
        }
        Map<Character, Integer> cache = new HashMap<>();
        char[] symbols1 = s1.toCharArray();
        for (char c : symbols1) {
            cache.put(c, cache.getOrDefault(c, 0) + 1);
        }
        char[] symbols2 = s2.toCharArray();
        int pointer = 0;
        Map<Character, Integer> founded = new HashMap<>();
        while (pointer <= symbols2.length - symbols1.length) {
            boolean error = false;
            for (int i = 0; i < symbols1.length; i++) {
                if (!cache.containsKey(symbols2[pointer + i])) {
                    error = true;
                    break;
                } else {
                    founded.put(symbols2[pointer + i], founded.getOrDefault(symbols2[pointer + i], 0) + 1);
                }
            }
            if (error || !same(founded, cache)) {
                pointer++;
                founded.clear();
            } else {
                return true;
            }
        }
        return false;
    }

    private boolean same(Map<Character, Integer> first, Map<Character, Integer> second) {
        if (first.size() != second.size()) {
            return false;
        }
        for (char s : first.keySet()) {
            if (!first.get(s).equals(second.get(s))) {
                return false;
            }
        }
        return true;
    }
}
