package joker.su.leetcode;

/*
Given a string s, reverse only all the vowels in the string and return it.
The vowels are 'a', 'e', 'i', 'o', and 'u', and they can appear in both lower and upper cases, more than once.

Example 1:
Input: s = "hello"
Output: "holle"

Example 2:
Input: s = "leetcode"
Output: "leotcede"

Constraints:
1 <= s.length <= 3 * 105
s consist of printable ASCII characters.
 */
public class ReverseVowelsOfString {
    public String reverseVowels(String s) {
        int left = 0;
        int right = s.length() - 1;
        char[] data = s.toCharArray();

        while(left < right) {
            while(left < s.length() && !isVowel(data[left])) {
                left++;
            }
            while(right >= 0 && !isVowel(data[right])) {
                right--;
            }
            if(left < right) {
                char temp = data[left];
                data[left] = data[right];
                data[right] = temp;

                left++;
                right--;
            }
        }
        return new String(data);
    }

    private boolean isVowel(char symbol) {
        return 'a' == symbol || 'e' == symbol || 'i' == symbol || 'o' == symbol || 'u' == symbol ||
            'A' == symbol || 'E' == symbol || 'I' == symbol || 'O' == symbol || 'U' == symbol;
    }
}
