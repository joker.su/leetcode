package joker.su.leetcode;

import java.util.ArrayDeque;
import java.util.Queue;

import joker.su.leetcode.api.Pair;

/*
You are starving and you want to eat food as quickly as possible. You want to find the shortest path to arrive at any food cell.
You are given an m x n character matrix, grid, of these different types of cells:
'*' is your location. There is exactly one '*' cell.
'#' is a food cell. There may be multiple food cells.
'O' is free space, and you can travel through these cells.
'X' is an obstacle, and you cannot travel through these cells.
You can travel to any adjacent cell north, east, south, or west of your current location if there is not an obstacle.

Return the length of the shortest path for you to reach any food cell. If there is no path for you to reach food, return -1.

Example 1:
Input: grid = [["X","X","X","X","X","X"],["X","*","O","O","O","X"],["X","O","O","#","O","X"],["X","X","X","X","X","X"]]
Output: 3
Explanation: It takes 3 steps to reach the food.

Example 2:
Input: grid = [["X","X","X","X","X"],["X","*","X","O","X"],["X","O","X","#","X"],["X","X","X","X","X"]]
Output: -1
Explanation: It is not possible to reach the food.

Example 3:
Input: grid = [["X","X","X","X","X","X","X","X"],["X","*","O","X","O","#","O","X"],["X","O","O","X","O","O","X","X"],["X","O","O","O","O","#","O","X"],["X","X","X","X","X","X","X","X"]]
Output: 6
Explanation: There can be multiple food cells. It only takes 6 steps to reach the bottom food.

Constraints:
m == grid.length
n == grid[i].length
1 <= m, n <= 200
grid[row][col] is '*', 'X', 'O', or '#'.
The grid contains exactly one '*'.
 */
public class ShortestPathToGetFood {

    public int getFood(char[][] grid) {
        int rows = grid.length;
        int columns = grid[0].length;
        Queue<Pair<Integer, Integer>> queue  = new ArrayDeque<>();
        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < columns && queue.isEmpty(); j++) {
                if (grid[i][j] == '*') {
                    queue.add(Pair.of(i, j));
                    queue.add(Pair.of(-1, -1));
                    break;
                }
            }
        }

        int[][] directions = {{1,0}, {-1,0}, {0,1}, {0,-1}};
        int distance = 0;
        while (!queue.isEmpty()) {
            int currentRow = queue.peek().getKey();
            int currentColumn = queue.poll().getValue();
            if(currentRow == -1 && currentColumn == -1) {
                distance++;
                if(!queue.isEmpty()) {
                    queue.add(Pair.of(-1, -1));
                }
                continue;
            }
            for(int i = 0; i < 4; i++) {
                int nextRow = currentRow + directions[i][0];
                int nextColumn = currentColumn + directions[i][1];
                if(nextRow >= 0 && nextRow < rows && nextColumn >= 0 && nextColumn < columns) {
                    if (grid[nextRow][nextColumn] == '#') {
                        return distance + 1;
                    } else if (grid[nextRow][nextColumn] == 'O') {
                        queue.add(Pair.of(nextRow, nextColumn));
                        grid[nextRow][nextColumn] = '1';
                    }
                }
            }
        }
        return -1;
    }
}
