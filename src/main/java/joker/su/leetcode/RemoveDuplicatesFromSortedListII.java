package joker.su.leetcode;

import joker.su.leetcode.api.ListNode;
/*
Given the head of a sorted linked list, delete all nodes that have duplicate numbers, leaving only distinct numbers from the original list.
Return the linked list sorted as well.

Example 1:
Input: head = [1,2,3,3,4,4,5]
Output: [1,2,5]

Example 2:
Input: head = [1,1,1,2,3]
Output: [2,3]

Constraints:
The number of nodes in the list is in the range [0, 300].
-100 <= Node.val <= 100
The list is guaranteed to be sorted in ascending order.
 */
public class RemoveDuplicatesFromSortedListII {

    public ListNode deleteDuplicates(ListNode head) {
        ListNode result = new ListNode();
        ListNode node = result;
        int currentDup = Integer.MIN_VALUE;
        while (head != null){
            if(head.val == currentDup || head.next != null && head.next.val == head.val) {
                currentDup = head.val;
            } else {
                node.next = new ListNode(head.val);
                node = node.next;
            }
            head = head.next;
        }
        return result.next;
    }
}
