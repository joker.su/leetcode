package joker.su.leetcode;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/*
Given a string containing digits from 2-9 inclusive, return all possible letter combinations that the number could represent.
Return the answer in any order.
A mapping of digits to letters (just like on the telephone buttons) is given below. Note that 1 does not map to any letters.

Example 1:
Input: digits = "23"
Output: ["ad","ae","af","bd","be","bf","cd","ce","cf"]

Example 2:
Input: digits = ""
Output: []

Example 3:
Input: digits = "2"
Output: ["a","b","c"]

Constraints:
0 <= digits.length <= 4
digits[i] is a digit in the range ['2', '9'].
*/
public class LetterCombinationsOfPhoneNumber {
    private final Map<Character, String> letters = Map.of(
        '2', "abc", '3', "def", '4', "ghi", '5', "jkl",
        '6', "mno", '7', "pqrs", '8', "tuv", '9', "wxyz");

    public List<String> letterCombinations(String digits) {
        List<String> answer = new ArrayList<>();
        if(digits.length() == 0) {
            return answer;
        }
        StringBuilder builder = new StringBuilder();
        backtrack(answer, digits, builder, 0);
        return answer;
    }

    private void backtrack(List<String> answer, String digits, StringBuilder builder, int index) {
        if(builder.length() == digits.length()) {
            answer.add(builder.toString());
            return;
        }
        String digitLetters = letters.get(digits.charAt(index));
        for(char symbol: digitLetters.toCharArray()) {
            builder.append(symbol);
            backtrack(answer, digits, builder, index + 1);
            builder.setLength(builder.length() - 1);
        }
    }
}
