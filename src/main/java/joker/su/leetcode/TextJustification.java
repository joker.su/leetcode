package joker.su.leetcode;

import java.util.LinkedList;
import java.util.List;

/*
Given an array of strings words and a width maxWidth, format the text such that each line has exactly maxWidth characters and
 is fully (left and right) justified.
You should pack your words in a greedy approach; that is, pack as many words as you can in each line.
 Pad extra spaces ' ' when necessary so that each line has exactly maxWidth characters.
Extra spaces between words should be distributed as evenly as possible. If the number of spaces on a line does not divide evenly
 between words, the empty slots on the left will be assigned more spaces than the slots on the right.
For the last line of text, it should be left-justified, and no extra space is inserted between words.

Note:
A word is defined as a character sequence consisting of non-space characters only.
Each word's length is guaranteed to be greater than 0 and not exceed maxWidth.
The input array words contains at least one word.

Example 1:
Input: words = ["This", "is", "an", "example", "of", "text", "justification."], maxWidth = 16
Output:
[
   "This    is    an",
   "example  of text",
   "justification.  "
]

Example 2:
Input: words = ["What","must","be","acknowledgment","shall","be"], maxWidth = 16
Output:
[
  "What   must   be",
  "acknowledgment  ",
  "shall be        "
]
Explanation: Note that the last line is "shall be    " instead of "shall     be", because the last line must be left-justified instead of fully-justified.
Note that the second line is also left-justified because it contains only one word.

Example 3:
Input: words = ["Science","is","what","we","understand","well","enough","to","explain","to","a","computer.","Art","is","everything","else","we","do"], maxWidth = 20
Output:
[
  "Science  is  what we",
  "understand      well",
  "enough to explain to",
  "a  computer.  Art is",
  "everything  else  we",
  "do                  "
]


Constraints:
1 <= words.length <= 300
1 <= words[i].length <= 20
words[i] consists of only English letters and symbols.
1 <= maxWidth <= 100
words[i].length <= maxWidth
 */
public class TextJustification {

    public List<String> fullJustify(String[] words, int maxWidth) {
        List<String> result = new LinkedList<>();
        if(words.length == 0) {
            return result;
        }

        int left = 0;
        int bufferSize = 0;
        for(int right = 0; right <= words.length; right++) {
            if(right == words.length || bufferSize + words[right].length() + right - left > maxWidth) {
                result.add(alignment(words, left, right, bufferSize, maxWidth));
                left = right;
                bufferSize = 0;
            }
            bufferSize += right == words.length ? 0 : words[right].length();
        }

        return result;

    }

    private String alignment(String[] words, int lineStart, int lineEnd, int lineSize, int lineMaxWidth) {
        int spacesCount = lineEnd - 1 == lineStart ? 1 : lineEnd - lineStart - 1;
        int extraSpace = lineMaxWidth - lineSize;
        StringBuilder builder = new StringBuilder(lineMaxWidth + extraSpace);
        if(words.length == lineEnd) {
            // last line of text should be left-justifie
            for (int i = lineStart; i < lineEnd; i++) {
                builder.append(words[i]).append(" ");
            }
            if(builder.length() < lineMaxWidth) {
                builder.append(" ".repeat(extraSpace - spacesCount));
            }
        } else {
            int spacer = extraSpace / spacesCount;
            for (int i = lineStart; i < lineEnd; i++) {
                if(extraSpace % spacesCount != 0) {
                    builder.append(words[i]).append(" ".repeat(spacer + 1));
                    spacesCount--;
                    extraSpace -= spacer + 1;
                } else {
                    builder.append(words[i]).append(" ".repeat(spacer));
                }
            }

        }
        builder.setLength(lineMaxWidth);
        return builder.toString();
    }
}
