package joker.su.leetcode;
/*
Given an m x n 2D binary grid grid which represents a map of '1's (land) and '0's (water), return the number of islands.
An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically. You may assume all four edges of the grid are all surrounded by water.

Example 1:
Input: grid = [
  ["1","1","1","1","0"],
  ["1","1","0","1","0"],
  ["1","1","0","0","0"],
  ["0","0","0","0","0"]
]
Output: 1

Example 2:
Input: grid = [
  ["1","1","0","0","0"],
  ["1","1","0","0","0"],
  ["0","0","1","0","0"],
  ["0","0","0","1","1"]
]
Output: 3

Constraints:
m == grid.length
n == grid[i].length
1 <= m, n <= 300
grid[i][j] is '0' or '1'.
 */
public class NumberOfIslands {

    public int numIslands(char[][] grid) {
        int answer = 0;

        for (int row = 0; row < grid.length; row++) {
            for (int column = 0; column < grid[row].length; column++) {
                if (grid[row][column] == '1') {
                    answer++;
                    markIsland(grid, row, column);
                }
            }
        }
        return answer;
    }

    private void markIsland(char[][] grid, int row, int column) {
        if (row < 0 || row == grid.length || column < 0 || column == grid[row].length || grid[row][column] != '1') {
            return;
        }
        grid[row][column] = '#';
        int[] rowOffset = new int[]{0, 1, 0, -1};
        int[] columnOffset = new int[]{1, 0, -1, 0};
        for (int i = 0; i < 4; i++) {
            markIsland(grid, row + rowOffset[i], column + columnOffset[i]);
        }
    }
}
