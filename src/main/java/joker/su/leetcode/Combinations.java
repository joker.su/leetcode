package joker.su.leetcode;

import java.util.LinkedList;
import java.util.List;

/*
Given two integers n and k, return all possible combinations of k numbers out of the range [1, n].
You may return the answer in any order.

Example 1:
Input: n = 4, k = 2
Output:
[
  [2,4],
  [3,4],
  [2,3],
  [1,2],
  [1,3],
  [1,4],
]

Example 2:
Input: n = 1, k = 1
Output: [[1]]

Constraints:
1 <= n <= 20
1 <= k <= n
*/
public class Combinations {

    private final List<List<Integer>> result = new LinkedList<>();
    private int n;
    private int k;

    public List<List<Integer>> combine(int n, int k) {
        this.n = n;
        this.k = k;
        backtrack(1, new LinkedList<>());
        return result;
    }

    private void backtrack(int first, LinkedList<Integer> current) {
        if (current.size() == k) {
            result.add(new LinkedList<>(current));
            return;
        }

        for (int i = first; i <= n; i++) {
            current.add(i);
            backtrack(i + 1, current);
            current.removeLast();
        }
    }
}
