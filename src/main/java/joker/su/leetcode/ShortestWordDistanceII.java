package joker.su.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
Design a data structure that will be initialized with a string array, and then it should answer queries of the shortest distance between
two different strings from the array.

Implement the WordDistance class:
WordDistance(String[] wordsDict) initializes the object with the strings array wordsDict.
int shortest(String word1, String word2) returns the shortest distance between word1 and word2 in the array wordsDict.


Example 1:
Input
["WordDistance", "shortest", "shortest"]
[[["practice", "makes", "perfect", "coding", "makes"]], ["coding", "practice"], ["makes", "coding"]]
Output
[null, 3, 1]
Explanation
WordDistance wordDistance = new WordDistance(["practice", "makes", "perfect", "coding", "makes"]);
wordDistance.shortest("coding", "practice"); // return 3
wordDistance.shortest("makes", "coding");    // return 1


Constraints:
1 <= wordsDict.length <= 3 * 104
1 <= wordsDict[i].length <= 10
wordsDict[i] consists of lowercase English letters.
word1 and word2 are in wordsDict.
word1 != word2
At most 5000 calls will be made to shortest.
 */
public class ShortestWordDistanceII {

    private final Map<String, List<Integer>> dictionary = new HashMap<>();

    public ShortestWordDistanceII(String[] wordsDict) {
        for(int i = 0; i < wordsDict.length; i++) {
            dictionary.computeIfAbsent(wordsDict[i], key -> new ArrayList<>()).add(i);
        }
    }

    public int shortest(String word1, String word2) {
        List<Integer> word1Positions = dictionary.get(word1);
        List<Integer> word2Positions = dictionary.get(word2);

        if(word1Positions == null || word2Positions == null){
            return Integer.MAX_VALUE;
        } else {
            return minDifferance(word1Positions, word2Positions);
        }
    }

    private int minDifferance(List<Integer> first, List<Integer> second) {
        int shortestPath = Integer.MAX_VALUE;
        int firstIndex = 0;
        int secondIndex = 0;

        while (firstIndex < first.size() && secondIndex < second.size())
        {
            if(first.get(firstIndex) > second.get(secondIndex)) {
                shortestPath = Math.min(shortestPath, first.get(firstIndex) - second.get(secondIndex));
                secondIndex++;
            } else {
                shortestPath = Math.min(shortestPath, second.get(secondIndex) - first.get(firstIndex));
                firstIndex++;
            }
        }
        return shortestPath;
    }
}
