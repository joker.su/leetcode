package joker.su.leetcode;
/*
Given two sorted arrays nums1 and nums2 of size m and n respectively, return the median of the two sorted arrays.
The overall run time complexity should be O(log (m+n)).

Example 1:
Input: nums1 = [1,3], nums2 = [2]
Output: 2.00000
Explanation: merged array = [1,2,3] and median is 2.

Example 2:
Input: nums1 = [1,2], nums2 = [3,4]
Output: 2.50000
Explanation: merged array = [1,2,3,4] and median is (2 + 3) / 2 = 2.5.

Constraints:
nums1.length == m
nums2.length == n
0 <= m <= 1000
0 <= n <= 1000
1 <= m + n <= 2000
-106 <= nums1[i], nums2[i] <= 106
 */
public class MedianOfTwoSortedArrays {
    public double findMedianSortedArrays(int[] x, int[] y) {
        int xSize = x.length;
        int ySize = y.length;

        if(xSize > ySize) {
            return findMedianSortedArrays(y, x);
        }

        if(xSize == 0) {
            if((ySize % 2) == 0) {
                return (y[ySize / 2] + y[ySize / 2 - 1]) / 2.0;
            } else {
                return y[ySize / 2];
            }
        }

        int mergedHalf = (xSize + ySize + 1) / 2;
        int low = 0;
        int high = xSize;

        while (low <= high) {
            int partitionX = (low + high) / 2;
            int partitionY = mergedHalf - partitionX;

            int maxLeftX = partitionX == 0 ? Integer.MIN_VALUE : x[partitionX - 1];
            int minRightX = partitionX == xSize ? Integer.MAX_VALUE : x[partitionX];

            int maxLeftY = partitionY == 0 ? Integer.MIN_VALUE : y[partitionY - 1];
            int minRightY = partitionY == ySize ? Integer.MAX_VALUE : y[partitionY];

            if(maxLeftX <= minRightY && maxLeftY <= minRightX) {
                if((xSize + ySize) % 2 == 0) {
                    return (Math.max(maxLeftX, maxLeftY) + Math.min(minRightX, minRightY)) / 2.0;
                } else {
                    return Math.max(maxLeftX, maxLeftY);
                }
            } else if(maxLeftX > minRightY) {
                high = partitionX - 1;
            } else {
                low = partitionX + 1;
            }
        }

        throw new IllegalArgumentException("Looks like we have not sorted arrays");
    }
}
