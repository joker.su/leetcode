package joker.su.leetcode;

import joker.su.leetcode.api.TreeNode;

/*
The thief has found himself a new place for his thievery again. There is only one entrance to this area, called root.
Besides the root, each house has one and only one parent house.
After a tour, the smart thief realized that all houses in this place form a binary tree.
It will automatically contact the police if two directly-linked houses were broken into on the same night.
Given the root of the binary tree, return the maximum amount of money the thief can rob without alerting the police.

Example 1:
Input: root = [3,2,3,null,3,null,1]
Output: 7
Explanation: Maximum amount of money the thief can rob = 3 + 3 + 1 = 7.

Example 2:
Input: root = [3,4,5,1,3,null,1]
Output: 9
Explanation: Maximum amount of money the thief can rob = 4 + 5 = 9.

Constraints:
The number of nodes in the tree is in the range [1, 104].
0 <= Node.val <= 104
 */
public class HouseRobberIII {

    public int rob(TreeNode root) {
        int[] answer = robNode(root);
        return Math.max(answer[0], answer[1]);
    }

    private int[] robNode(TreeNode node) {
        if (node == null) {
            return new int[]{0, 0};
        }

        int[] leftChildResult = robNode(node.left);
        int[] rightChildResult = robNode(node.right);

        int robNodeWay = node.val + leftChildResult[1] + rightChildResult[1];
        int notRobNodeWay = Math.max(leftChildResult[0], leftChildResult[1])
            + Math.max(rightChildResult[0], rightChildResult[1]);

        return new int[]{robNodeWay, notRobNodeWay};
    }
}
