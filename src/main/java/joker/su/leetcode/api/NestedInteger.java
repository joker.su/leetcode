package joker.su.leetcode.api;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class NestedInteger {

    private Integer value;

    private List<NestedInteger> sub;

    public NestedInteger() {
        this.sub = new ArrayList<>();
    }

    public NestedInteger(int value) {
        this.value = value;
    }

    public boolean isInteger() {
        return value != null;
    }

    public Integer getInteger() {
        return value;
    }

    public void setInteger(int value) {
        this.value = value;
    }

    public void add(NestedInteger ni) {
        if(sub == null) {
            sub = new LinkedList<>();
        }
        sub.add(ni);
    }

    public List<NestedInteger> getList() {
        return sub;
    }


    public static NestedInteger of(String str) {
        if(str.isEmpty()) {
            return null;
        }
        Deque<NestedInteger> stack = new ArrayDeque<>();
        NestedInteger result = null;
        StringBuilder builder = new StringBuilder();
        for(char action: str.toCharArray()) {
            if(action == '[') {
                addNumber(stack, builder);
                NestedInteger newLevel = new NestedInteger();
                if(!stack.isEmpty())
                {
                    stack.peek().add(newLevel);
                }
                stack.push(newLevel);
            } else if(action == ']') {
                addNumber(stack, builder);
                result = stack.pop();
            } else if(action == ',') {
                addNumber(stack, builder);
            } else {
                builder.append(action);
            }
        }
        return result;
    }

    private static void addNumber(Deque<NestedInteger> stack, StringBuilder builder) {
        if(!builder.isEmpty()) {
            int value = Integer.parseInt(builder.toString());
            builder.setLength(0);
            stack.peek().add(new NestedInteger(value));
        }
    }
}
