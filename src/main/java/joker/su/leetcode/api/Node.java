package joker.su.leetcode.api;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import java.util.ArrayList;
import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Getter
@Setter
public class Node {

    @EqualsAndHashCode.Include
    public final int val;
    public Node parent;
    public Node left;
    public Node right;
    public Node next;

    public Node(int val) {
        this(val, null);
    }

    public Node(int val, Node parent) {
        this(val, parent, null, null);
    }

    public Node(int val, Node parent, Node left, Node right) {
        this.val = val;
        this.parent = parent;
        this.left = left;
        this.right = right;
    }

    public Node findNodeByValue(int value) {
        final Multimap<Integer, Node> map = ArrayListMultimap.create();
        map.put(1, this);
        unwind(map, this.left, this.right, 2);
        return map.values().stream().filter(x -> x.getVal() == value).findFirst().orElse(null);
    }

    public Integer[] toArray() {
        final List<Integer> result = new ArrayList<>();
        final Multimap<Integer, Node> map = ArrayListMultimap.create();
        map.put(1, this);
        unwind(map, this.left, this.right, 2);
        map.values().forEach(node -> {
            result.add(node.getVal());
            if (node.getNext() == null) {
                result.add(null);
            }
        });
        return result.toArray(Integer[]::new);
    }

    private static void unwind(Multimap<Integer, Node> map, Node left, Node right, int level) {
        if (left != null) {
            map.put(level, left);
            unwind(map, left.left, left.right, level + 1);
        }
        if (right != null) {
            map.put(level, right);
            unwind(map, right.left, right.right, level + 1);
        }
    }


    public static Node toTree(Integer[] values) {
        if (values.length == 0) {
            return null;
        }
        return arrayToTree(values, 0, null);
    }


    private static Node arrayToTree(Integer[] values, int index, Node parent) {
        if (index >= values.length || values[index] == null) {
            return null;
        }
        Node node = new Node(values[index], parent);
        node.setLeft(arrayToTree(values, index * 2 + 1, node));
        node.setRight(arrayToTree(values, index * 2 + 2, node));
        return node;
    }
}
