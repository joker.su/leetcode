package joker.su.leetcode.api;

public class Pair<T, U> {

    private final T key;
    private final U value;

    public Pair(T key, U value) {
        this.key = key;
        this.value = value;
    }

    public T getKey() {
        return this.key;
    }

    public U getValue() {
        return this.value;
    }

    public static <T, U> Pair<T, U> of(T key, U value) {
        return new Pair<>( key, value);
    }
}
