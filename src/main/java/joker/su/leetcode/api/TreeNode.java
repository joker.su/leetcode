package joker.su.leetcode.api;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class TreeNode {

    @EqualsAndHashCode.Include
    public int val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode() {
    }

    public TreeNode(int val) {
        this.val = val;
    }

    public TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }

    public static TreeNode toTree(Integer[] values) {
        return toTree(values, false);
    }

    public static TreeNode toTree(Integer[] values, boolean nullable) {
        if (values.length == 0) {
            return nullable ? null : new TreeNode();
        }
        return arrayToTree(values, 0);
    }


    private static TreeNode arrayToTree(Integer[] values, int index) {
        if (index >= values.length || values[index] == null) {
            return null;
        }
        return new TreeNode(values[index], arrayToTree(values, index * 2 + 1), arrayToTree(values, index * 2 + 2));
    }
}
