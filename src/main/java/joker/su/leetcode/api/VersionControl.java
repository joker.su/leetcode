package joker.su.leetcode.api;

import lombok.Setter;

@Setter
public class VersionControl {

    private int currentVersion = 1;
    private int badVersion = 1;

    public boolean isBadVersion(int version) {
        return version <= currentVersion && version >= badVersion;
    }

}
