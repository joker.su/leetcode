package joker.su.leetcode.api;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ListNode {

    @EqualsAndHashCode.Include
    public UUID id = UUID.randomUUID();

    public int val;

    public ListNode next;
    public ListNode random;

    public ListNode() {
    }

    public ListNode(int val) {
        this.val = val;
    }

    public ListNode(int val, ListNode next) {
        this.val = val; this.next = next;
    }

    public ListNode findNextWithValue(int value) {
        ListNode current = this;
        while (current != null) {
            if(current.val == value) {
                return current;
            }
            current = current.next;
        }
        return null;
    }

    public static ListNode toNodeList(int count) {
        ListNode fake = new ListNode(0);
        ListNode node = fake;
        while (count > 0) {
            ListNode nextNode = new ListNode(node.val + 1);
            node.next = nextNode;
            node = nextNode;
            count--;
        }
        return fake.next;
    }


    public static ListNode toNodeList(int[] values) {
        if (values == null || values.length == 0) {
            return null;
        }
        ListNode head = new ListNode(values[0]);
        ListNode previous = head;
        for (int index = 1; index < values.length; index++) {
            ListNode node = new ListNode(values[index]);
            previous.next = node;
            previous = node;
        }
        return head;
    }

    public static ListNode toNodeList(Integer[][] values) {
        if (values == null || values.length == 0) {
            return null;
        }
        ListNode head = new ListNode(values[0][0]);
        ListNode previous = head;
        List<ListNode> nodeList = new LinkedList<>();
        nodeList.add(head);
        for (int index = 1; index < values.length; index++) {
            ListNode node = new ListNode(values[index][0]);
            previous.next = node;
            previous = node;
            nodeList.add(node);
        }

        previous = head;
        for (Integer[] value : values) {
            if (value[1] != null) {
                previous.random = nodeList.get(value[1]);
            }
            previous = previous.next;
        }
        return head;
    }

    public static Integer[][] toArrayValueWithRandom(ListNode node) {
        if (node == null) {
            return new Integer[][]{};
        }
        Map<ListNode, Integer> mapNodes = new LinkedHashMap<>();
        int counter = 0;
        while (node != null) {
            mapNodes.put(node, counter);
            node = node.next;
            counter++;
        }
        return mapNodes.keySet().stream().map(x -> new Integer[]{x.val, x.random == null ? null : mapNodes.get(x.random)})
            .toArray(Integer[][]::new);
    }

    public static int[] toArrayValue(ListNode node) {
        if (node == null) {
            return new int[]{};
        }
        List<ListNode> listNodes = new LinkedList<>();
        while (node != null) {
            listNodes.add(node);
            node = node.next;
        }
        return listNodes.stream().mapToInt(x -> x.val).toArray();
    }
}
