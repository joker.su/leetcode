package joker.su.leetcode;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Map;
import java.util.function.BiFunction;

public class EvaluateReversePolishNotation {

    Map<String, BiFunction<Integer,Integer,Integer>> operations = Map.of(
        "+", (a,b) -> a + b,
        "-", (a,b) -> a - b,
        "*", (a,b) -> a * b,
        "/", (a,b) -> a / b
    );

    public int evalRPN(String[] tokens) {
        Deque<Integer> stack = new ArrayDeque<>();
        for(String token : tokens) {
            if(operations.containsKey(token)) {
                int second = stack.pop();
                int first = stack.pop();
                BiFunction<Integer,Integer,Integer> operation = operations.get(token);
                stack.push(operation.apply(first, second));
            } else {
                stack.push(Integer.parseInt(token));
            }
        }
        return stack.pop();
    }
}
