package joker.su.leetcode;

import java.util.ArrayDeque;
import java.util.Queue;

/*
You are given an n x n binary matrix grid where 1 represents land and 0 represents water.
An island is a 4-directionally connected group of 1's not connected to any other 1's. There are exactly two islands in grid.
You may change 0's to 1's to connect the two islands to form one island.
Return the smallest number of 0's you must flip to connect the two islands.

Example 1:
Input: grid = [[0,1],[1,0]]
Output: 1

Example 2:
Input: grid = [[0,1,0],[0,0,0],[0,0,1]]
Output: 2

Example 3:
Input: grid = [[1,1,1,1,1],[1,0,0,0,1],[1,0,1,0,1],[1,0,0,0,1],[1,1,1,1,1]]
Output: 1

Constraints:
n == grid.length == grid[i].length
2 <= n <= 100
grid[i][j] is either 0 or 1.
There are exactly two islands in grid.
 */
public class ShortestBridge {

    private final int[][] directions = new int[][]{{1, 0}, {-1, 0}, {0, 1}, {0, -1}};

    public int shortestBridge(int[][] grid) {
        int n = grid.length;
        Queue<int[]> queue = new ArrayDeque<>();
        boolean[][] visited = new boolean[n][n];

        for (int row = 0; row < n; row++) {
            if (!queue.isEmpty()) {
                break;
            }
            for (int column = 0; column < n; column++) {
                if (grid[row][column] == 1) {
                    searchIsland(grid, row, column, visited, queue);
                    break;
                }
            }
        }
        int steps = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                int[] position = queue.poll();
                for (int[] dir: directions) {
                    int row = position[0] + dir[0];
                    int column = position[1] + dir[1];
                    if (row < 0 || row >= grid.length || column < 0 || column >= grid.length || visited[row][column]) {
                        continue;
                    }
                    if(grid[row][column] == 1) {
                        return steps;
                    }
                    queue.add(new int[]{row, column});
                    visited[row][column] = true;
                }
            }
            steps++;
        }

        return -1;
    }

    private void searchIsland(int[][] grid, int row, int column, boolean[][] visited, Queue<int[]> queue) {
        if (row < 0 || row >= grid.length || column < 0 || column >= grid.length || visited[row][column] || grid[row][column] == 0) {
            return;
        }
        queue.add(new int[]{row, column});
        visited[row][column] = true;

        for (int[] dir: directions){
            searchIsland(grid, row + dir[0], column + dir[1], visited, queue);
        }
    }
}
