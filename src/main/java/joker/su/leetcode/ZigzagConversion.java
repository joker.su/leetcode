package joker.su.leetcode;

/*
The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this: (you may want to display this pattern in a fixed font for better legibility)
P   A   H   N
A P L S I I G
Y   I   R
And then read line by line: "PAHNAPLSIIGYIR"
Write the code that will take a string and make this conversion given a number of rows.

Example 1:
Input: s = "PAYPALISHIRING", numRows = 3
Output: "PAHNAPLSIIGYIR"

Example 2:
Input: s = "PAYPALISHIRING", numRows = 4
Output: "PINALSIGYAHRPI"
Explanation:
P     I    N
A   L S  I G
Y A   H R
P     I

Example 3:
Input: s = "A", numRows = 1
Output: "A"

Constraints:
1 <= s.length <= 1000
s consists of English letters (lower-case and upper-case), ',' and '.'.
1 <= numRows <= 1000
 */
public class ZigzagConversion {

    public String convert(String s, int numRows) {
        if (numRows == 1) {
            return s;
        }
        int size = s.length();
        int sections = (int) Math.ceil(size / (2.0 * numRows - 2));
        int numColumns = sections * (numRows - 1);
        Character[][] matrix = new Character[numRows][numColumns];

        int row = 0;
        int column = 0;
        int i = 0;
        while (i < size) {
            if (row < numRows) {
                matrix[row][column] = s.charAt(i);
                row++;
                i++;
                continue;
            }
            row -= 2;
            column++;
            while (row > 0 && i < size) {
                matrix[row][column] = s.charAt(i);
                row--;
                column++;
                i++;
            }
        }
        StringBuilder builder = new StringBuilder();
        for (row = 0; row < numRows; row++) {
            for (column = 0; column < numColumns; column++) {
                if (matrix[row][column] != null) {
                    builder.append(matrix[row][column]);
                }
            }
        }
        return builder.toString();
    }
}
