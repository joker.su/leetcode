package joker.su.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FindAllAnagramsInString {

    public List<Integer> findAnagrams(String s, String p) {
        List<Integer> answer = new ArrayList<>();
        int pl = p.length();
        int sl = s.length();
        if (sl < pl) {
            return answer;
        }

        Map<Character, Integer> symbols = new HashMap<>();
        Map<Character, Integer> currentCheck = new HashMap<>();

        for (char symbol : p.toCharArray()) {
            symbols.put(symbol, symbols.getOrDefault(symbol, 0) + 1);
        }

        for (int i = 0; i < sl; i++) {
            char symbol = s.charAt(i);
            if (currentCheck.containsKey(symbol)) {
                currentCheck.put(symbol, currentCheck.get(symbol) + 1);
            } else {
                currentCheck.put(symbol, 1);
            }

            if (i >= pl) {
                char removedChar = s.charAt(i - pl);
                if (currentCheck.get(removedChar) == 1) {
                    currentCheck.remove(removedChar);
                } else {
                    currentCheck.put(removedChar, currentCheck.get(removedChar) - 1);
                }
            }

            if (currentCheck.equals(symbols)) {
                answer.add(i - pl + 1);
            }
        }
        return answer;
    }
}
