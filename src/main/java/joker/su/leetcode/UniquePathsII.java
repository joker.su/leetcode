package joker.su.leetcode;
/*
You are given an m x n integer array grid. There is a robot initially located at the top-left corner (i.e., grid[0][0]).
The robot tries to move to the bottom-right corner (i.e., grid[m-1][n-1]). The robot can only move either down or right at any point in time.
An obstacle and space are marked as 1 or 0 respectively in grid. A path that the robot takes cannot include any square that is an obstacle.
Return the number of possible unique paths that the robot can take to reach the bottom-right corner.
The testcases are generated so that the answer will be less than or equal to 2 * 109.

Example 1:
Input: obstacleGrid = [[0,0,0],[0,1,0],[0,0,0]]
Output: 2
Explanation: There is one obstacle in the middle of the 3x3 grid above.
There are two ways to reach the bottom-right corner:
1. Right -> Right -> Down -> Down
2. Down -> Down -> Right -> Right

Example 2:
Input: obstacleGrid = [[0,1],[0,0]]
Output: 1


Constraints:
m == obstacleGrid.length
n == obstacleGrid[i].length
1 <= m, n <= 100
obstacleGrid[i][j] is 0 or 1.
 */

public class UniquePathsII {

    public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        if (obstacleGrid[0][0] == 1) {
            return 0;
        }
        int rowCount = obstacleGrid.length;
        int columnCount = obstacleGrid[0].length;
        int[][] paths = new int[rowCount][columnCount];
        paths[0][0] = 1;
        for (int i = 1; i < rowCount; i++) {
            if (obstacleGrid[i][0] == 1 || paths[i - 1][0] == 0) {
                paths[i][0] = 0;
            } else {
                paths[i][0] = 1;
            }
        }
        for (int i = 1; i < columnCount; i++) {
            if (obstacleGrid[0][i] == 1 || paths[0][i - 1] == 0) {
                paths[0][i] = 0;
            } else {
                paths[0][i] = 1;
            }
        }

        for (int row = 1; row < rowCount; row++) {
            for (int column = 1; column < columnCount; column++) {
                if (obstacleGrid[row][column] == 1) {
                    paths[row][column] = 0;
                } else {
                    paths[row][column] = paths[row - 1][column] + paths[row][column - 1];
                }
            }
        }
        return paths[rowCount - 1][columnCount - 1];
    }

}
