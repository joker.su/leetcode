package joker.su.leetcode;

/*
Write a function to find the longest common prefix string amongst an array of strings.
If there is no common prefix, return an empty string "".

Example 1:
Input: strs = ["flower","flow","flight"]
Output: "fl"

Example 2:
Input: strs = ["dog","racecar","car"]
Output: ""
Explanation: There is no common prefix among the input strings.

Constraints:
1 <= strs.length <= 200
0 <= strs[i].length <= 200
strs[i] consists of only lowercase English letters.
 */
public class LongestCommonPrefix {

    public String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0) {
            return "";
        }
        String firstWord = strs[0];
        for (int i = 0; i < firstWord.length(); i++) {
            for (int j = 1; j < strs.length; j++) {
                if (strs[j].length() <= i || firstWord.charAt(i) != strs[j].charAt(i)) {
                    return firstWord.substring(0, i);
                }
            }
        }
        return firstWord;
    }
}
