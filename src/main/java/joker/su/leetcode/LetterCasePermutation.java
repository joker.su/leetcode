package joker.su.leetcode;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;


public class LetterCasePermutation {

    public List<String> letterCasePermutation(String s) {
        List<StringBuilder> result = new LinkedList<>();
        result.add(new StringBuilder());
        for (char c : s.toCharArray()) {
            if (Character.isDigit(c)) {
                result.forEach(x -> x.append(c));
            } else {
                List<StringBuilder> copied = result.stream().map(x -> new StringBuilder(x.toString())).toList();
                result.forEach(x -> x.append(Character.toLowerCase(c)));
                copied.forEach(x -> result.add(x.append(Character.toUpperCase(c))));
            }
        }
        return result.stream().map(StringBuilder::toString).distinct().collect(Collectors.toList());
    }
}
