package joker.su.leetcode;

import java.util.Arrays;
import java.util.List;

/*
You are given an integer n. We reorder the digits in any order (including the original order) such that the leading digit is not zero.
Return true if and only if we can do this so that the resulting number is a power of two.

Example 1:
Input: n = 1
Output: true

Example 2:
Input: n = 10
Output: false

Constraints:
1 <= n <= 109
*/
public class ReorderedPowerOf2 {

    private final int[][] cache;

    public ReorderedPowerOf2() {
        cache = new int[32][];
        for(int i = 0; i < 32; i++) {
            cache[i] = count(1 << i);
        }
    }

    public boolean reorderedPowerOf2(int n) {
        int[] numberOf = count(n);
        for (int[] numberOfTwo : cache) {
            if(Arrays.equals(numberOfTwo, numberOf)){
                return true;
            }
        }
        return false;
    }

    private int[] count(int number) {
        int[] result = new int[10];
        while (number > 0) {
            result[number % 10]++;
            number = number / 10;
        }
        return result;
    }
}
