package joker.su.leetcode;

/*
There is an integer array nums sorted in ascending order (with distinct values).
Prior to being passed to your function, nums is possibly rotated at an unknown pivot index k (1 <= k < nums.length) such
that the resulting array is [nums[k], nums[k+1], ..., nums[n-1], nums[0], nums[1], ..., nums[k-1]] (0-indexed). For example, [0,1,2,4,5,6,7] might be rotated at pivot index 3 and become [4,5,6,7,0,1,2].
Given the array nums after the possible rotation and an integer target, return the index of target if it is in nums, or -1 if it is not in nums.
You must write an algorithm with O(log n) runtime complexity.

Example 1:
Input: nums = [4,5,6,7,0,1,2], target = 0
Output: 4

Example 2:
Input: nums = [4,5,6,7,0,1,2], target = 3
Output: -1

Example 3:
Input: nums = [1], target = 0
Output: -1

Constraints:
1 <= nums.length <= 5000
-104 <= nums[i] <= 104
All values of nums are unique.
nums is an ascending array that is possibly rotated.
-104 <= target <= 104
*/
public class SearchRotatedSortedArray {

    public int search(int[] nums, int target) {
        if (nums.length == 0) {
            return -1;
        }
        int gapId = foundGap(nums);
        if (nums[nums.length - 1] >= target) {
            return binarySearch(nums, gapId + 1, nums.length - 1, target);
        } else {
            return binarySearch(nums, 0, gapId, target);
        }
    }

    private int foundGap(int[] nums) {
        if (nums.length == 1 || nums[nums.length - 1] > nums[0]) {
            return -1;
        }
        int lastId = nums.length - 1;
        while (nums[lastId] < nums[0]) {
            lastId--;
        }
        return lastId;
    }

    private int binarySearch(int[] nums, int from, int to, int target) {
        int half = (to - from) / 2;
        int counter = half + 1;
        int index = from + half;

        while (index >= from && index <= to && counter >= 0) {
            if (nums[index] == target) {
                return index;
            }
            half = half > 2 ? half / 2 : 1;
            if (nums[index] > target) {
                index = index - half;
            } else {
                index = index + half;
            }
            counter--;
        }
        return -1;
    }
}
