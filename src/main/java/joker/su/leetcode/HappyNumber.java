package joker.su.leetcode;

import java.util.HashSet;

/*
Write an algorithm to determine if a number n is happy.
A happy number is a number defined by the following process:

Starting with any positive integer, replace the number by the sum of the squares of its digits.
Repeat the process until the number equals 1 (where it will stay), or it loops endlessly in a cycle which does not include 1.
Those numbers for which this process ends in 1 are happy.
Return true if n is a happy number, and false if not.

Example 1:
Input: n = 19
Output: true
Explanation:
12 + 92 = 82
82 + 22 = 68
62 + 82 = 100
12 + 02 + 02 = 1

Example 2:
Input: n = 2
Output: false

Constraints:
1 <= n <= 231 - 1
 */
public class HappyNumber {
    public boolean isHappy(int n) {
        HashSet<Integer> checked = new HashSet<>();
        int number = n;
        while (number != 1) {
            if(checked.contains(number)) {
                return false;
            }
            checked.add(number);
            number = getNextNumber(number);
        }
        return true;
    }

    private int getNextNumber(int number) {
        int nextNumber = 0;
        while (number / 10 > 0) {
            int digit = number % 10;
            number = number / 10;
            nextNumber += digit * digit;
        }
        nextNumber += number * number;
        return nextNumber;
    }
}
