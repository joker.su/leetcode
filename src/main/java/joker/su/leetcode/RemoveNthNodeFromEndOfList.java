package joker.su.leetcode;

import java.util.HashMap;

import joker.su.leetcode.api.ListNode;

/*
Given the head of a linked list, remove the nth node from the end of the list and return its head.

Example 1:
Input: head = [1,2,3,4,5], n = 2
Output: [1,2,3,5]

Example 2:
Input: head = [1], n = 1
Output: []

Example 3:
Input: head = [1,2], n = 1
Output: [1]


Constraints:
The number of nodes in the list is sz.
1 <= sz <= 30
0 <= Node.val <= 100
1 <= n <= sz
*/
public class RemoveNthNodeFromEndOfList {

    public ListNode removeNthFromEnd(ListNode head, int n) {
        HashMap<Integer, ListNode> cache = new HashMap<>();
        int count = 0;
        while(head != null){
            cache.put(count, head);
            head = head.next;
            count++;
        }
        if(count > n) {
            ListNode beforeNode = cache.get(count - n - 1);
            beforeNode.next = cache.get(count - n + 1);
            return cache.get(0);
        } else if(count == n) {
            return cache.get(1);
        }
        return null;
    }
}
