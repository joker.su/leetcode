package joker.su.leetcode;

/*
Convert a non-negative integer num to its English words representation.

Example 1:
Input: num = 123
Output: "One Hundred Twenty Three"

Example 2:
Input: num = 12345
Output: "Twelve Thousand Three Hundred Forty Five"

Example 3:
Input: num = 1234567
Output: "One Million Two Hundred Thirty Four Thousand Five Hundred Sixty Seven"

Constraints:
0 <= num <= 231 - 1
 */
public class IntegerToEnglishWords {

    public String numberToWords(int num) {
        if(num == 0) {
            return "Zero";
        }
        int currentNumber = num;
        StringBuilder builder = new StringBuilder();

        int billions = currentNumber / 1_000_000_000;
        if (billions != 0) {
            currentNumber -= billions * 1_000_000_000;
            printLessThousand(billions, builder);
            removeLastSpaceIfNeed(builder);
            builder.append(" Billion").append(" ");
        }

        int millions = currentNumber / 1_000_000;
        if (millions != 0) {
            currentNumber -= millions * 1_000_000;
            printLessThousand(millions, builder);
            removeLastSpaceIfNeed(builder);
            builder.append(" Million").append(" ");
        }

        int thousands = currentNumber / 1_000;
        if (thousands != 0) {
            currentNumber -= thousands * 1_000;
            printLessThousand(thousands, builder);
            removeLastSpaceIfNeed(builder);
            builder.append(" Thousand").append(" ");
        }

        if (currentNumber != 0) {
            printLessThousand(currentNumber, builder);
        }
        removeLastSpaceIfNeed(builder);
        return builder.toString();
    }

    private void removeLastSpaceIfNeed(StringBuilder builder) {
        if (!builder.isEmpty() && builder.charAt(builder.length() - 1) == ' ') {
            builder.setLength(builder.length() - 1);
        }
    }

    private void printLessThousand(int num, StringBuilder builder) {
        int currentNumber = num;
        int hundred = currentNumber / 100;
        if (hundred != 0) {
            currentNumber -= hundred * 100;
            printLessHundred(hundred, builder);
            builder.append(" Hundred").append(" ");
        }
        if (currentNumber != 0) {
            printLessHundred(currentNumber, builder);
        }
    }

    private void printLessHundred(int num, StringBuilder builder) {
        if (num > 0 && num < 10) {
            builder.append(ones(num));
        } else if (num > 9 && num < 20) {
            builder.append(teens(num));
        } else if (num < 100) {
            int ones = num % 10;
            if (ones == 0) {
                builder.append(tens(num / 10));
            } else {
                builder.append(tens(num / 10)).append(" ").append(ones(ones));
            }
        }
    }

    private String ones(int num) {
        return switch (num) {
            case 1 -> "One";
            case 2 -> "Two";
            case 3 -> "Three";
            case 4 -> "Four";
            case 5 -> "Five";
            case 6 -> "Six";
            case 7 -> "Seven";
            case 8 -> "Eight";
            case 9 -> "Nine";
            default -> "";
        };
    }

    public String teens(int num) {
        return switch (num) {
            case 10 -> "Ten";
            case 11 -> "Eleven";
            case 12 -> "Twelve";
            case 13 -> "Thirteen";
            case 14 -> "Fourteen";
            case 15 -> "Fifteen";
            case 16 -> "Sixteen";
            case 17 -> "Seventeen";
            case 18 -> "Eighteen";
            case 19 -> "Nineteen";
            default -> "";
        };
    }

    public String tens(int num) {
        return switch (num) {
            case 2 -> "Twenty";
            case 3 -> "Thirty";
            case 4 -> "Forty";
            case 5 -> "Fifty";
            case 6 -> "Sixty";
            case 7 -> "Seventy";
            case 8 -> "Eighty";
            case 9 -> "Ninety";
            default -> "";
        };
    }
}
