package joker.su.leetcode;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Deque;

/*
Given an array of intervals where intervals[i] = [starti, endi], merge all overlapping intervals,
and return an array of the non-overlapping intervals that cover all the intervals in the input.

Example 1:
Input: intervals = [[1,3],[2,6],[8,10],[15,18]]
Output: [[1,6],[8,10],[15,18]]
Explanation: Since intervals [1,3] and [2,6] overlap, merge them into [1,6].

Example 2:
Input: intervals = [[1,4],[4,5]]
Output: [[1,5]]
Explanation: Intervals [1,4] and [4,5] are considered overlapping.


Constraints:
1 <= intervals.length <= 104
intervals[i].length == 2
0 <= starti <= endi <= 104
 */
public class MergeIntervals {

    public int[][] merge(int[][] intervals) {
        Arrays.sort(intervals, Comparator.comparingInt(x -> x[0]));
        Deque<int[]> result = new ArrayDeque<>();
        result.push(intervals[0]);
        for (int i = 1; i < intervals.length; i++) {
            int[] current = result.pop();
            int[] next = intervals[i];
            if (current[1] >= next[0]) {
                if (current[1] < next[1]) {
                    result.push(new int[]{current[0], next[1]});
                } else {
                    result.push(new int[]{current[0], current[1]});
                }
            } else {
                result.push(current);
                result.push(next);
            }
        }
        return result.toArray(new int[][]{});
    }
}
