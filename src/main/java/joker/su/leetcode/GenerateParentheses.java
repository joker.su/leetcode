package joker.su.leetcode;

import java.util.LinkedList;
import java.util.List;

/*
Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.

Example 1:
Input: n = 3
Output: ["((()))","(()())","(())()","()(())","()()()"]

Example 2:
Input: n = 1
Output: ["()"]

Constraints:
1 <= n <= 8
 */
public class GenerateParentheses {

    public List<String> generateParenthesis(int n) {
        List<String> answer = new LinkedList<>();
        backtrace(answer, n, 0, 0, new StringBuilder());
        return answer;
    }

    public void backtrace(List<String> answer, int n, int openCount, int closeCount, StringBuilder builder) {
        if (builder.length() == n * 2) {
            answer.add(builder.toString());
            return;
        }

        if (openCount < n) {
            builder.append("(");
            backtrace(answer, n, openCount + 1, closeCount, builder);
            builder.setLength(builder.length() - 1);
        }

        if (closeCount < openCount) {
            builder.append(")");
            backtrace(answer, n, openCount, closeCount + 1, builder);
            builder.setLength(builder.length() - 1);
        }
    }
}
