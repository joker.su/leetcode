package joker.su.leetcode;

/*
Given a sorted array of distinct integers and a target value, return the index if the target is found.
If not, return the index where it would be if it were inserted in order.

You must write an algorithm with O(log n) runtime complexity.



Example 1:

Input: nums = [1,3,5,6], target = 5
Output: 2
Example 2:

Input: nums = [1,3,5,6], target = 2
Output: 1
Example 3:

Input: nums = [1,3,5,6], target = 7
Output: 4


Constraints:

1 <= nums.length <= 104
-104 <= nums[i] <= 104
nums contains distinct values sorted in ascending order.
-104 <= target <= 104
 */
public class SearchInsertPosition {

    public int searchInsert(int[] nums, int target) {
        int lessIndex = nums[0] < target ? 0 : -1;
        int greatIndex = nums[nums.length - 1] >= target ? nums.length - 1 : nums.length;
        if (lessIndex == -1) {
            return 0;
        }
        if (greatIndex == nums.length) {
            return greatIndex;
        }
        int toCheck = lessIndex + (greatIndex - lessIndex) / 2;
        while (greatIndex - lessIndex != 1) {
            if (nums[toCheck] == target) {
                return toCheck;
            } else if (nums[toCheck] > target) {
                greatIndex = toCheck;
            } else {
                lessIndex = toCheck;
            }
            toCheck = lessIndex + (greatIndex - lessIndex) / 2;
        }
        return nums[lessIndex] > target ? lessIndex : greatIndex;
    }
}
