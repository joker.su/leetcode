package joker.su.leetcode;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/*
Given an array nums of distinct integers, return all the possible permutations. You can return the answer in any order.

Example 1:
Input: nums = [1,2,3]
Output: [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]

Example 2:
Input: nums = [0,1]
Output: [[0,1],[1,0]]

Example 3:
Input: nums = [1]
Output: [[1]]

Constraints:
1 <= nums.length <= 6
-10 <= nums[i] <= 10
All the integers of nums are unique.
 */
public class Permutations {

    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> result = new LinkedList<>();
        backtrack(0, Arrays.stream(nums).boxed().collect(Collectors.toList()), nums.length, result);
        return result;
    }


    private void backtrack(int first, List<Integer> numbers, int size, List<List<Integer>> result) {
        if (first == size) {
            result.add(new LinkedList<>(numbers));
            return;
        }
        for (int i = first; i < size; i++) {
            Collections.swap(numbers, first, i);
            backtrack(first + 1, numbers, size, result);
            Collections.swap(numbers, i, first);
        }
    }
}
