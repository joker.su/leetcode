package joker.su.leetcode;

import java.util.HashSet;
import java.util.Set;

/*
Given n points on a 2D plane, find if there is such a line parallel to the y-axis that reflects the given points symmetrically.
In other words, answer whether or not if there exists a line that after reflecting all points over the given line, the original points' set is the same as the reflected ones.
Note that there can be repeated points.

Example 1:
Input: points = [[1,1],[-1,1]]
Output: true
Explanation: We can choose the line x = 0.

Example 2:
Input: points = [[1,1],[-1,-1]]
Output: false
Explanation: We can't choose a line.


Constraints:
n == points.length
1 <= n <= 104
-108 <= points[i][j] <= 108
 */
public class LineReflection {

    public boolean isReflected(int[][] points) {
        int minX = Integer.MAX_VALUE;
        int maxX = Integer.MIN_VALUE;

        Set<String> set = new HashSet<>();
        for(int[] point : points) {
            minX = Math.min(minX, point[0]);
            maxX = Math.max(maxX, point[0]);
            set.add(point[0] + "x" + point[1]);
        }

        int sum = minX + maxX;
        for(int[] point : points) {
            int reflectedX = sum - point[0];
            if(!set.contains(reflectedX + "x" + point[1])) {
                return false;
            }
        }
        return true;
    }
}
