package joker.su.leetcode;

import java.util.ArrayDeque;
import java.util.Deque;

/*
Given an array of integers arr, find the sum of min(b), where b ranges over every (contiguous) subarray of arr.
Since the answer may be large, return the answer modulo 109 + 7.

Example 1:
Input: arr = [3,1,2,4]
Output: 17
Explanation:
Subarrays are [3], [1], [2], [4], [3,1], [1,2], [2,4], [3,1,2], [1,2,4], [3,1,2,4].
Minimums are 3, 1, 2, 4, 1, 1, 2, 1, 1, 1.
Sum is 17.

Example 2:
Input: arr = [11,81,94,43,3]
Output: 444

Constraints:
1 <= arr.length <= 3 * 104
1 <= arr[i] <= 3 * 104
*/
public class SumOfSubarrayMinimums {

    public int sumSubarrayMins(int[] arr) {
        long answer = 0;
        int mod = 1_000_000_007;
        int size = arr.length;

        Deque<Integer> stack = new ArrayDeque<>();
        stack.push(-1);
        for (int i = 0; i <= size; i++) {
            while (stack.peek() != -1 && (i == size || arr[stack.peek()] >= arr[i])) {
                int currentPosition = stack.pop();
                long count = (((long) currentPosition - stack.peek()) * (i - currentPosition)) % mod;
                answer = (answer + count * arr[currentPosition] % mod) % mod;
            }
            stack.push(i);
        }
        return (int) (answer + mod) % mod;
    }
}
