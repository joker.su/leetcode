package joker.su.leetcode;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/*
Design a data structure that simulates an in-memory file system.

Implement the FileSystem class:
FileSystem() Initializes the object of the system.
List<String> ls(String path)
If path is a file path, returns a list that only contains this file's name.
If path is a directory path, returns the list of file and directory names in this directory.
The answer should in lexicographic order.
void mkdir(String path) Makes a new directory according to the given path. The given directory path does not exist.
 If the middle directories in the path do not exist, you should create them as well.
void addContentToFile(String filePath, String content)
If filePath does not exist, creates that file containing given content.
If filePath already exists, appends the given content to original content.
String readContentFromFile(String filePath) Returns the content in the file at filePath.


Example 1:
Input
["FileSystem", "ls", "mkdir", "addContentToFile", "ls", "readContentFromFile"]
[[], ["/"], ["/a/b/c"], ["/a/b/c/d", "hello"], ["/"], ["/a/b/c/d"]]
Output
[null, [], null, null, ["a"], "hello"]

Explanation
FileSystem fileSystem = new FileSystem();
fileSystem.ls("/");                         // return []
fileSystem.mkdir("/a/b/c");
fileSystem.addContentToFile("/a/b/c/d", "hello");
fileSystem.ls("/");                         // return ["a"]
fileSystem.readContentFromFile("/a/b/c/d"); // return "hello"


Constraints:
1 <= path.length, filePath.length <= 100
path and filePath are absolute paths which begin with '/' and do not end with '/' except that the path is just "/".
You can assume that all directory names and file names only contain lowercase letters, and the same names will not exist in the same directory.
You can assume that all operations will be passed valid parameters, and users will not attempt to retrieve file content or list a directory or file that does not exist.
1 <= content.length <= 50
At most 300 calls will be made to ls, mkdir, addContentToFile, and readContentFromFile.
 */
public class DesignInMemoryFileSystem {

    private static class Directory {

        public final Map<String, Directory> subDirectories = new HashMap<>();
        public final Map<String, String> files = new HashMap<>();
    }

    private final Directory root;

    public DesignInMemoryFileSystem() {
        root = new Directory();
    }

    public List<String> ls(String path) {
        if (path == null) {
            return Collections.emptyList();
        }
        String[] pathElements = path.split("/");
        Directory currentDirectory = root;
        if (!path.equals("/")) {
            for (int i = 1; i < pathElements.length - 1; i++) {
                if (!currentDirectory.subDirectories.containsKey(pathElements[i])) {
                    return Collections.emptyList();
                }
                currentDirectory = currentDirectory.subDirectories.get(pathElements[i]);
            }
            String fileName = pathElements[pathElements.length - 1];
            if(currentDirectory.files.containsKey(fileName)){
                return List.of(fileName);
            } else {
                currentDirectory = currentDirectory.subDirectories.get(fileName);
            }
        }
        List<String> result = new LinkedList<>();
        result.addAll(currentDirectory.subDirectories.keySet());
        result.addAll(currentDirectory.files.keySet());

        Collections.sort(result);
        return result;
    }

    private Directory getOrCreateDir(String[] pathElements, boolean ignoreLast) {
        Directory currentDirectory = root;
        int lastElement = ignoreLast ? pathElements.length - 1 : pathElements.length;
        for (int i = 1; i < lastElement; i++) {
            if (!currentDirectory.subDirectories.containsKey(pathElements[i])) {
                currentDirectory.subDirectories.put(pathElements[i], new Directory());
            }
            currentDirectory = currentDirectory.subDirectories.get(pathElements[i]);
        }
        return currentDirectory;
    }

    public void mkdir(String filePath) {
        if (filePath == null) {
            return;
        }
        String[] pathElements = filePath.split("/");
        getOrCreateDir(pathElements, false);
    }

    public void addContentToFile(String filePath, String content) {
        if (filePath == null) {
            return;
        }
        String[] pathElements = filePath.split("/");
        Directory directory = getOrCreateDir(pathElements, true);
        String fileName = pathElements[pathElements.length - 1];
        directory.files.put(fileName, directory.files.getOrDefault(fileName, "") + content);
    }

    public String readContentFromFile(String filePath) {
        if (filePath == null) {
            return null;
        }
        String[] pathElements = filePath.split("/");
        Directory currentDirectory = root;
        for (int i = 1; i < pathElements.length - 1; i++) {
            if (!currentDirectory.subDirectories.containsKey(pathElements[i])) {
                return null;
            }
            currentDirectory = currentDirectory.subDirectories.get(pathElements[i]);
        }
        String fileName = pathElements[pathElements.length - 1];
        return currentDirectory.files.getOrDefault(fileName, null);
    }
}
