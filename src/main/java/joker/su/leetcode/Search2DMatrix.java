package joker.su.leetcode;

/*
Write an efficient algorithm that searches for a value target in an m x n integer matrix matrix. This matrix has the following properties:
Integers in each row are sorted from left to right.
The first integer of each row is greater than the last integer of the previous row.

Example 1:
Input: matrix = [[1,3,5,7],[10,11,16,20],[23,30,34,60]], target = 3
Output: true

Example 2:
Input: matrix = [[1,3,5,7],[10,11,16,20],[23,30,34,60]], target = 13
Output: false

Constraints:
m == matrix.length
n == matrix[i].length
1 <= m, n <= 100
-104 <= matrix[i][j], target <= 104
 */
public class Search2DMatrix {

    public boolean searchMatrix(int[][] matrix, int target) {
        int[] row = binarySearchColumn(matrix, target);
        if (row == null) {
            return false;
        }
        return binarySearchRow(row, target) != -1;
    }

    private int[] binarySearchColumn(int[][] matrix, int target) {
        int half = matrix.length / 2;
        int index = half;
        int counter = half;

        while (index >= 0 && index < matrix.length && counter >= 0) {
            int firstInRow = matrix[index][0];
            int lastInRow = matrix[index][matrix[index].length - 1];
            if (firstInRow <= target && lastInRow >= target) {
                return matrix[index];
            }
            half = half > 2 ? half / 2 : 1;
            if (firstInRow > target) {
                index = index - half;
            } else {
                index = index + half;
            }
            counter--;
        }
        return null;
    }

    private int binarySearchRow(int[] nums, int target) {
        int half = nums.length / 2;
        int index = half;
        int counter = half;

        while (index >= 0 && index < nums.length && counter >= 0) {
            if (nums[index] == target) {
                return index;
            }
            half = half > 2 ? half / 2 : 1;
            if (nums[index] > target) {
                index = index - half;
            } else {
                index = index + half;
            }
            counter--;
        }
        return -1;
    }
}
