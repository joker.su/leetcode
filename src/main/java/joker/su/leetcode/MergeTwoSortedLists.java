package joker.su.leetcode;

import joker.su.leetcode.api.ListNode;

/*
You are given the heads of two sorted linked lists list1 and list2.
Merge the two lists in a one sorted list. The list should be made by splicing together the nodes of the first two lists.
Return the head of the merged linked list.

Example 1:
Input: list1 = [1,2,4], list2 = [1,3,4]
Output: [1,1,2,3,4,4]

Example 2:
Input: list1 = [], list2 = []
Output: []

Example 3:
Input: list1 = [], list2 = [0]
Output: [0]

Constraints:
The number of nodes in both lists is in the range [0, 50].
-100 <= Node.val <= 100
Both list1 and list2 are sorted in non-decreasing order.
 */
public class MergeTwoSortedLists {

    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        if (list1 == null && list2 == null) {
            return null;
        }
        ListNode leftNode = list1;
        ListNode rightNode = list2;

        ListNode prehead = new ListNode();
        ListNode previous = prehead;
        while (leftNode != null || rightNode != null) {
            int leftValue = leftNode != null ? leftNode.val : Integer.MIN_VALUE;
            int rightValue = rightNode != null ? rightNode.val : Integer.MIN_VALUE;
            if (leftValue > rightValue && rightValue != Integer.MIN_VALUE) {
                ListNode nextNode = new ListNode(rightValue);
                previous.next = nextNode;
                previous = nextNode;
                rightNode = rightNode.next;
            } else if (leftValue < rightValue && leftValue != Integer.MIN_VALUE) {
                ListNode nextNode = new ListNode(leftValue);
                previous.next = nextNode;
                previous = nextNode;
                leftNode = leftNode.next;
            } else if (leftValue != Integer.MIN_VALUE || rightValue != Integer.MIN_VALUE) {
                if (leftValue != Integer.MIN_VALUE) {
                    ListNode nextNode = new ListNode(leftValue);
                    previous.next = nextNode;
                    previous = nextNode;
                    leftNode = leftNode.next;
                }
                if (rightValue != Integer.MIN_VALUE) {
                    ListNode nextNode = new ListNode(rightValue);
                    previous.next = nextNode;
                    previous = nextNode;
                    rightNode = rightNode.next;
                }
            }
        }

        return prehead.next;
    }
}
