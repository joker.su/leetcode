package joker.su.leetcode;

public class StringCompression {

    public int compress(char[] chars) {
        int write = 0;
        char currentChar = chars[0];
        int counter = 0;

        for(int i = 0; i <= chars.length; i++){
            char symbol = i == chars.length ? 0 : chars[i];
            if(symbol == currentChar) {
                counter++;
            } else {
                chars[write++] = currentChar;
                if(counter != 1) {
                    for(char number : String.valueOf(counter).toCharArray()) {
                        chars[write++] = number;
                    }
                }
                currentChar = symbol;
                counter = 1;
            }
        }
        return write;
    }
}
