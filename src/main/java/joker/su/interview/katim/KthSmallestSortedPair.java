package joker.su.interview.katim;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.stream.Collectors;

/*
Given an array of integers with size n, find the k-th lexicographically smallest pair.
Pair is defined as array[i], array[j] for all 0 <= i,j < n.
Pair (a,b) is lexicographically smaller than pair (c, d) if a < c or (a == c and b < d)

Example 1:
Input: [3, 2, 1] k = 7
Output: [3,1]
Explanation: We have next sorted pairs - (1,1), (1,2), (1,3), (2,1), (2,2), (2,3), (3,1), (3,2), (3,3). 7-th is (3,1)

Example 2:
Input: [2, 2, 1] k = 5
Output: [2,1]
Explanation: We have next sorted pairs - (1,1), (1,2), (1,2), (2,1), (2,1), (2,2), (2,2), (2,2), (2,2). 5-th is (2,1)
 */
public class KthSmallestSortedPair {

    public List<Integer> getkthSmallestTerm(List<Integer> arr, long k) {
        Set<Integer> set = new HashSet<>(arr);

        if (arr.size() == set.size()) {
            List<Integer> sorted = arr.stream().distinct().sorted().collect(Collectors.toList());
            int size = sorted.size();
            int first = (int) (k / size);
            int second = (int) (k % size);
            return List.of(sorted.get(second == 0 ? first - 1 : first), sorted.get(second == 0 ? size - 1 : second - 1));
        } else {
            // duplicates
            int size = arr.size();
            Comparator<List<Integer>> comparator = (x, y) -> x.get(0) != y.get(0) ? x.get(0).compareTo(y.get(0))
                                                                                  : x.get(1).compareTo(y.get(1));
            PriorityQueue<List<Integer>> queue = new PriorityQueue<>((int) k, comparator.reversed());
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    queue.add(List.of(arr.get(i), arr.get(j)));
                    if (queue.size() > k) {
                        queue.poll();
                    }
                }
            }
            return queue.poll();
        }
    }
}
