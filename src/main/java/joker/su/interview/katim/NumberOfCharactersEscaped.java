package joker.su.interview.katim;

/*
Given a string that contains lowercase English letters and the characters '#' and '!'. Determine the number of times a character will be escaped.

A character will be escaped if:
 - it is a lowercase English letter
 - there is a '!' character immediately before it
 - both the lowercase letter and the '!' are between a '#' character

Example 1:
Input: #ab!c#de!f
Output: 1
Explanation: there are only '!c' between two #.

Example 2:
Input: ##!r#po#
Output: 0
 */
public class NumberOfCharactersEscaped {

    public int numberOfCharactersEscaped(String expression) {
        int answer = 0;
        boolean isHash = false;
        boolean isExclamation = false;
        for (char symbol : expression.toCharArray()) {
            if (symbol == '#') {
                isHash = !isHash;
                isExclamation = false;
            } else if (symbol == '!') {
                isExclamation = true;
            } else {
                if (isHash && isExclamation) {
                    answer++;
                }
                isExclamation = false;
            }
        }
        return answer;
    }
}
