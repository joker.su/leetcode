package joker.su.interview.g42;

import java.util.HashSet;
import java.util.Set;

/*
Given a string that contains lowercase English letters.
You must split this string into a minimal number of substrings in such a way no letter occurs more than once in each substring a character will be escaped.

Example 1:
Input: world
Output: 1
Explanation: There is no need to split the string into substrings as all letters occur just once.

Example 2:
Input: dddd
Output: 4
Explanation: There is no need to split the string into substrings as all letters occur just once.

Example 3:
Input: cycle
Output: 2

Example 4:
Input: abba
Output: 2
 */
public class MinimalNumberOfSubstrings {

    public int getNumberOfSubstrings(String S) {
        int answer = 0;
        Set<Character> cache = new HashSet<>();
        for (char letter : S.toCharArray()) {
            if (cache.contains(letter)) {
                cache.clear();
                answer++;
            }
            cache.add(letter);
        }
        return answer + 1;
    }
}
