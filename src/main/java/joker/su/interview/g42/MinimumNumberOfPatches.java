package joker.su.interview.g42;

/*
There is a road consisting of N segments represented by a string S.
Segment S[k] of the road may contain a pothole, denoted by a single uppercase "X" character,
or may be a good segment without any potholes, denoted by a single dot "."
For example, string '.X..X' means that there are two potholes in the road: one located in segment S[1] and one in segment S[4]. All other segments are good.

The road fixing machine can patch over three consecutive segments at once with asphalt and repair all the potholes located within each of these segments.
Good or already repaired segments remain good after patching them.

Return the minimum number of patches required to repair all the potholes in the road.

Example 1:
Input: .X..X
Output: 2
Explanation: The road fixing machine could patch, for example, segments 0-2 and 2-4

Example 2:
Input: X.XXXXX.X.
Output: 3
Explanation: The road fixing machine could patch, for example, segments 0-2, 3-5 and 2-4

Example 3:
Input: XX.XXX..
Output: 2
Explanation: The road fixing machine could patch, for example, segments 0-2 and 3-5

Example 4:
Input: XXXX
Output: 2
Explanation: The road fixing machine could patch, for example, segments 0-2 and 1-3
*/
public class MinimumNumberOfPatches {

    public int minNumberOfPatches(String S) {
        int answer = 0;
        char[] road = S.toCharArray();
        int i = 0;
        while(i < road.length) {
            char segment = road[i];
            if(segment == 'X' || segment == 'x') {
                answer++;
                for(int j = 0; j < 3 && i < road.length; j++) {
                    road[i] = '.';
                    i++;
                }
                continue;
            }
            i++;
        }
        return answer;
    }
}
