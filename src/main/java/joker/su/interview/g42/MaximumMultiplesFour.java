package joker.su.interview.g42;

/*
Given an array of integers with size n, find the maximum among all integers which are multiples of 4.

Example 1:
Input: [-6, -91, 1011, -100, 84, -22, 0, 1, 473]
Output: 84
 */
public class MaximumMultiplesFour {

    public int findMaximumMultiplesFour(int[] numbers) {
        int answer = Integer.MIN_VALUE;
        for (int number : numbers) {
            if (isMultiplesOfFour(number)) {
                answer = Math.max(answer, number);
            }
        }
        return answer;
    }

    private boolean isMultiplesOfFour(int number) {
        return number % 4 == 0;
    }
}
